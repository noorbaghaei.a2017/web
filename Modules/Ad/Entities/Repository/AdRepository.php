<?php


namespace Modules\Ad\Entities\Repository;


use Modules\Ad\Entities\Ad;

class AdRepository implements AdRepositoryInterface
{

    public function getAll()
    {
       return Ad::latest()->get();
    }
}
