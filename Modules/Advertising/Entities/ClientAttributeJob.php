<?php

namespace Modules\Advertising\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\UserServices;



class ClientAttributeJob extends Model
{


    protected $table = 'client_attribute_job';
    protected $fillable = ['id','client','service','attribute','title','text','excerpt','token'];


    public function user_service()
    {
        return $this->belongsTo(UserServices::class, 'id');
    }

    

}
