<?php

namespace Modules\Advertising\Entities\Repository;



interface CategoryJobRepositoryInterface
{
    public function getAll();
}
