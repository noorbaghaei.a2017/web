<?php


namespace Modules\Advertising\Entities\Repository;


use Modules\Advertising\Entities\Position;

class PositionRepository implements PositionRepositoryInterface
{

    public function getAll()
    {
        return Position::latest()->get();
    }
}
