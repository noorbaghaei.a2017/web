<?php

namespace Modules\Advertising\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Helper\Trades\TimeAttribute;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class TimeJob extends Model implements HasMedia
{

    use HasMediaTrait,TimeAttribute;
    protected $table = 'time_job';
    protected $fillable = ['id','excerpt','client','service','text','icon','client','saturday','sunday','monday','tuesday','wednesday','thursday','friday','token'];



    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('medium')
            ->width(418)
            ->height(200)
            ->performOnCollections(config('cms.collection-image'));

        $this->addMediaConversion('thumb')
            ->width(70)
            ->height(60)
            ->performOnCollections(config('cms.collection-image'));
    }


}
