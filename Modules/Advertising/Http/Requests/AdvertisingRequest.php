<?php

namespace Modules\Advertising\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdvertisingRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        
            'title'=>'required|string',
            'text'=>'required|string',
            'postal_code'=>'required',
            'city'=>'required',
            'type'=>'required',
            'type_salary'=>'required',
            'excerpt'=>'required|string',
            'image'=>'mimes:jpeg,png,jpg|max:2000',
            'gender'=>'required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
