<?php

return [
    "text-create"=>' با استفاده از فرم زیر میتوانید تحصیلات جدید اضافه کنید.',
    "text-edit"=>' با استفاده از فرم زیر میتوانید تحصیلات خود را ویرایش کنید.',
    "store"=>"ثبت با موفقیت انجام شد.",
    "delete"=>"حذف با موفقیت انجام شد",
    "update"=>"بروز رسانی با موفقیت انجام شد",
    "index"=>"لیست تحصیلات",
    "error"=>"خطا",
    "singular"=>"تحصیلات",
    "collect"=>"تحصیلات",
    "permission"=>[
        "education-full-access"=>"دسترسی کامل به تحصیلات",
        "education-list"=>"لیست تحصیلات",
        "education-delete"=>"حذف تحصیلات",
        "education-create"=>"ایجاد تحصیلات",
        "education-edit"=>"ویرایش تحصیلات",
    ]

];
