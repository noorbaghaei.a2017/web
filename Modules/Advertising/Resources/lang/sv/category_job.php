<?php
return [
    "text-create"=>"you can create your category job",
    "text-edit"=>"you can edit your category job",
    "store"=>"Store Success",
    "store_cv"=>"Send Cv",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"categories job list",
    "error"=>"error",
    "singular"=>"category job",
    "collect"=>"categories job",
    "permission"=>[
        "category_job-full-access"=>"category job full access",
        "category_job-list"=>"categories job list",
        "category_job-delete"=>"category job delete",
        "category_job-create"=>"category job create",
        "category_job-edit"=>"edit category job",
    ]


];
