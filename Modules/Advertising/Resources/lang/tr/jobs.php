<?php
return [
    "text-create"=>"you can create your job",
    "text-edit"=>"you can edit your job",
    "store"=>"Store Success",
    "store_cv"=>"Send Cv",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"jobs list",
    "error"=>"error",
    "singular"=>"job",
    "collect"=>"jobs",
    "permission"=>[
        "job-full-access"=>"job full access",
        "job-list"=>"jobs list",
        "job-delete"=>"job delete",
        "job-create"=>"job create",
        "job-edit"=>"edit job",
    ]


];
