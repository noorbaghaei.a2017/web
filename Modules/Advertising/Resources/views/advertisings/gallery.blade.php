@extends('core::layout.panel')
@section('pageTitle', __('cms.create'))
@section('content')

    <div class="padding">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h2>{{__('cms.create')}} </h2>
                        <small>
                            {{__('advertising::advertisings.text-gallery')}}
                        </small>
                    </div>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">

                        <form  role="form" method="post" action="{{route('advertising.gallery.store',['advertising'=>$item->token])}}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                @include('core::layout.load-images',['item'=>$item,'medias'=>$medias,'model'=>'advertising'])

                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label for="image" class="form-control-label">{{__('cms.thumbnail')}} </label>
                                    @include('core::layout.load-gallery-image')
                                </div>
                            </div>
                            <div class="form-group row m-t-md">
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-primary btn-sm text-sm">{{__('cms.add')}} </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

