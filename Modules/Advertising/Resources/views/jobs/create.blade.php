
@extends('core::dashboard.main')

@section('content')
   
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{__('cms.create_bussiness')}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('jobs.index')}}">{{__('cms.bussiness')}}</a></li>
              <li class="breadcrumb-item active">{{__('cms.create_bussiness')}}</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <!-- /.container-fluid -->


  <!-- Main content -->
  <section class="content">


    <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">{{__('cms.create_bussiness')}}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="POST" action="{{route('jobs.store')}}" enctype="multipart/form-data">
                  @csrf
                <div class="card-body">
                @include('core::layout.alert-danger')
                <br>
<div class="row">
                  <div class="col-lg-12 col-md-12">
                  <div class="form-group">
                      
                      <input name="image" type="file" class="custom-file-input" id="customFile">
                      <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>

</div>

</div>
<br>

                 <div class="row">


                    <div class="col-lg-6 col-md-12"> 
                        <div class="form-group">
                            <label for="exampleInputName">{{__('cms.title')}}</label>
                            <input type="text" name="title" value="{{old('title')}}" class="form-control" id="exampleInputName" placeholder="{{__('cms.title')}}">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12"> 
                        <div class="form-group">
                            <label for="exampleInputName">{{__('cms.translate_english')}}</label>
                            <input type="text" name="title_en" value="{{old('title_en')}}" class="form-control" id="exampleInputName" placeholder="{{__('cms.translate_english')}}">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12"> 
                        <div class="form-group">
                            <label for="exampleInputName">{{__('cms.translate_persion')}}</label>
                            <input type="text" name="title_fa" value="{{old('title_fa')}}" class="form-control" id="exampleInputName" placeholder="{{__('cms.translate_persion')}}">
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-12">
                    <div class="form-group">
                    <label for="exampleInputEmail">{{__('cms.email')}}</label>
                    <input type="email" name="email" value="{{old('email')}}" class="form-control" id="exampleInputEmail" placeholder="{{__('cms.email')}}">
                  </div>

</div>
                </div>
                <div class="row">


<div class="col-lg-12 col-md-12"> 
    <div class="form-group">
        <label for="text">{{__('cms.text')}}</label>
        <input type="text" name="text" value="{{old('text')}}" class="form-control" id="text" placeholder="{{__('cms.text')}}">
    </div>
</div>


</div>
                <div class="row">


<div class="col-lg-6 col-md-12"> 
                  <div class="form-group">
                    <label for="exampleInputExcerpt">{{__('cms.excerpt')}}</label>
                    <input type="text" name="excerpt" value="{{old('excerpt')}}" class="form-control" id="exampleInputExcerpt" placeholder="{{__('cms.excerpt')}}">
                  </div>
</div>
<div class="col-lg-6 col-md-12"> 
                      <div class="form-group">
                        <label>{{__('cms.clients')}}</label>
                        <select name="client" class="form-control">
                        <option value="0" selected>{{__('cms.empty')}}</option>

                            @foreach($clients as $client)
                          <option value="{{$client->token}}">{{$client->full_name}}</option>
                       @endforeach
                        </select>
                      </div>

</div>
</div>

<div class="row">
                  <div class="col-lg-6 col-md-12">
                  <div class="form-group">
                    <label for="exampleInputAddress">{{__('cms.address')}}</label>
                    <input type="text" name="address" value="{{old('address')}}" class="form-control" id="exampleInputAddress" placeholder="{{__('cms.address')}}">
                  </div>
</div>

<div class="col-lg-6 col-md-12">
                  <div class="form-group">
                    <label for="exampleInputPhone">{{__('cms.phone')}}</label>
                    <input type="text" name="phone" value="{{old('phone')}}" class="form-control" id="exampleInputPhone" placeholder="{{__('cms.phone')}}">
                  </div>
</div>

</div>

                    <div class="row">


                    <div class="col-lg-6 col-md-12"> 
                      <div class="form-group">
                        <label>{{__('cms.categories')}}</label>
                        <select name="category" class="form-control" onchange="loadSubCategory(event)">
                            @foreach($list_services as $list)
                          <option value="{{$list->id}}">{{$list->title}}</option>
                       @endforeach
                        </select>
                      </div>

</div>

<div class="col-lg-6 col-md-12"> 
                      <div class="form-group">
                        <label>{{__('cms.sub-category')}}</label>
                        <select id="sub_category" name="sub_category" class="form-control">
                      
                        </select>
                      </div>

</div>

</div>

<div class="row">


<div class="col-lg-12 col-md-6">
                                   
                                    <label for="is_special" class="form-control-label">{{__('cms.is_special')}}  </label>
                                    <select  class="form-control" id="is_special" name="is_special" >

                                        <option  value="1" selected>{{__('cms.active')}}</option>
                                        <option  value="0">{{__('cms.inactive')}}</option>


                                    </select>
                                </div>


</div>


<div class="row">


<div class="col-lg-6 col-md-12"> 
  <div class="form-group">
    <label>{{__('cms.postal_code')}}</label>
    <label for="exampleInputPostal">{{__('cms.postal_code')}}</label>
   <input type="text" name="postal_code" id="postal_code" onkeyup="loadCity(event)" value="{{old('postal_code')}}" class="form-control" id="exampleInputPostal" placeholder="{{__('cms.postal_code')}}">
     
    
  </div>

</div>

<div class="col-lg-6 col-md-12"> 
  <div class="form-group">
    <label>{{__('cms.city')}}</label>
    <select id="city" name="city" class="form-control">
       
    </select>
  </div>

</div>

</div>
                  
                  
                
                 
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">{{__('cms.save')}}</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->

    
    </section>
    <!-- /.content -->
 
  
   


    


@endsection


@section('scripts')

<script>




function loadCity(e) {

var myInput = document.getElementById("postal_code");
    


if(myInput.value.length==5){

    $code=$("#postal_code").val();


$.ajax({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    type:'POST',
    contentType:'application/json; charset=utf-8',
    url:'/ajax/load/city/postalcode/'+$code,
    data: { field1:$code} ,
    beforeSend:function(){

        $('#city').hide();
    
},
    success: function (response) {

       
    if(response.data.status){

        console.log(response.data);
        $('#city').empty();
       
            $('#city').append("<option value='"+response.data.result[0].fields.plz_name+"' selected>"+response.data.result[0].fields.plz_name+"</option>");
        

    }
    else{
        console.log('no');
    }
    },
    error: function (xhr,ajaxOptions,thrownError) {
    
    console.log(xhr,ajaxOptions,thrownError);
        // $("#result-ajax").empty()
        //     .append("<span>"+msg+"</span>");
    },
    complete:function(){
        $('#city').show();
}

});

}
}




function loadSubCategory(e) {


     
$category= e.target.value;



$.ajax({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    type:'POST',
    contentType:'application/json; charset=utf-8',
    url:'/ajax/without/lang/load/sub/category/bussiness/'+$category,
    data: { field1:$category} ,
    beforeSend:function(){
    
},
beforeSend:function(){
    $('#sub_category').hide();
},

success: function (response) {


   if(response.data.status){

       console.log(response.data);

       $('#sub_category').empty();
     
       
     
     //start for each for load select input 

       $.each(response.data.result, function(key, index) {
   
                        if(key==0){
                                $('#sub_category').append("<option value='"+index.id+"'   selected>"+index.title+"</option>");
                               
                            }
                            else{
                                $('#sub_category').append("<option value='"+index.id+"'   >"+index.title+"</option>");
                               
                            }
         
 
        });

       }


  //end for each for load select input 

   
},
error: function (xhr,ajaxOptions,thrownError) {

console.log(xhr,ajaxOptions,thrownError);

    $("#sub_category").empty();
    

  
},
complete:function(){
    $('#sub_category').show();
}

});
}



</script>


@endsection












