@extends('core::dashboard.main')

@section('content')
   



    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{__('cms.edit')}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard.website')}}">{{__('cms.dashboard')}}</a></li>
              <li class="breadcrumb-item active">{{__('cms.edit')}}</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>


    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">


                @if(!$item->Hasmedia('images'))
                    <img class="profile-user-img img-fluid img-circle"
                       src="{{asset('template/images/no-image.jpg')}}"
                       alt="" style="border-radius:unset;width:100%;">

    @else
    <img class="profile-user-img img-fluid img-circle"
                       src="{{$item->getFirstMediaUrl('images')}}"
                       alt="" style="border-radius:unset;width:100%;">

    @endif
                 


                </div>

                <h3 class="profile-username text-center">{{$item->title}}</h3>

                <p class="text-muted text-center">Manager</p>

               

               

             
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">{{__('cms.about-it')}}</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
               
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                 
                  <li class="nav-item"><a class="nav-link active" href="#settings" data-toggle="tab">{{__('cms.setting_info')}}</a></li>
                  <li class="nav-item"><a class="nav-link" href="#translate" data-toggle="tab">{{__('cms.translate_lang')}}</a></li>
                  <li class="nav-item"><a class="nav-link" href="#time" data-toggle="tab">{{__('cms.time')}}</a></li>
                  <li class="nav-item"><a class="nav-link" href="#menu" data-toggle="tab">{{__('cms.menu')}}</a></li>
                  <li class="nav-item"><a class="nav-link" href="#gallery" data-toggle="tab">{{__('cms.gallery')}}</a></li>
                  <li class="nav-item"><a class="nav-link" href="#banner" data-toggle="tab">{{__('cms.banner')}}</a></li>
                  <li class="nav-item"><a class="nav-link" href="#slide" data-toggle="tab">{{__('cms.slide')}}</a></li>
                  <li class="nav-item"><a class="nav-link" href="#privacy" data-toggle="tab">{{__('cms.privacy_info')}}</a></li>
                
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
              @include('core::layout.alert-danger')
              @include('core::layout.alert-success')
                <div class="tab-content">
                 
                 

                  <div class="tab-pane active" id="settings">
                    <form action="{{route('jobs.update', ['job' => $item->token])}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    {{method_field('PATCH')}}
                    <div class="form-group row">
                    <div class="col-sm-10">
                      <input name="image" type="file" class="custom-file-input" id="customFile">
                      <label class="custom-file-label" for="customFile">Choose Image</label>
                    </div>
                    </div>  
                     
                    
                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">{{__('title')}}</label>
                      
                          <input type="text" name="title" value="{{$item->title}}" class="form-control" id="inputName" placeholder="{{__('cms.title')}}">
                       
                      </div>
                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">{{__('text')}}</label>
                      
                          <input type="text" name="text" value="{{$item->text}}" class="form-control" id="inputName" placeholder="{{__('cms.text')}}">
                       
                      </div>
                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">{{__('excerpt')}}</label>
                      
                          <input type="text" name="excerpt" value="{{$item->excerpt}}" class="form-control" id="inputName" placeholder="{{__('cms.excerpt')}}">
                       
                      </div>
                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">{{__('address')}}</label>
                      
                          <input type="text" name="address" value="{{$item->address}}" class="form-control" id="inputName" placeholder="{{__('cms.address')}}">
                       
                      </div>

                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">{{__('email')}}</label>
                      
                          <input type="email" name="email" value="{{$item->email}}" class="form-control" id="inputName" placeholder="{{__('cms.email')}}">
                       
                      </div>

                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">{{__('phone')}}</label>
                      
                          <input type="text" name="phone" value="{{$item->phone}}" class="form-control" id="inputName" placeholder="{{__('cms.phone')}}">
                       
                      </div>

                      <div class="form-group row">
                        <label>{{__('cms.clients')}}</label>
                        <select name="client" class="form-control">
                        <option value="0" {{$item->user==null ? 'selected' : ''}}>{{__('cms.empty')}}</option>

                            @foreach($clients as $client)
                          <option value="{{$client->full_name}}" {{$item->user==$client->id ? 'selected' : ''}}>{{$client->full_name}}</option>
                       @endforeach
                        </select>
                      </div>
                     
                      <div class="form-group row">
                        <label>{{__('cms.categories')}}</label>
                        <select name="category" class="form-control" onchange="loadSubCategory(event)">
                        @foreach($list_services as $list)
                          <option value="{{$list->id}}" {{$list->id==$parent->id ? 'selected' : ''}}>{{$list->title}}</option>
                       @endforeach
                        </select>
                      </div>

                     



                      <div class="form-group row">
                        <label>{{__('cms.sub-category')}}</label>
                        <select id="sub_category" name="category" class="form-control">
                        @foreach($childs as $child)
                          <option value="{{$child->id}}" {{$child->id==$service->id ? 'selected' : ''}}>{{$child->title}}</option>
                       @endforeach
                        </select>
                      </div>


                      <div class="form-group row">


                                  
                                    <label for="is_special" class="form-control-label">{{__('cms.is_special')}}  </label>
                                    <select  class="form-control" id="is_special" name="is_special" >

                                        <option  value="1" {{$item->is_special==1 ? 'selected' : ''}}>{{__('cms.active')}}</option>
                                        <option  value="0" {{$item->is_special==0 ? 'selected' : ''}}>{{__('cms.inactive')}}</option>


                                    </select>
                             

</div>
                     

                     
  <div class="form-group row">
    
    <label for="exampleInputPostal">{{__('cms.postal_code')}}</label>
    
   <input type="text" name="postal_code" id="postal_code" onkeyup="loadCity(event)" value="{{$item->postal_code}}" class="form-control" id="exampleInputPostal" placeholder="{{__('cms.postal_code')}}">
    
    
  </div>




  <div class="form-group row">
  
    <label>{{__('cms.city')}}</label>
    
    <select id="city" name="city"  class="form-control">
       
       <option value="{{$item->city}}">{{$item->city}}</option>
    </select>
   
  </div>





                      
                     
                      
                    
                     
                     
                     
                     
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" class="btn btn-danger">{{__('cms.save')}}</button>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="tab-pane" id="translate">
                    <form action="{{ route('job.language.update',['lang'=>'fa','token'=>$item->token]) }}" method="POST">
                      @csrf
                                @method('PATCH')
                      <div class="form-group row">
                            <label for="inputName" class="col-sm-2 col-form-label">{{__('cms.title_fa')}}</label>
                            <div class="col-sm-10">
                              <input type="text" name="title" value="{{isset($item->translates->where('lang','fa')->first()->title) ? $item->translates->where('lang','fa')->first()->title : ''}}" class="form-control" id="inputName" placeholder="{{__('cms.title_fa')}}" autocomplete="off">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="inputName" class="col-sm-2 col-form-label">{{__('cms.excerpt_fa')}}</label>
                            <div class="col-sm-10">
                              <input type="text" name="excerpt" value="{{isset($item->translates->where('lang','fa')->first()->excerpt) ? $item->translates->where('lang','fa')->first()->excerpt : ''}}" class="form-control" id="inputName" placeholder="{{__('cms.excerpt_fa')}}" autocomplete="off">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="inputName" class="col-sm-2 col-form-label">{{__('cms.text_fa')}}</label>
                            <div class="col-sm-10">
                              <input type="text" name="text" value="{{isset($item->translates->where('lang','fa')->first()->text) ? $item->translates->where('lang','fa')->first()->text : ''}}" class="form-control" id="inputName" placeholder="{{__('cms.text_fa')}}" autocomplete="off">
                            </div>
                          </div>
                        
                          
                          
                          <div class="form-group row">
                            <div class="offset-sm-2 col-sm-10">
                              <button type="submit" class="btn btn-danger">{{__('cms.save')}}</button>
                            </div>
                          </div>
    
                          </form>
                          <form action="{{ route('job.language.update',['lang'=>'en','token'=>$item->token]) }}" method="POST">
                      @csrf
                      @method('PATCH')
                          <div class="form-group row">
                            <label for="inputName" class="col-sm-2 col-form-label">{{__('cms.title_en')}}</label>
                            <div class="col-sm-10">
                              <input type="text" name="title" value="{{isset($item->translates->where('lang','en')->first()->title) ? $item->translates->where('lang','en')->first()->title : ''}}" class="form-control" id="inputName" placeholder="{{__('cms.title_en')}}" autocomplete="off">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="inputName" class="col-sm-2 col-form-label">{{__('cms.excerpt_en')}}</label>
                            <div class="col-sm-10">
                              <input type="text" name="excerpt" value="{{isset($item->translates->where('lang','en')->first()->excerpt) ? $item->translates->where('lang','en')->first()->excerpt : ''}}" class="form-control" id="inputName" placeholder="{{__('cms.excerpt_en')}}" autocomplete="off">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="inputName" class="col-sm-2 col-form-label">{{__('cms.text_en')}}</label>
                            <div class="col-sm-10">
                              <input type="text" name="text" value="{{isset($item->translates->where('lang','en')->first()->text) ? $item->translates->where('lang','en')->first()->text : ''}}" class="form-control" id="inputName" placeholder="{{__('cms.text_en')}}" autocomplete="off">
                            </div>
                          </div>
                          <div class="form-group row">
                            <div class="offset-sm-2 col-sm-10">
                              <button type="submit" class="btn btn-danger">{{__('cms.save')}}</button>
                            </div>
                          </div>
    
                          </form>
                   </div>
                  <div class="tab-pane" id="privacy">
                   
                  </div>

                  <div class="tab-pane" id="time">
                    <form action="{{ route('update.time.job',['token'=>$item->token]) }}" method="POST">
                      @csrf
                     
                          <div class="form-group row">
                            <label for="inputName" class="col-sm-2 col-form-label">{{__('cms.monday')}}</label>
                            <div class="col-sm-10">
                              <input type="text" name="monday" value="{{$item->time->monday }}" class="form-control" id="inputName" placeholder="{{__('cms.monday')}}" autocomplete="off">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="inputName" class="col-sm-2 col-form-label">{{__('cms.tuesday')}}</label>
                            <div class="col-sm-10">
                              <input type="text" name="tuesday" value="{{$item->time->tuesday }}" class="form-control" id="inputName" placeholder="{{__('cms.tuesday')}}" autocomplete="off">
                            </div>
                          </div>
                        
                          <div class="form-group row">
                            <label for="inputName" class="col-sm-2 col-form-label">{{__('cms.wednesday')}}</label>
                            <div class="col-sm-10">
                              <input type="text" name="wednesday" value="{{$item->time->wednesday }}" class="form-control" id="inputName" placeholder="{{__('cms.wednesday')}}" autocomplete="off">
                            </div>
                          </div>

                          <div class="form-group row">
                            <label for="inputName" class="col-sm-2 col-form-label">{{__('cms.thursday')}}</label>
                            <div class="col-sm-10">
                              <input type="text" name="thursday" value="{{$item->time->thursday }}" class="form-control" id="inputName" placeholder="{{__('cms.thursday')}}" autocomplete="off">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="inputName" class="col-sm-2 col-form-label">{{__('cms.friday')}}</label>
                            <div class="col-sm-10">
                              <input type="text" name="friday" value="{{$item->time->friday }}" class="form-control" id="inputName" placeholder="{{__('cms.friday')}}" autocomplete="off">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="inputName" class="col-sm-2 col-form-label">{{__('cms.saturday')}}</label>
                            <div class="col-sm-10">
                              <input type="text" name="saturday" value="{{$item->time->saturday }}" class="form-control" id="inputName" placeholder="{{__('cms.saturday')}}" autocomplete="off">
                            </div>
                          </div>
                        
                          <div class="form-group row">
                            <label for="inputName" class="col-sm-2 col-form-label">{{__('cms.sunday')}}</label>
                            <div class="col-sm-10">
                              <input type="text" name="sunday" value="{{$item->time->sunday }}" class="form-control" id="inputName" placeholder="{{__('cms.sunday')}}" autocomplete="off">
                            </div>
                          </div>
                        
                          <div class="form-group row">
                            <div class="offset-sm-2 col-sm-10">
                              <button type="submit" class="btn btn-danger">{{__('cms.save')}}</button>
                            </div>
                          </div>
    
                          </form>
                  </div>

                  <div class="tab-pane" id="menu">
                   

                    @foreach ($menus as $menu)
 
                    <form action="{{route('store.menu.job.admin',['menu'=>$menu->id,'token'=>$item->token])}}" method="POST" enctype="multipart/form-data">
                      @csrf
                      <h5>{{ $menu->title }}</h5>
                    <div class="form-group row">
                      <div class="col-sm-12">
                        <input name="image" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose Image</label>
                      </div>
                      <div class="form-group row">
                        <label for="inputName" class="col-sm-12 col-form-label">{{__('cms.title')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="title" value="" class="form-control" id="inputName" placeholder="{{__('cms.title')}}" autocomplete="off">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputName" class="col-sm-12 col-form-label">{{__('cms.excerpt')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="excerpt" value="" class="form-control" id="inputName" placeholder="{{__('cms.excerpt')}}" autocomplete="off">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputName" class="col-sm-12 col-form-label">{{__('cms.price')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="price" value="" class="form-control" id="inputName" placeholder="{{__('cms.price')}}" autocomplete="off">
                        </div>
                      </div>
                      
                      </div> 
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" class="btn btn-danger">{{__('cms.save')}}</button>
                        </div>
                      </div>
                     
                    </form> 
                    
                    @endforeach
                  </div>
                  <div class="tab-pane" id="gallery">
                    <form action="{{ route('add.gallery.job',['token'=>$item->token]) }}" method="POST" enctype="multipart/form-data">
                      @csrf
                    <div class="form-group row">
                      <div class="col-sm-12">
                        <input name="photo" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose Image</label>
                      </div>
                      
                      </div> 
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" class="btn btn-danger">{{__('cms.save')}}</button>
                        </div>
                      </div>
                     
                    </form> 
                  </div>
                  <div class="tab-pane" id="banner">
                    
                    <form action="{{ route('update.banner.job',['token'=>$item->token]) }}" method="POST" enctype="multipart/form-data">
                      @csrf
                    <div class="form-group row">
                      <div class="col-sm-12">
                        <input name="banner" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose Image</label>
                      </div>
                      
                      </div> 
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" class="btn btn-danger">{{__('cms.save')}}</button>
                        </div>
                      </div>
                     
                    </form> 
                  </div>
                  <div class="tab-pane" id="slide">
                   
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
    </section>





@endsection
 


@section('scripts')

<script>




function loadCity(e) {

var myInput = document.getElementById("postal_code");
    


if(myInput.value.length==5){

    $code=$("#postal_code").val();


$.ajax({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    type:'POST',
    contentType:'application/json; charset=utf-8',
    url:'/ajax/load/city/postalcode/'+$code,
    data: { field1:$code} ,
    beforeSend:function(){

        $('#city').hide();
    
},
    success: function (response) {

       
    if(response.data.status){

        console.log(response.data);
        $('#city').empty();
       
            $('#city').append("<option value='"+response.data.result[0].fields.plz_name+"' selected>"+response.data.result[0].fields.plz_name+"</option>");
        

    }
    else{
        console.log('no');
    }
    },
    error: function (xhr,ajaxOptions,thrownError) {
    
    console.log(xhr,ajaxOptions,thrownError);
        // $("#result-ajax").empty()
        //     .append("<span>"+msg+"</span>");
    },
    complete:function(){
        $('#city').show();
}

});

}
}




function loadSubCategory(e) {


     
$category= e.target.value;



$.ajax({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    type:'POST',
    contentType:'application/json; charset=utf-8',
    url:'/ajax/without/lang/load/sub/category/bussiness/'+$category,
    data: { field1:$category} ,
    beforeSend:function(){
    
},
beforeSend:function(){
    $('#sub_category').hide();
},

success: function (response) {


   if(response.data.status){

       console.log(response.data);

       $('#sub_category').empty();
     
       
     
     //start for each for load select input 

       $.each(response.data.result, function(key, index) {
   
                        if(key==0){
                                $('#sub_category').append("<option value='"+index.id+"'   selected>"+index.title+"</option>");
                               
                            }
                            else{
                                $('#sub_category').append("<option value='"+index.id+"'   >"+index.title+"</option>");
                               
                            }
         
 
        });

       }


  //end for each for load select input 

   
},
error: function (xhr,ajaxOptions,thrownError) {

console.log(xhr,ajaxOptions,thrownError);

    $("#sub_category").empty();
    

  
},
complete:function(){
    $('#sub_category').show();
}

});
}



</script>


@endsection

 
 
