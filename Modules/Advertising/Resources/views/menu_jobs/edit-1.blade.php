@extends('core::layout.panel')
@section('pageTitle', __('cms.edit'))
@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">
                <div class="box p-a-xs">

                    <div class="row">
                        <div class="col-md-5">
                            <a href="#">
                            @if(!$item->Hasmedia('images'))
                                    <img style="width: 400px;height: auto" src="{{asset('img/no-img.gif')}}" alt="" class="img-responsive">



                            @else
                                    <img style="width: 400px;height: auto" src="{{$item->getFirstMediaUrl('images')}}" alt="" class="img-responsive">

                                @endif


                            </a>
                        </div>
                        <div class="col-md-7">
                        <div style="padding-top: 35px">
                            @foreach(config('cms.extra-lang') as $language)
                                <a href="{{route('menujob.language.show',['lang'=>$language,'token'=>$item->token])}}">
                                    <img src="{{asset(config('cms.flag.'.$language))}}" width="30">
                                </a>
                            @endforeach
                        </div>
                            <div style="padding-top: 35px">
                                <h6 style="padding-top: 35px"> {{__('cms.subject')}} : </h6>
                                <h4 style="padding-top: 35px">    {{$item->title}}</h4>
                            </div>
                          
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    @include('core::layout.alert-danger')
                    <div class="box-header">
                       
                        <div class="pull-left">
                            <small>
                                {{__('advertising::menu_job.text-edit')}}
                            </small>
                        </div>
                        <a onclick="window.print()" class="btn btn-primary btn-sm text-sm text-white pull-right">{{__('cms.print')}} </a>
                    </div>
                    <br>
                    <br>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        <form id="signupForm" action="{{route('menujobs.update', ['menujob' => $item->token])}}" method="POST" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" value="{{$item->token}}" name="token">
                            {{method_field('PATCH')}}
                        <div class="form-group row">
                            <div class="col-sm-3">
                                <label for="title" class="form-control-label">{{__('cms.title')}}</label>
                                <input type="text" name="title" class="form-control" id="title"  value="{{$item->title}}">
                            </div>
                            <div class="col-sm-3">
                                <label for="icon" class="form-control-label">{{__('cms.icon')}}</label>
                                <input type="text" name="icon" class="form-control" id="title"  value="{{$item->icon}}">
                            </div>
                            <div class="col-sm-3">
                                <label for="title" class="form-control-label">{{__('cms.thumbnail')}} </label>
                                @include('core::layout.load-single-image')
                            </div>
                           

                        </div>
                      
                            @include('core::layout.update-button')

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('heads')

    <link href="{{asset('assets/css/validate/screen.css')}}" rel="stylesheet"/>

@endsection()

@section('scripts')


    <script src="{{asset('assets/scripts/validate/jquery.validate.js')}}"></script>


    <script>


        $().ready(function() {






         

         


          

            // validate the comment form when it is submitted
            $("#commentForm").validate();

            // validate signup form on keyup and submit
            $("#signupForm").validate({
                rules: {
                    title: {
                        required: true
                    },
                   


                },
                messages: {
                    title:"{{__('cms.title.required')}}",
                  
                }
            });


            //code to hide topic selection, disable for demo
            var newsletter = $("#newsletter");
            // newsletter topics are optional, hide at first
            var inital = newsletter.is(":checked");
            var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
            var topicInputs = topics.find("input").attr("disabled", !inital);
            // show when newsletter is checked
            newsletter.click(function() {
                topics[this.checked ? "removeClass" : "addClass"]("gray");
                topicInputs.attr("disabled", !this.checked);
            });
        });
    </script>

@endsection
