<?php


namespace Modules\Article\Entities\Repository;


interface ArticleRepositoryInterface
{

    public function getAll();

}
