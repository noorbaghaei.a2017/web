<?php

namespace Modules\Brand\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Brand\Entities\Brand;
use Modules\Brand\Entities\Repository\BrandRepositoryInterface;
use Modules\Brand\Http\Requests\BrandRequest;
use Modules\Brand\Transformers\BrandCollection;

class BrandController extends Controller
{

    protected $entity;
    private $repository;

    public function __construct(BrandRepositoryInterface $repository)
    {
        $this->entity=new Brand();
        $this->repository=$repository;
        $this->middleware('permission:brand-list');
        $this->middleware('permission:brand-create')->only(['create','store']);
        $this->middleware('permission:brand-edit' )->only(['edit','update']);
        $this->middleware('permission:brand-delete')->only(['destroy']);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=$this->repository->getAll();

            $brands = new BrandCollection($items);

            $data= collect($brands->response()->getData())->toArray();

            return view('core::response.index',compact('data'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            return view('brand::brands.create');
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
            !isset($request->title) &&
            !isset($request->href)

            ){
                $items=$this->entity->latest()->paginate(config('cms.paginate'));
                return view('brand::brands.index',compact('items'));
            }
            $items=$this->entity
                ->where("title",'LIKE','%'.trim($request->title).'%')
                ->where("href",'LIKE','%'.trim($request->href).'%')
                ->paginate(config('cms.paginate'));
            return view('customer::customers.index',compact('items','request'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(BrandRequest $request)
    {
        try {
            $href=is_null($request->href) ? "#" : $request->href;
            $this->entity->user=auth('web')->user()->id;
            $this->entity->title=$request->input('title');
            $this->entity->excerpt=$request->input('excerpt');
            $this->entity->text=$request->input('text');
            $this->entity->order=orderInfo($request->input('order'));
            $this->entity->href=$href;
            $this->entity->token=tokenGenerate();

            $saved=$this->entity->save();

            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }


            if(!$saved){
                return redirect()->back()->with('error',__('brand::brands.error'));
            }else{
                return redirect(route("brands.index"))->with('message',__('brand::brands.store'));
            }


        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }


    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($token)
    {
        try {
            $item=$this->entity->whereToken($token)->first();
            return view('brand::brands.edit',compact('item'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(BrandRequest $request, Brand $brand)
    {
        try {
            $this->entity=$brand;
            $href=is_null($request->href) ? "#" : $request->href;
            $update=$this->entity->update([
                'user'=>auth('web')->user()->id,
                "slug"=>null,
                "title"=>$request->input('title'),
                "excerpt"=>$request->input('excerpt'),
                "text"=>$request->input('text'),
                "href"=>$href,
                "order"=>orderInfo($request->input('order')),
            ]);

            $this->entity->replicate();
            if($request->has('image')){
                destroyMedia($this->entity,config('cms.collection-image'));
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }
            if(!$update){
                return redirect()->back()->withErrors('error',__('brand::brands.error'));
            }else{
                return redirect(route("brands.index"))->with('message',__('brand::brands.update'));
            }


        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($token)
    {
        try {

            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            if($this->entity->Hasmedia(config('cms.collection-image'))){
                destroyMedia($this->entity,config('cms.collection-image'));
            }
            $deleted=$this->entity->delete();

            if(!$deleted){
                return redirect()->back()->with('error',__('brand::brands.error'));
            }else{
                return redirect(route("brands.index"))->with('message',__('brand::brands.delete'));
            }


        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    public  function languageShow(Request $request,$lang,$token){
        $item=Brand::with('translates')->where('token',$token)->first();
        return view('brand::brands.language',compact('item','lang'));

    }
    public  function languageUpdate(Request $request,$lang,$token){

        DB::beginTransaction();
        $item=Brand::with('translates')->where('token',$token)->first();
        if( $item->translates->where('lang',$lang)->first()){
            $changed=$item->translates->where('lang',$lang)->first()->update([
                'symbol'=>$request->symbol,
                'title'=>$request->title,
                'text'=>$request->text,
                'description_seo'=>$request->description_seo,
                'title_seo'=>$request->title_seo,
                'keyword_seo'=>$request->keyword_seo,
                'canonical_seo'=>$request->canonical_seo,
            ]);
        }else{
            $changed=$item->translates()->create([
                'symbol'=>$request->symbol,
                'title'=>$request->title,
                'text'=>$request->text,
                'description_seo'=>$request->description_seo,
                'title_seo'=>$request->title_seo,
                'keyword_seo'=>$request->keyword_seo,
                'canonical_seo'=>$request->canonical_seo,
                'lang'=>$lang
            ]);
        }


        if(!$changed){
            DB::rollBack();
            return redirect()->back()->with('error',__('brand::brands.error'));
        }else{
            DB::commit();
            return redirect(route("brands.index"))->with('message',__('brand::brands.update'));
        }

    }
}
