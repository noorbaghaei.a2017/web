
@extends('core::dashboard.main')

@section('content')
   
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{__('cms.create_slider')}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('jobs.index')}}">{{__('cms.bussiness')}}</a></li>
              <li class="breadcrumb-item active">{{__('cms.create_category')}}</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <!-- /.container-fluid -->


  <!-- Main content -->
  <section class="content">


    <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">{{__('cms.create_slider')}}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="POST" action="{{route('carousels.store')}}" enctype="multipart/form-data">
                  @csrf
                <div class="card-body">
                @include('core::layout.alert-danger')
                <br>
<div class="row">
                  <div class="col-lg-12 col-md-12">
                  <div class="form-group">
                      
                      <input name="image" type="file" class="custom-file-input" id="customFile">
                      <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>

</div>

</div>

<div class="row">


<div class="col-lg-6 col-md-12"> 
    <div class="form-group">
        <label for="exampleInputName">{{__('cms.title')}}</label>
        <input type="text" name="title" value="{{old('title')}}" class="form-control" id="exampleInputName" placeholder="{{__('cms.title')}}">
    </div>
</div>
<div class="col-lg-6 col-md-12"> 
    <div class="form-group">
        <label for="text">{{__('cms.text')}}</label>
        <input type="text" name="text" value="{{old('text')}}" class="form-control" id="text" placeholder="{{__('cms.text')}}">
    </div>
</div>
<div class="col-lg-6 col-md-12"> 
    <div class="form-group">
        <label for="excerpt">{{__('cms.excerpt')}}</label>
        <input type="text" name="excerpt" value="{{old('excerpt')}}" class="form-control" id="excerpt" placeholder="{{__('cms.excerpt')}}">
    </div>
</div>


</div>
                
                <div class="row">


                <div class="col-lg-6 col-md-12"> 
                      <div class="form-group">
                        <label>{{__('cms.status')}}</label>
                        <select name="status" class="form-control">
                        <option  value="1" selected>{{__('cms.active')}}</option>
                                        <option  value="0">{{__('cms.inactive')}}</option>
                        </select>
                      </div>

</div>


  






</div>
                  
                  
                
                 
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">{{__('cms.save')}}</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->

    
    </section>
    <!-- /.content -->
 
  
   


    


@endsection


@section('scripts')

<script>




</script>


@endsection












