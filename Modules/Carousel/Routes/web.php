<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;


Route::group(["prefix"=>config('cms.prefix-admin'), "middleware" => ["auth:web"]], function () {
    Route::resource('/carousels', 'CarouselController');
    Route::get('carousels/{lang}/{token}', 'CarouselController@languageShow')->name('carousel.language.show');
    Route::patch('carousels/lang/update/{lang}/{token}', 'CarouselController@languageUpdate')->name('carousel.language.update');

    Route::group(["prefix"=>'search', "middleware" => ["auth:web"]], function () {
        Route::post('/carousels', 'CarouselController@search')->name('search.carousel');
    });
});
