<!-- content -->
@foreach($items as $chat)
    <div class="p-a-md">
        @if(!is_null($chat->user))
            <div class="m-b">
                <a href="#" class="pull-left w-40 m-r-sm">

                    @if(!$setting->Hasmedia('logo'))
                        <img src="{{asset('img/no-img.gif')}}" alt="." class="w-full img-circle">
                    @else
                        <img src="{{$setting->getFirstMediaUrl('logo')}}" alt="." class="w-full img-circle">
                    @endif

                </a>
                <div class="clear">
                    <div>

                        <div class="p-a p-y-sm success inline rounded">
                            {{$chat->text}}
                        </div>

                    </div>

                    <div class="text-muted text-xs m-t-xs"><i class="fa fa-ok text-success"></i> {{$item->created_at->ago()}}  </div>
                </div>
            </div>

        @else
            <div class="m-b">
                <a href="#" class="pull-right w-40 m-r-sm">

                    @if(!\Modules\Client\Entities\Client::find(59)->Hasmedia('images'))
                        <img src="{{asset('img/no-img.gif')}}" alt="." class="w-full img-circle">
                    @else
                        <img src="{{\Modules\Client\Entities\Client::find(59)->getFirstMediaUrl('images')}}" alt="." class="w-full img-circle">
                    @endif

                </a>
                <div class="clear text-right">
                    <div>

                        <div class="p-a p-y-sm success inline rounded">
                            {{$chat->text}}
                        </div>

                    </div>

                    <div class="text-muted text-xs m-t-xs"><i class="fa fa-ok text-success"></i>{{$item->created_at->ago()}}</div>
                </div>
            </div>

        @endif
    </div>
@endforeach
<!-- / -->
