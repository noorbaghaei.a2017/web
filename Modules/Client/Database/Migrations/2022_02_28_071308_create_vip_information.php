<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVipInformation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vip_information', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('client')->nullable();
            $table->foreign('client')->references('id')->on('clients')->onDelete('cascade');            
            $table->longText('code_vip')->nullable();
            $table->string('full_name_bime_gozar');
            $table->string('father_name_bime_gozar');
            $table->string('code_meli_bime_gozar');
            $table->string('shenasname_meli_bime_gozar');
            $table->string('sodor_bime_gozar');
            $table->string('tavalod_bime_gozar');
            $table->string('tarikh_tavalod_bime_gozar');
            $table->string('jensiat_bime_gozar');
            $table->string('tahsilat_bime_gozar');
            $table->string('taahol_bime_gozar');
            $table->string('telephone_hamrah_bime_gozar');
            $table->string('email_bime_gozar')->nullable();
            $table->string('phone_bime_gozar');
            $table->string('code_posti_bime_gozar');
            $table->text('address_work_bime_gozar');
            $table->text('address_home_bime_gozar');
            $table->string('job_bime_gozar');
            $table->string('nesbate_bime_bime_shode')->nullable();
            $table->string('full_name_bime_shode')->nullable();
            $table->string('father_name_bime_shode')->nullable();
            $table->string('code_meli_bime_shode')->nullable();
            $table->string('shenasname_bime_shode')->nullable();
            $table->string('sodor_bime_shode')->nullable();
            $table->string('mahale_tavalod_bime_shode')->nullable();
            $table->string('tarikh_tavalod_bime_shode')->nullable();
            $table->string('jensiat_bime_shode')->nullable();
            $table->string('sathe_tahsilat_bime_shode')->nullable();
            $table->string('vaziate_taahol_bime_shode')->nullable();
            $table->string('vaziate_khedmat_bime_shode')->nullable();
            $table->string('telephone_hamrah_bime_shode')->nullable();
            $table->string('email_bime_shode')->nullable();
            $table->string('phone_bime_shode')->nullable();
            $table->string('code_posti_bime_shode')->nullable();
            $table->text('address_work_bime_shode')->nullable();
            $table->text('address_home_bime_shode')->nullable();
            $table->string('job_bime_shode')->nullable();
            $table->string('full_name_bime_shode_hayat');
            $table->string('father_name_bime_shode_hayat');
            $table->string('code_meli_bime_shode_hayat');
            $table->string('shenasname_bime_shode_hayat');
            $table->string('tarikh_tavalod_bime_shode_hayat');
            $table->string('nesbat_bime_shode_hayat');
            $table->string('darsad_bime_shode_hayat');
            $table->string('info_user_bime_shode_hayat')->nullable();
            $table->string('full_name_bime_shode_fot');
            $table->string('father_name_bime_shode_fot');
            $table->string('code_meli_bime_shode_fot');
            $table->string('shenasname_meli_bime_shode_fot');
            $table->string('tarikh_tavalod_bime_shode_fot');
            $table->string('nesbate_bime_shode_fot');
            $table->string('darsad_bime_shode_fot');
            $table->string('vaziate_sarbazi_bime_gozar');
            $table->string('taeid_vekalat_bime_shode');
            $table->string('taeid_ghavanin');
            $table->string('info_user_bime_shode_fot')->nullable();
            $table->string('name_type_bime_shode_detail');
            $table->string('sarmaye_bime_shode_detail');
            $table->string('tarikh_shoro_bime_shode_detail');
            $table->string('modate_bime_shode_detail');
            $table->string('name_sherkat_bime_shode_detail');
            $table->string('name_vahede_sodor_bime_shode_detail');
            $table->string('name_pardakht_bime_shode_detail');
            $table->string('modat_bime_shode_detail');
            $table->tinyInteger('status')->default(0);
            $table->string('poshesh_hadese_bime_shode_detail');
            $table->string('poshesh_amraze_khas_bime_shode_detail');
            $table->string('moafiat_pardakht_bime_shode_detail');

            $table->text('token');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('');
    }
}
