<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePezeshkiVip extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pezeshki_vip', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('vip_list')->nullable();
            $table->foreign('vip_list')->references('id')->on('vip_information')->onDelete('cascade');            
            $table->string('afsordegi_pezeshki')->nullable();
            $table->string('saratan_pezeshki')->nullable();
            $table->string('sabeghe_shimi_pezeshki')->nullable();
            $table->string('vaziate_salamat_pezeshki')->nullable();
            $table->json('azmayeshat_gharbalgari_pezeshki')->nullable();
            $table->text('azmayeshat_tozihat_pezeshki')->nullable();
            $table->text('mashrobat_tozihat_pezeshki')->nullable();
            $table->text('dokhaniyat_tozihat_pezeshki')->nullable();
            $table->string('father_hayat_pezeshki')->nullable();
            $table->string('mother_hayat_pezeshki')->nullable();
            $table->string('father_age_pezeshki')->nullable();
            $table->string('mother_age_pezeshki')->nullable();
            $table->string('sabeghe_bimari_pezeshki')->nullable();
            $table->string('sabeghe_bimari_khas_pezeshki')->nullable();
            $table->string('ghad_pezeshki')->nullable();
            $table->string('vazn_pezeshki')->nullable();
            $table->string('taghir_vazn_pezeshki')->nullable();
            $table->json('moshkele_posti_pezeshki')->nullable();
            $table->string('ehsas_khastegi_pezeshki')->nullable();
            $table->json('moshkelate_sar_pezeshki')->nullable();
            $table->string('morajee_be_cheshmpezeshki_pezeshki')->nullable();
            $table->json('mavarede_khas_cheshm_pezeshki')->nullable();
            $table->string('morajee_be_dandanpezeshki_pezeshki')->nullable();
            $table->json('moshkelate_digar_pezeshki')->nullable();
            $table->string('moshkelate_gardan_pezeshki')->nullable();
            $table->json('moshkelate_tanafosi_pezeshki')->nullable();
            $table->string('moshkelate_cxr_pezeshki')->nullable();
            $table->json('moshkelate_ghalbi_oroghi_pezeshki')->nullable();
            $table->string('moshkelate_navar_ghlabi_pezeshki')->nullable();
            $table->json('moshkelate_dastgahe_govaresh_pezeshki')->nullable();
            $table->string('moshkelate_dastgahe_azolani_pezeshki')->nullable();
            $table->json('moshkelate_dastgahe_asab_markazi_pezeshki')->nullable();
            $table->json('moshkelate_dastgahe_khonsaz_pezeshki')->nullable();
            $table->json('moshkelate_dastgahe_ghodade_daronriz_pezeshki')->nullable();
            $table->json('moshkelate_pestan_pezeshki')->nullable();
            $table->string('bimari_digar_bishtar_pezeshki')->nullable();
            $table->string('masraf_daro_pezeshki')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('');
    }
}
