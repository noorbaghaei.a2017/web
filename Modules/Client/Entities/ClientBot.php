<?php

namespace Modules\Client\Entities;

use Illuminate\Database\Eloquent\Model;

class ClientBot extends Model
{
    protected $table="client_bot";
    protected $fillable = ['title','description','botable_id','botable_type','count','client'];

    public function botable()
    {
        return $this->morphTo();
    }
}
