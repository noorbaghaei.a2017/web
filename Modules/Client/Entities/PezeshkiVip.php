<?php

namespace Modules\Client\Entities;

use Illuminate\Database\Eloquent\Model;

class pezeshkiVip extends Model
{


    protected $table="pezeshki_vip";

    protected $fillable = [
        'vip_list',
        'afsordegi_pezeshki',
        'saratan_pezeshki',
        'mavarede_khas_cheshm_pezeshki',
        'morajee_be_dandanpezeshki_pezeshki',
        'sabeghe_shimi_pezeshki',
        'vaziate_salamat_pezeshki',
        'azmayeshat_gharbalgari_pezeshki',
        'azmayeshat_tozihat_pezeshki',
        'mashrobat_tozihat_pezeshki',
        'dokhaniyat_tozihat_pezeshki',
        'father_hayat_pezeshki',
        'mother_hayat_pezeshki',
        'father_age_pezeshki',
        'mother_age_pezeshki',
        'sabeghe_bimari_pezeshki',
        'sabeghe_bimari_khas_pezeshki',
        'ghad_pezeshki',
        'vazn_pezeshki',
        'taghir_vazn_pezeshki',
        'moshkele_posti_pezeshki',
        'ehsas_khastegi_pezeshki',
        'moshkelate_sar_pezeshki',
        'morajee_be_cheshmpezeshki_pezeshki',
        'moshkelate_digar_pezeshki',
        'moshkelate_gardan_pezeshki',
        'moshkelate_tanafosi_pezeshki',
        'moshkelate_cxr_pezeshki',
        'moshkelate_ghalbi_oroghi_pezeshki',
        'moshkelate_navar_ghlabi_pezeshki',
        'moshkelate_dastgahe_govaresh_pezeshki',
        'moshkelate_dastgahe_azolani_pezeshki',
        'moshkelate_dastgahe_asab_markazi_pezeshki',
        'moshkelate_dastgahe_khonsaz_pezeshki',
        'moshkelate_dastgahe_ghodade_daronriz_pezeshki',
        'moshkelate_pestan_pezeshki',
        'bimari_digar_bishtar_pezeshki',
        'masraf_daro_pezeshki',
        'created_at',
        'updated_at',
    ];
}
