<?php

namespace Modules\Client\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class vip extends Model implements HasMedia
{
    use HasMediaTrait;

    
    protected $table="vip_information";

    protected $fillable = [
        'client',
        'code_vip',
        'full_name_bime_gozar',
        'father_name_bime_gozar',
        'code_meli_bime_gozar',
        'shenasname_meli_bime_gozar',
        'sodor_bime_gozar',
        'tavalod_bime_gozar',
        'tarikh_tavalod_bime_gozar',
        'jensiat_bime_gozar',
        'tahsilat_bime_gozar',
        'taahol_bime_gozar',
        'status',
        'telephone_hamrah_bime_gozar',
        'email_bime_gozar',
        'phone_bime_gozar',
        'code_posti_bime_gozar',
        'address_work_bime_gozar',
        'address_home_bime_gozar',
        'job_bime_gozar',
        'nesbate_bime_bime_shode',
        'full_name_bime_shode',
        'father_name_bime_shode',
        'code_meli_bime_shode',
        'shenasname_bime_shode',
        'sodor_bime_shode',
        'mahale_tavalod_bime_shode',
        'tarikh_tavalod_bime_shode',
        'jensiat_bime_shode',
        'sathe_tahsilat_bime_shode',
        'vaziate_taahol_bime_shode',
        'vaziate_khedmat_bime_shode',
        'telephone_hamrah_bime_shode',
        'email_bime_shode',
        'phone_bime_shode',
        'code_posti_bime_shode',
        'address_work_bime_shode',
        'address_home_bime_shode',
        'job_bime_shode',
        'full_name_bime_shode_hayat',
        'father_name_bime_shode_hayat',
        'code_meli_bime_shode_hayat',
        'shenasname_bime_shode_hayat',
        'tarikh_tavalod_bime_shode_hayat',
        'nesbat_bime_shode_hayat',
        'darsad_bime_shode_hayat',
        'info_user_bime_shode_hayat',
        'full_name_bime_shode_fot',
        'father_name_bime_shode_fot',
        'code_meli_bime_shode_fot',
        'shenasname_meli_bime_shode_fot',
        'tarikh_tavalod_bime_shode_fot',
        'nesbate_bime_shode_fot',
        'darsad_bime_shode_fot',
        'info_user_bime_shode_fot',
        'name_type_bime_shode_detail',
        'sarmaye_bime_shode_detail',
        'tarikh_shoro_bime_shode_detail',
        'modate_bime_shode_detail',
        'name_sherkat_bime_shode_detail',
        'name_vahede_sodor_bime_shode_detail',
        'name_pardakht_bime_shode_detail',
        'modat_bime_shode_detail',
        'poshesh_hadese_bime_shode_detail',
        'poshesh_amraze_khas_bime_shode_detail',
        'poshesh_amraze_khas_bime_shode_detail',
        'moafiat_pardakht_bime_shode_detail',
        'taeid_vekalat_bime_shode',
        'taeid_ghavanin',
        'token',
        'created_at',
        'updated_at',

    ];

    public function pezeshki()
    {
        return $this->belongsTo(pezeshkiVip::class,'vip_list','id');
    }
}

