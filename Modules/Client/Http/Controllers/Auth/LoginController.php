<?php

namespace Modules\Client\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Modules\Client\Entities\Client;
use Modules\Client\Entities\ClientRole;
use Modules\Client\Http\Requests\RegisterEmployer;
use Modules\Client\Http\Requests\RegisterSeeker;
use Modules\Core\Entities\Code;
use Modules\Sms\Http\Controllers\Gateway\FarazSms;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/user/panel';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:client')->except('logout');
    }

    protected function guard()
    {
        return Auth::guard('client');
    }

    protected function validateLogin(Request $request)
    {
        $request->validate([
            $this->username() => 'required|string',
            'password' => 'required|min:8',
        ]);
    }

    public function username()
    {
        $value=\request()->input('mobile');

        $field='mobile';

        // if(filter_var($value,FILTER_VALIDATE_EMAIL)){
        //     $field='email';
        // }
        // elseif (preg_match('/(9)[0-9]{9}/',$value)){
        //     $field='mobile';
        // }
        // else{
        //     $field='username';
        // }

        \request()->merge([$field=>$value]);

        return $field;
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('template.auth.login.index');
    }

    public  function login(Request $request)
    {

        $this->validate($request, [
            'mobile'           => 'required|regex:/(^((09)[0-9]{9})?$)/u|max:11',
            'password'           => 'required|min:8',
        ]);
 

        if (Auth::guard('client')->attempt([$this->username() => $request->input('mobile'), 'password' => $request->password,'is_active'=>2], $request->remember)) {

            $data=[
                'auth'=>\auth('client')->user()
            ];
            // sendCustomEmail($data,'emails.front.welcome');

            // return redirect()->intended(route('client.dashboard'));

        }
        return $this->sendFailedLoginResponse($request);


    }
    public  function verifyMobile(RegisterSeeker $request)
    {
        if(!is_null($request)){
            $code=strval(rand(1000,9999));
             $client=Client::create([
                'first_name'=>$request->first_name,
                'last_name'=>$request->last_name,
                'mobile'=>$request->mobile,
                'code'=>$code,
                 'code_expire'=>now()->addMinutes(10),
                'role'=>ClientRole::whereTitle('seeker')->firstOrFail()->id,
                'email'=>is_null($request->email) || empty(trim($request->email)) ? null: $request->email,
                'password'=>Hash::make($request->password),
                'token'=>tokenGenerate(),
            ]);



            $client->myCode()->create([
                'code'=>generateCode()
            ]);
            $data=[
                'mobile'=>$client->mobile
            ];

            sendCustomEmail($data,'emails.front.register');

            $kavengar=new FarazSms();

            $message=$client->code;

            $sms=[
                'sender'=>['+9850009227180825'],
                'mobile'=>[$client->mobile],
                'message'=>$message
            ];
            $kavengar->sendMessage($sms);

            $client->info()->create();

            $client->analyzer()->create();

            $client->company()->create();

            $client->seo()->create();

            $client->wallet()->create([
                'score'=>500
            ]);

            if(!is_null(empty($request->code))){
                $leader_id=Code::whereCode($request->code)->firstOrFail();
                $leader=Client::find($leader_id->codeable_id);
                $new_score=$leader->wallet->score + 100;
                $leader->wallet->update([
                    'score'=>$new_score
                ]);
            }

            return view('template.auth.verifies.mobile',compact('client'));
        }
        else{
            return  redirect('front.website');
        }


    }


    public  function submitMobile(Request $request,$mobile)
    {

        $client=Client::whereToken($mobile)->first();

        if($request->code==$client->code){

            $data=[
                'mobile'=>$client->mobile
            ];
            sendCustomEmail($data,'emails.front.register');

            $client->update([
                'is_active'=>2,
                'code_expire'=>now(),
            ]);


            auth('client')->loginUsingId($client->id);

            return redirect(route('client.dashboard'));
        }
        else{
            return  redirect('front.website');
        }


    }




    public  function verifyMobileEmployer(RegisterEmployer $request)
    {
        try {
            DB::beginTransaction();
            if(!is_null($request)){
                $code=strval(rand(1000,9999));
                $client=Client::create([
                    'first_name'=>$request->first_name,
                    'last_name'=>$request->last_name,
                    'mobile'=>$request->mobile,
                    'code'=>$code,
                    'code_expire'=>now()->addMinutes(10),
                    'role'=>ClientRole::whereTitle('employer')->firstOrFail()->id,
                    'email'=>is_null($request->email) || empty(trim($request->email)) ? null: $request->email,
                    'password'=>Hash::make($request->password),
                    'token'=>tokenGenerate(),
                ]);

                $client->myCode()->create([
                    'code'=>generateCode()
                ]);
                $data=[
                    'mobile'=>$client->mobile
                ];

            sendCustomEmail($data,'emails.front.register');

            $kavengar=new FarazSms();

            $message=$client->code;

            $sms=[
                'sender'=>['+9850009227180825'],
                'mobile'=>[$client->mobile],
                'message'=>$message
            ];
            $kavengar->sendMessage($sms);

                $client->info()->create();

                $client->analyzer()->create();

                $client->company()->create([
                    'name'=>$request->company
                ]);

                $client->company()->create();

                $client->seo()->create();

                $client->wallet()->create([
                    'score'=>500
                ]);

                if(!is_null(empty($request->code))){
                    $leader_id=Code::whereCode($request->code)->firstOrFail();
                    $leader=Client::find($leader_id->codeable_id);
                    $new_score=$leader->wallet->score + 100;
                    $leader->wallet->update([
                        'score'=>$new_score
                    ]);
                }
                DB::commit();
                return view('template.auth.verifies.mobile',compact('client'));
            }
            else{
                DB::rollBack();
                return  redirect('front.website');
            }
        }catch (\Exception $exception){
            DB::rollBack();
            return dd($exception->getMessage());
        }


    }
    public  function submitMobileEmployer(Request $request,$mobile)
    {

        $client=Client::whereToken($mobile)->first();

        if($request->code==$client->code){

            $data=[
                'mobile'=>$client->mobile
            ];
            sendCustomEmail($data,'emails.front.register');

            $client->update([
                'is_active'=>2,
                'code_expire'=>now(),
            ]);


            auth('client')->loginUsingId($client->id);

            return redirect(route('client.dashboard'));
        }
        else{
            return  redirect('front.website');
        }



    }


    public function logout(Request $request){

        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect(route('front.website'));
    }

    protected function loggedOut(Request $request)
    {
        //
    }



}
