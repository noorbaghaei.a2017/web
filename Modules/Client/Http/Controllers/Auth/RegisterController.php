<?php

namespace Modules\Client\Http\Controllers\Auth;

use App\Facades\Rest\Rest;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\DB;
use Modules\Client\Entities\Client;
use Modules\Client\Entities\ClientRole;
use Modules\Client\Http\Requests\RegisterClient;
use Modules\Client\Http\Requests\RegisterClientAsonkala;
use Modules\Client\Http\Requests\RegisterEmployer;
use Modules\Client\Http\Requests\RegisterSeeker;
use Modules\Core\Entities\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    public function showRegisterForm(){

        return view("template.auth.register.index");
    }


    public function register(RegisterSeeker $request){

        try {
             return redirect(route('client.verify.mobile'));


        }catch (\Exception $exception){
            return abort('500');
           
           
        }
    }


    public function registerAsonkala(RegisterClientAsonkala $request){
        try {
            DB::beginTransaction();

            $client=Client::create([
                'role'=>ClientRole::whereTitle('user')->firstOrFail()->id,
                'mobile'=>$request->mobile,
                'first_name'=>$request->first_name,
                'email'=>$request->email,
                'is_active'=>2,
                'last_name'=>$request->last_name,
                'password'=>Hash::make($request->password),
                'token'=>tokenGenerate(),
            ]);
            if($client){
                $data=[
                    'mobile'=>$request->mobile
                ];
                sendCustomEmail($data,'emails.front.register');
            }


            $client->myCode()->create([
                'code'=>generateCode()
            ]);

            $client->info()->create();

            $client->analyzer()->create();

            $client->company()->create();

            $client->seo()->create();


            $client->wallet()->create([
                'score'=>500
            ]);

            $client->cart()->create([
                'client'=>$client->id,
                'mobile'=>$client->mobile
            ]);

            auth('client')->loginUsingId($client->id);


            DB::commit();

            return redirect(route('client.dashboard'));


        }catch (\Exception $exception){
            DB::rollBack();
           
            return abort('500');
        }
    }

    public function registerClient(RegisterClient $request){
        try {
          
            DB::beginTransaction();
            $client=Client::create([
                'role'=>ClientRole::whereTitle('user')->firstOrFail()->id,
                'mobile'=>$request->mobile,
                'first_name'=>$request->first_name,
                'last_name'=>$request->last_name,
                'email'=>$request->email,
                'is_active'=>2,
                'password'=>Hash::make($request->password),
                'token'=>tokenGenerate(),
                'code_vip'=>codeVip(),
                'client_token'=>tokenClient(),
            ]);
            if($client){
                $data=[
                    'mobile'=>$request->mobile
                ];
            }
            $client->myCode()->create([
                'code'=>generateCode()
            ]);

            $client->info()->create();

            $client->analyzer()->create();

            $client->company()->create();

            $client->seo()->create();

            $client->wallet()->create([
                'score'=>500
            ]);

            auth('client')->loginUsingId($client->id);
            DB::commit();
            return redirect(route('client.dashboard'));


        }catch (\Exception $exception){
            DB::rollBack();
            return abort('500');
           
            
        }
    }

    public function showEmployerRegisterForm(){

        return view("template.auth.employer.register");
    }

    public function registerEmployer(RegisterEmployer $request){

        try {
            return redirect(route('client.verify.mobile'));


        }catch (\Exception $exception){

            return abort('500');
        }
    }

}
