<?php

namespace Modules\Client\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\ForiegnPostalCode;
use Illuminate\Http\Request;

class ForeignClientUpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
       
       
                return [
                    'email'=>'required|unique:clients,email,'.$this->client.',id',
                    'mobile'=>'required|unique:clients,mobile,'.$this->client.',id',
                    'username'=>'unique:clients,username,'.$this->client.',id',
                    'address'=>'required',
                    'postal_code'=>['required','numeric','digits:10'],
                    'image'=>'mimes:jpeg,png,jpg|max:2000',
                ];
            
           
           
           
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
