<?php

namespace Modules\Client\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VipInformation extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'=>'required',
            'job_bime_gozar'=>'required',
            'address_home_bime_gozar'=>'required',
            'address_work_bime_gozar'=>'required',
            'code_posti_bime_gozar'=>'required',
            'phone_bime_gozar'=>'required',
            'telephone_hamrah_bime_gozar'=>'required',
            'vaziate_sarbazi_bime_gozar'=>'required',
            'taahol_bime_gozar'=>'required',
            'tahsilat_bime_gozar'=>'required',
            'jensiat_bime_gozar'=>'required',
            'tarikh_tavalod_bime_gozar'=>'required',
            'tavalod_bime_gozar'=>'required',
            'sodor_bime_gozar'=>'required',
            'shenasname_meli_bime_gozar'=>'required',
            'code_meli_bime_gozar'=>'required',
            'father_name_bime_gozar'=>'required',
            'full_name_bime_gozar'=>'required',
            'taeid_vekalat_bime_shode'=>'required'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
