
@extends('core::dashboard.main')

@section('content')
   
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{__('cms.clients')}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard.website')}}">{{__('cms.dashboard')}}</a></li>
              <li class="breadcrumb-item active">{{__('cms.clients')}}</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <!-- /.container-fluid -->


  <!-- Main content -->
  <section class="content">
      <div class="row">
      

      <div class="col-md-12">
            <div class="card">
            @include('core::layout.alert-success')
              <div class="card-header">
              <a href="{{route('clients.create')}}" class="btn btn-info"><i class="fas fa-plus"></i></a>
<br>
<br>
                <h3 class="card-title">{{__('cms.clients')}}</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>{{__('cms.thumbnail')}}</th>
                      <th>{{__('cms.email')}}</th>
                      <th>{{__('cms.full_name')}}</th>
                     
                      <th style="width: 40px">{{__('cms.status')}}</th>
                      <th>{{__('cms.option')}}</th>
                    </tr>
                  </thead>
                  <tbody>
                      @foreach ($items  as $key=>$item)
                          
                     
                    <tr>
                      <td>{{++$key}}</td>
                      <td>
                      @if($item->Hasmedia('images'))

<img src="{{$item->getFirstMediaUrl('images')}}" width="50" style="object-fit:cover;height: 53px;">

                      @else
                    
<img src="{{asset('template/images/Default-welcomer.png')}}" width="50" style="object-fit:cover;height: 53px;">


                      
                      @endif
                      </td>
                      <td>{{$item->email}}</td>
                      <td>{{$item->full_name}}</td>
                   
                      <td><span class="badge bg-success">{{__('cms.active')}}</span></td>
                      <td class="text-left py-0 align-middle">
                      <div class="btn-group btn-group-sm">
                      <a href="#" class="btn btn-warning"><i class="fas fa-edit"></i></a>
                      </div>
                   
                    </tr>
                   
                   
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
              {!! $items->links() !!}
              
              </div>
            </div>
            <!-- /.card -->

          
          </div>
       
       
      </div>
    
    </section>
    <!-- /.content -->
 
  


    


@endsection








