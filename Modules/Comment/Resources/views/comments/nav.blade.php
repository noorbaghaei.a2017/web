<li>

        <a href="{{route('comments.index')}}" class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="{{config('comment.icons.comment')}}"> </i>
                              </span>
            <span class="nav-text">{{__('comment::comments.collect')}}</span>
        </a>
    </li>

