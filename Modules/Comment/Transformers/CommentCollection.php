<?php

namespace Modules\Comment\Transformers;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Modules\Comment\Entities\Comment;

class CommentCollection extends ResourceCollection
{
    public $collects=Comment::class;
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(
                function ( $item ) {
                    return new CommentResource($item);
                }
            ),
            'filed' => [
                'title','text','status','update_date','create_date'
            ],
            'public_route'=>[

               
               
            ],
            'private_route'=>
                [
                   
                    [
                        'name'=>'comments.detail',
                        'param'=>[null],
                        'icon'=>config('cms.icon.detail'),
                        'title'=>__('cms.detail'),
                        'class'=>'btn btn-primary btn-sm text-sm text-white',
                        'modal'=>true,
                        'modal_type'=>'detail',
                        'method'=>'GET',
                    ]
                ],
                
                
                'class_model'=> 'comment',
            'count' => $this->collection->count(),
            'links' => [
                'self' => 'link-value',
            ],
            'collect'=>__('comment::comments.collect'),
            'title'=>__('comment::comments.index'),
        ];
    }
}
