<?php

return [
    'name' => 'Core',
    'icons'=>[
        'dashboard'=>'ion-filing',
        'currency'=>'fa  fa-money',
        'award'=>'fa  fa-money',
        'branche'=>'fa  fa-money',
        'access'=>'fa  fa-key',
    ]
];
