<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceFieldTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_field', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('service')->unsigned();
            $table->foreign('service')->references('id')->on('list_services')->onDelete('cascade');
            $table->string('title');
            $table->string('text')->nullable();
            $table->string('excerpt')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_field');
    }
}
