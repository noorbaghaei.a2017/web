<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserServiceFieldTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_service_field', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user')->unsigned();
            $table->foreign('user')->references('id')->on('clients')->onDelete('cascade');
            $table->bigInteger('user_service')->unsigned();
            $table->foreign('user_service')->references('id')->on('user_services')->onDelete('cascade');
            $table->bigInteger('field')->unsigned();
            $table->foreign('field')->references('id')->on('service_field')->onDelete('cascade');
            $table->string('title');
            $table->string('text')->nullable();
            $table->string('excerpt')->nullable();
            $table->string('price')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_service_field');
    }
}
