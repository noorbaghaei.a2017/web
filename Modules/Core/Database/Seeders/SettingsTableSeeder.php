<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Core\Entities\Setting;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('settings')->delete();

        DB::table('templates')->delete();

        $settings=[

            [
                'name' => 'Karbin',
                'address' => 'Iran',
                'domain' => 'http://karbim.ir',
                'country' => 'German',
                'email' => 'support@karbim.ir',
                'phone' => json_encode(['','']),
                'pre_phone' => '+98',
                'mobile' => json_encode(['',''])
            ]
        ];

        $templates=[

            [
                'title' => 'karbin',
                'description' => 'karbin template',
                'status' => 1
            ]
        ];

        DB::table('settings')->insert($settings);

        DB::table('templates')->insert($templates);

        $setting=Setting::with('info')->firstOrFail();

        $setting->seo()->create([
            'title'=>'',
            'description'=>'',
            'keyword'=>'',
            'robots'=>json_encode(['index','follow']),
            'canonical'=>env('APP_URL'),
        ]);
        $setting->info()->create([
            'telegram'=>'',
            'instagram'=>'',
            'whatsapp'=>''
        ]);
    }
}
