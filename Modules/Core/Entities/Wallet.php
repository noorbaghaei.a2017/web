<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    protected $table="wallets";

    protected $fillable=['score','coin','diamonds','gold','silver'];

    public function walletable()
    {
        return $this->morphTo();
    }
}
