<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class Week extends Model
{
    protected $fillable = ['saturday','sunday','monday','tuesday','wednesday','thursday','friday'];
}
