<?php
return [
    "text-create"=>"you can create your currency",
    "text-edit"=>"you can edit your currency",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"currencies list",
    "singular"=>"currency",
    "collect"=>"currencies",
    "permission"=>[
        "currency-full-access"=>"currency full access",
        "currency-delete"=>"currency delete",
        "currency-create"=>"currency create",
        "currency-edit"=>"edit currency",
    ]
];
