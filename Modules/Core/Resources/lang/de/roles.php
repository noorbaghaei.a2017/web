<?php
return [
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"roles list",
    "singular"=>"role",
    "collect"=>"roles",
    "permission"=>[
        "role-full-access"=>"role full access",
        "role-delete"=>"role delete",
        "role-create"=>"role create",
        "role-edit"=>"edit role",
    ]
];
