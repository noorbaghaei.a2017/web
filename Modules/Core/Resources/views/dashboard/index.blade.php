@extends('core::dashboard.main')

@section('content')
   



@can('user-list')
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">{{__('cms.dashboard') }}</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">{{__('cms.dashboard')}}</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


     <!-- Main content -->
     <section class="content">
      <div class="container-fluid">
        <h5 class="mb-2">{{__('cms.new')}}</h5>
        <div class="row">
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-info"><a href="#"><i class="far fa-envelope"></i></a></span>

              <div class="info-box-content">
                <span class="info-box-text">{{__('cms.ticket')}}</span>
                <span class="info-box-number">{{$requestscount}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-success"><a href="{{route('new.index.products')}}"><i class="fas fa-shopping-cart"></i></a></span>

              <div class="info-box-content">
                <span class="info-box-text">{{__('cms.product')}}</span>
                <span class="info-box-number">{{$new_products_count}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-warning"><a href="#"><i class="fas fa-suitcase"></i></a></span>

              <div class="info-box-content">
                <span class="info-box-text">{{__('cms.forms')}}</span>
                <span class="info-box-number">{{$formscount}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
    
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-danger"><a href="{{route('new.index.clients')}}"><i class="fas fa-users"></i></a></span>

              <div class="info-box-content">
                <span class="info-box-text">{{__('cms.clients')}}</span>
                <span class="info-box-number">{{$new_clients_count}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->



     
        <!-- =========================================================== -->
 


      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->



    
@endcan

@endsection

@section('scripts')

<script src="{{asset('admin/js/plugins/chart.js/Chart.min.js')}}"></script>

<script>

$(function () {


  var donutChartCanvas = $('#donutChart').get(0).getContext('2d')
    var donutData        = {
      labels: [
          'Berlin',
          'Köln',
          'Hamburg',
          'Frankfurt am Main',
          'München',
         
      ],
      datasets: [
        {
          data: [0,17,1,0,0],
          backgroundColor : ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc'],
        }
      ]
    }
    var donutOptions     = {
      maintainAspectRatio : false,
      responsive : true,
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    new Chart(donutChartCanvas, {
      type: 'doughnut',
      data: donutData,
      options: donutOptions
    })



})

</script>

@endsection