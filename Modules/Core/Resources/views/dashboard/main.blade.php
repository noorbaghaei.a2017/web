@include('core::dashboard.sections.header')
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Preloader -->
  <div class="preloader flex-column justify-content-center align-items-center">
  @if(!$setting->Hasmedia('logo'))
    <img class="animation__shake" src="{{asset('img/no-img.gif')}}" alt="AdminLTELogo" height="60" width="60">
 @else
 <img class="animation__shake" src="{{$setting->getFirstMediaUrl('logo')}}" alt="AdminLTELogo" height="60" width="60">

 @endif

</div>

  

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
    
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      @can('user-list')
      <li class="nav-item d-none d-sm-inline-block">
        <a href="{{route('dashboard.website')}}" class="nav-link">{{__('cms.dashboard')}}</a>
      </li>
      @endcan
      @can('user-list')
      <li class="nav-item d-none d-sm-inline-block">
        <a href="{{route('contacts.admin')}}" class="nav-link">{{__('cms.contacts')}}</a>
      </li>
      @endcan
    
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Navbar Search -->
      @can('user-list')
      <li class="nav-item">
        <a class="nav-link" data-widget="navbar-search" href="#" role="button">
          <i class="fas fa-search"></i>
        </a>
        
        <div class="navbar-search-block">
          <form class="form-inline">
            <div class="input-group input-group-sm">
              <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
              <div class="input-group-append">
                <button class="btn btn-navbar" type="submit">
                  <i class="fas fa-search"></i>
                </button>
                <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                  <i class="fas fa-times"></i>
                </button>
              </div>
            </div>
          </form>
        </div>
      </li>

@endcan



@can('user-list')
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-bell"></i>
          <span class="badge badge-warning navbar-badge">{{$new_products_count + $new_jobs_count + $new_clients_count }}</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-item dropdown-header">{{$new_products_count + $new_jobs_count + $new_clients_count }}  Notifications</span>
          <div class="dropdown-divider"></div>
          @if($new_products_count > 0)
          <a href="{{route('new.index.products')}}" class="dropdown-item">
            <i class="fas fa-envelope mr-2"></i> {{$new_products_count}} {{__('cms.new_products')}}
            <span class="float-right text-muted text-sm"></span>
          </a>
          @endif
          @if($new_jobs_count > 0)
          <a href="{{route('new.index.jobs')}}" class="dropdown-item">
            <i class="fas fa-envelope mr-2"></i> {{$new_jobs_count}} {{__('cms.new_bussiness')}}
            <span class="float-right text-muted text-sm"></span>
          </a>
          @endif
          @if($new_clients_count > 0)
          <a href="{{route('new.index.clients')}}" class="dropdown-item">
            <i class="fas fa-envelope mr-2"></i> {{$new_clients_count}} {{__('cms.new_clients')}}
            <span class="float-right text-muted text-sm"></span>
          </a>
          @endif
        
         
          <div class="dropdown-divider"></div>
          <a href="{{route('jobs.index')}}" class="dropdown-item dropdown-footer">{{__('cms.read_more')}}</a>
        
        </div>
      </li>
      @endcan


        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-user"></i>
          
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          
          <div class="dropdown-divider"></div>
        
          <a href="{{route('user.profile',['user'=>auth('web')->user()->token])}}" class="dropdown-item">
            <i class="fas fa-info mr-4"></i>{{__('cms.profile')}}
            
          </a>

        
         
         
         
      </li>




      <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
          <i class="fas fa-expand-arrows-alt"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
          <i class="fas fa-th-large"></i>
        </a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->
@include('core::dashboard.sections.nav')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">


@yield('content')


  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy;  <a href="#">amin nourbaghaei</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 5.1.9
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

@include('core::dashboard.sections.footer')
