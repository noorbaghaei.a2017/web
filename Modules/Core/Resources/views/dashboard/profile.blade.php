@extends('core::dashboard.main')

@section('content')
   



    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{__('cms.profile')}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard.website')}}">{{__('cms.dashboard')}}</a></li>
              <li class="breadcrumb-item active">{{__('cms.profile')}}</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>


    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">


                @if(!$item->Hasmedia('images'))
                    <img id="show-image" class="profile-user-img img-fluid img-circle"
                       src="{{asset('template/images/Default-welcomer.png')}}"
                       alt="">

    @else
    <img id="show-image" class="profile-user-img img-fluid img-circle"
                       src="{{$item->getFirstMediaUrl('images')}}"
                       alt="">

    @endif
                 


                </div>

                <h3 class="profile-username text-center">{{$item->full_name}}</h3>

                <p class="text-muted text-center">Manager</p>

               

               

                <form  action="{{route('logout')}}" method="POST" >
                         @csrf      

                                <button class="btn btn-primary btn-block" type="submit">{{__('cms.logout')}}</button>
                            </form>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">{{__('cms.about-it')}}</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <strong><i class="fas fa-book mr-1"></i> {{__('cms.info')}}</strong>

                <p class="text-muted">
                </p>

                <hr>

                <strong><i class="fas fa-map-marker-alt mr-1"></i> {{__('cms.location')}}</strong>

                <p class="text-muted"></p>

                <hr>

                <strong><i class="fas fa-pencil-alt mr-1"></i> {{__('cms.skill')}}</strong>

                <p class="text-muted">
                  
                 
                </p>

                <hr>

                <strong><i class="far fa-file-alt mr-1"></i> {{__('cms.note')}}</strong>

                <p class="text-muted"></p>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">{{__('cms.activities')}}</a></li>
                 
                  <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">{{__('cms.setting')}}</a></li>
                  <li class="nav-item"><a class="nav-link" href="#privacy" data-toggle="tab">{{__('cms.privacy_info')}}</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
              @include('core::layout.alert-danger')
              @include('core::layout.alert-success')
                <div class="tab-content">
                  <div class="active tab-pane" id="activity">
                    
                  </div>
                 

                  <div class="tab-pane" id="settings">
                    <form action="{{route('profile.update', ['user' => $item->token])}}" class="form-horizontal" method="POST" enctype="multipart/form-data">
                    {{csrf_field()}}
                                    {{method_field('PATCH')}}
                    <div class="form-group row">
                        <label for="image" class="col-sm-2 col-form-label">{{__('image')}}</label>
                        <div class="col-sm-10">
                          <input type="file" onchange="loadFile(event)" name="image"  class="form-control" id="image" >
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">{{__('first_name')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="first_name" value="{{$item->first_name}}" class="form-control" id="inputName" placeholder="{{__('cms.first_name')}}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">{{__('last_email')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="last_name" value="{{$item->last_name}}" class="form-control" id="inputName" placeholder="{{__('cms.last_name')}}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="mobile" class="col-sm-2 col-form-label">{{__('mobile')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="mobile" value="{{$item->mobile}}" class="form-control" id="mobile" placeholder="{{__('cms.mobile')}}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputEmail" class="col-sm-2 col-form-label">{{__('cms.email')}}</label>
                        <div class="col-sm-10">
                          <input type="email" class="form-control" value="{{$item->email}}" name="email" id="inputEmail" placeholder="{{__('cms.email')}}">
                        </div>
                      </div>
                    
                     
                     
                     
                     
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" class="btn btn-danger">{{__('cms.save')}}</button>
                        </div>
                      </div>
                    </form>
                  </div>

                  <div class="tab-pane" id="privacy">
                    <form action="{{route('user.update.password',['user'=>$item->token])}}" method="POST" class="form-horizontal">
                    {{csrf_field()}}
                      <div class="form-group row">
                        <label for="current_password" class="col-sm-2 col-form-label">{{__('cms.current-password')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="current_password" value="" class="form-control" id="current_password" placeholder="{{__('cms.current-password')}}" autocomplete="off">
                        </div>
                      </div>
                    
                      <div class="form-group row">
                        <label for="new_password" class="col-sm-2 col-form-label">{{__('cms.password')}}</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="new_password" id="new_password" placeholder="{{__('cms.password')}}" autocomplete="off">
                        </div>
                      </div>
                    
                     
                     
                     
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" class="btn btn-danger">{{__('cms.save')}}</button>
                        </div>
                      </div>
                    </form>
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
    </section>





@endsection


@section('scripts')


<script>

function loadFile(e) {
	 image = $('#show-image');
    
    image.attr('src',URL.createObjectURL(event.target.files[0]));
    
};


</script>

@endsection
 
 
 
 
