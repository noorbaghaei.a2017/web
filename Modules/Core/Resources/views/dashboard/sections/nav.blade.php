
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a target="_blank" href="{{route('front.website')}}" class="brand-link">
    @if(!$setting->Hasmedia('logo'))
    <img src="{{asset('img/no-img.gif')}}" alt="{{$setting->name}}" class="brand-image img-circle elevation-3" style="opacity: .8">

    @else
    <img src="{{$setting->getFirstMediaUrl('logo')}}" alt="{{$setting->name}}" class="brand-image img-circle elevation-3" style="opacity: .8">

    @endif
      <span class="brand-text font-weight-light">{{$setting->name}}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
        
        @if(!$admin->Hasmedia('images'))
    <img src="{{asset('template/images/Default-welcomer.png')}}" class="img-circle elevation-2" alt="User Image">

    @else
 <img src="{{$admin->getFirstMediaUrl('images')}}" class="img-circle elevation-2" alt="User Image">

 @endif
       
        </div>
        <div class="info">
          <a href="{{route('user.profile',['user'=>auth('web')->user()->token])}}" class="d-block">{{$admin->first_name}}</a>
        </div>
      </div>
      @can('user-list')
      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>
      @endcan

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
               @can('user-list')
               <li class="nav-item">
                    <a href="{{route('dashboard.website')}}" class="nav-link">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p>
                        {{__('cms.dashboard')}}
                    </p>
                    </a>
          </li>
          @endcan
         
          
        

       

    





          @can('user-list')
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                {{__('cms.clients')}}
                <i class="fas fa-angle-left right"></i>
              </p>
            </a> 
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('clients.index')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{__('cms.clients')}}</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('contacts.admin')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{__('cms.admins')}}</p>
                </a>
              </li>
            
             
            </ul>
          </li> 
          @endcan


          @can('user-list')
          <li class="nav-item">
                    <a href="{{route('dashboard.setting.index')}}" class="nav-link">
                    <i class="nav-icon fas fa-cog"></i>
                    <p>
                        {{__('cms.setting')}}
                    </p>
                    </a>
          </li>
          @endcan
       
        
         
          
       
         
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>