<div class="form-group row">
@if(is_null($item))

    <div class="col-sm-12">
        <span class="text-danger">*</span>
        <label for="category" class="form-control-label">{{__('cms.category')}}  </label>
        <select dir="rtl" class="form-control" id="category" name="category" required>
            @foreach($categories as $category)
                <option value="{{$category->token}}" {{$loop->first ? "selected" : ""}}>{{$category->symbol}}</option>
            @endforeach

        </select>
    </div>

@else

    <div class="form-group row">
        <div class="col-sm-12">
            <span class="text-danger">*</span>
            <label for="category" class="form-control-label">{{__('cms.category')}}  </label>
            <select dir="rtl" class="form-control" id="category" name="category" required>
                @foreach($categories as $category)
                    <option value="{{$category->token}}" {{$category->id==$item->category ? "selected" : ""}}>{{$category->symbol}}</option>
                @endforeach

            </select>
        </div>
    </div>

@endif
</div>
