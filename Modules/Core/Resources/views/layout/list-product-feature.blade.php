@if(is_null($item))

    <div class="form-group row">

            <div class="col-sm-6">
                <span class="text-danger">*</span>
                <label for="attributes" class="form-control-label">{{__('cms.product_attribute')}}  </label>

                <select dir="rtl" class="form-control" id="attributes" name="attributes[]"  multiple>

                        @foreach($option_attributes as $attribute)
                        <optgroup label="{{$attribute->title}}">
                            @foreach($attribute->options as $a)
                        <option value="{{$a->id}}">{{$a->symbol}}</option>
                         @endforeach
                        </optgroup>
                            @endforeach
                </select>

            </div>



            <div class="col-sm-6">
                <span class="text-danger">*</span>
                <label for="properties" class="form-control-label">{{__('cms.product_property')}}  </label>
                <select dir="rtl" class="form-control" id="properties" name="properties[]"  multiple>
                @foreach($option_properties as $property)
                    <optgroup label="{{$property->title}}">
                        @foreach($property->options as $p)
                            <option value="{{$p->id}}">{{$p->symbol}}</option>
                        @endforeach
                    </optgroup>
                @endforeach
                </select>
            </div>

    </div>
@else

    <div class="form-group row">

        <div class="col-sm-6">
            <span class="text-danger">*</span>
            <label for="attributes" class="form-control-label">{{__('cms.product_attribute')}}  </label>

            <select dir="rtl" class="form-control" id="attributes" name="attributes[]"  multiple>

                @foreach($option_attributes as $attribute)
                    <optgroup label="{{$attribute->title}}">
                        @foreach($attribute->options as $a)
                            <option value="{{$a->id}}" {{in_array($a->id,($item->attributes)) ? "selected": ""}}>{{$a->symbol}}</option>
                        @endforeach
                    </optgroup>
                @endforeach
            </select>

        </div>



        <div class="col-sm-6">
            <span class="text-danger">*</span>
            <label for="properties" class="form-control-label">{{__('cms.product_property')}}  </label>
            <select dir="rtl" class="form-control" id="properties" name="properties[]"  multiple>
                @foreach($option_properties as $property)
                    <optgroup label="{{$property->title}}">
                        @foreach($property->options as $p)
                            <option value="{{$p->id}}" {{in_array($a->id,($item->properties)) ? "selected": ""}}>{{$p->symbol}}</option>
                        @endforeach
                    </optgroup>
                @endforeach
            </select>
        </div>

    </div>


@endif





