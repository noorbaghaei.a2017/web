
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h2>{{__('cms.search')}} </h2>
            </div>
            <div class="box-divider m-a-0"></div>
            <div class="box-body">
                @include('core::layout.alert-danger')
                <form role="form" method="{{$data['search_route']->method}}" action="{{route($data['search_route']->name)}}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row">

                        @foreach(collect($data['search_route']->filter)->toArray() as $item)
                        <div class="col-sm-3">
                            <label for="{{$item}}" class="form-control-label">{{__('cms.'.$item)}}  </label>
                            <input type="text" value="{{isset($request->$item) ? $request->$item : ""}}" name="{{$item}}" class="form-control" id="{{$item}}"  autocomplete="off">
                        </div>
                        @endforeach


                    </div>

                    <div class="form-group row m-t-md">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary btn-sm text-sm">{{__('cms.search')}} </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

