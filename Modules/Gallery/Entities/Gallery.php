<?php

namespace Modules\Gallery\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\Analyzer;
use Modules\Core\Entities\Translate;
use Modules\Core\Entities\User;
use Modules\Core\Helper\Trades\TimeAttribute;
use Modules\Question\Entities\Question;
use Modules\Seo\Entities\Seo;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Spatie\Tags\HasTags;

class Gallery extends Model implements HasMedia
{
    use HasTags,TimeAttribute,HasMediaTrait;

    protected $fillable = ['title','parent','excerpt','galleryable_id','galleryable_type','token','order','user','status','category','text'];

    public function getRouteKeyName()
    {
        return multiRouteKey();
    }

    public function analyzer()
    {
        return $this->morphOne(Analyzer::class, 'analyzerable');
    }
    public function questions()
    {
        return $this->morphMany(Question::class, 'questionable');
    }
    public function galleries()
    {
        return $this->morphMany(Gallery::class, 'galleryable');
    }
    public function seo()
    {
        return $this->morphOne(Seo::class, 'seoable');
    }
    public function translates()
    {
        return $this->morphMany(Translate::class, 'translateable');
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function galleryable()
    {
        return $this->morphTo();
    }
    public function user_info()
    {
        return $this->belongsTo(User::class,'user','id');
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('medium')
            ->width(250)
            ->height(250)
            ->keepOriginalImageFormat()
            ->performOnCollections(config('cms.collection-image'));

    }
}
