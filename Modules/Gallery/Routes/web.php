<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::group(["prefix"=>config('cms.prefix-admin'),"middleware"=>["auth:web"]],function() {
    Route::resource('/galleries', 'GalleryController')->only('create','store','destroy','update','index','edit');

    Route::group(["prefix"=>'search'], function () {
        Route::post('/galleries', 'GalleryController@search')->name('search.galleries');
    });

    Route::group(["prefix"=>'gallery/categories'], function () {
        Route::get('/', 'GalleryController@categories')->name('gallery.categories');
        Route::get('/create', 'GalleryController@categoryCreate')->name('gallery.category.create');
        Route::post('/store', 'GalleryController@categoryStore')->name('gallery.category.store');
        Route::get('/edit/{category}', 'GalleryController@categoryEdit')->name('gallery.category.edit');
        Route::patch('/update/{category}', 'GalleryController@categoryUpdate')->name('gallery.category.update');

    });

    Route::group(["prefix"=>'gallery/questions'], function () {
        Route::get('/{gallery}', 'GalleryController@question')->name('gallery.questions');
        Route::get('/create/{gallery}', 'GalleryController@questionCreate')->name('gallery.question.create');
        Route::post('/store/{gallery}', 'GalleryController@questionStore')->name('gallery.question.store');
        Route::delete('/destroy/{question}', 'GalleryController@questionDestroy')->name('gallery.question.destroy');
        Route::get('/edit/{gallery}/{question}', 'GalleryController@questionEdit')->name('gallery.question.edit');
        Route::patch('/update/{question}', 'GalleryController@questionUpdate')->name('gallery.question.update');
    });

});
