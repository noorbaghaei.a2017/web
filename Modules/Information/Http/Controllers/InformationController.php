<?php

namespace Modules\Information\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Article\Transformers\ArticleCollection;
use Modules\Core\Entities\Category;
use Modules\Core\Entities\Info;
use Modules\Core\Http\Controllers\HasCategory;
use Modules\Core\Http\Controllers\HasQuestion;
use Modules\Core\Http\Requests\CategoryRequest;
use Modules\Information\Entities\Information;
use Modules\Information\Entities\Repository\InformationRepositoryInterface;
use Modules\Information\Http\Requests\InformationRequest;
use Modules\Information\Transformers\InformationCollection;
use Modules\Information\Transformers\InformationResource;
use Modules\Question\Entities\Question;
use Modules\Question\Http\Requests\QuestionRequest;

class InformationController extends Controller
{
    use HasCategory,HasQuestion;

    protected $entity;
    protected $class;
    private $repository;



//category

    protected $route_categories_index='information::categories.index';
    protected $route_categories_create='information::categories.create';
    protected $route_categories_edit='information::categories.edit';
    protected $route_categories='information.categories';


//question

    protected $route_questions_index='information::questions.index';
    protected $route_questions_create='information::questions.create';
    protected $route_questions_edit='information::questions.edit';
    protected $route_questions='informations.index';


//notification

    protected $notification_store='information::informations.store';
    protected $notification_update='information::informations.update';
    protected $notification_delete='information::informations.delete';
    protected $notification_error='information::informations.error';


    public function __construct(InformationRepositoryInterface $repository)
    {
        $this->entity=new Information();
        $this->class=Information::class;
        $this->repository=$repository;

        $this->middleware('permission:information-list')->only('index');
        $this->middleware('permission:information-create')->only(['create','store']);
        $this->middleware('permission:information-edit' )->only(['edit','update']);
        $this->middleware('permission:information-delete')->only(['destroy']);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {

            $items=$this->repository->getAll();

            $result = new InformationCollection($items);

            $data= collect($result->response()->getData())->toArray();

            return view('core::response.index',compact('data'));
        }
        catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
                !isset($request->title) &&
                !isset($request->slug)
            ){
                $items=$this->repository->getAll();

                $result = new InformationCollection($items);

                $data= collect($result->response()->getData())->toArray();

                return view('core::response.index',compact('data'));
            }
            $items=$this->entity
                ->where("title",'LIKE','%'.trim($request->title).'%')
                ->where("slug",'LIKE','%'.trim($request->slug).'%')
                ->paginate(config('cms.paginate'));

            $result = new InformationCollection($items);

            $data= collect($result->response()->getData())->toArray();

            return view('core::response.index',compact('data'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            $categories=Category::latest()->where('model',Information::class)->get();
            return view('information::informations.create',compact('categories'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param InformationRequest $request
     * @return Response
     */
    public function store(InformationRequest $request)
    {
        try {
            DB::beginTransaction();
            $this->entity->user=auth('web')->user()->id;
            $this->entity->title=$request->input('title');
            $this->entity->excerpt=$request->input('excerpt');
            $this->entity->refer_link=$request->input('refer_link');
            $this->entity->category=Category::whereToken($request->input('category'))->first()->id;
            $this->entity->text=$request->input('text');
            $this->entity->token=tokenGenerate();

            $saved=$this->entity->save();

            $this->entity->seo()->create([
                'title'=>$request->input('title-seo'),
                'description'=>$request->input('description-seo'),
                'keyword'=>$request->input('keyword-seo'),
                'canonical'=>$request->input('canonical-seo'),
                'robots'=>(is_null(json_encode($request->input('robots'))) ? [] : json_encode($request->input('robots'))),
                'author'=>$request->input('author-seo'),
                'publisher'=>$request->input('publisher-seo'),
            ]);
            $this->entity->analyzer()->create();
            $this->entity->attachTags($request->input('tags'));
            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            if(!$saved){
                DB::rollBack();
                return redirect()->back()->with('error',__('information::informations.error'));
            }else{
                DB::commit();
                return redirect(route("informations.index"))->with('message',__('information::informations.store'));
            }



        }catch (\Exception $exception){
            DB::rollBack();
            sendMailErrorController($exception);
            return abort('500');

        }
    }


    /**
     * Show the form for editing the specified resource.
     * @param $token
     * @return Response
     */
    public function edit($token)
    {
        try {
            $categories=Category::latest()->where('model',Information::class)->get();
            $item=$this->entity->with('tags')->whereToken($token)->first();
            return view('information::informations.edit',compact('item','categories'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param InformationRequest $request
     * @param $token
     * @return void
     */
    public function update(InformationRequest $request, $token)
    {
        try {
            DB::beginTransaction();
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            $updated=$this->entity->update([
                'user'=>auth('web')->user()->id,
                "slug"=>null,
                "title"=>$request->input('title'),
                "refer_link"=>$request->input('refer_link'),
                "category"=>Category::whereToken($request->input('category'))->first()->id,
                "excerpt"=>$request->input('excerpt'),
                "text"=>$request->input('text'),
            ]);
            $this->entity->seo()->update([
                'title'=>$request->input('title-seo'),
                'description'=>$request->input('description-seo'),
                'keyword'=>$request->input('keyword-seo'),
                'canonical'=>$request->input('canonical-seo'),
                'robots'=>(is_null(json_encode($request->input('robots'))) ? [] : json_encode($request->input('robots'))),
                'author'=>$request->input('author-seo'),
                'publisher'=>$request->input('publisher-seo'),
            ]);
            $this->entity->syncTags($request->input('tags'));
            if($request->has('image')){
                destroyMedia($this->entity,config('cms.collection-image'));
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            if(!$updated){
                DB::rollBack();
                return redirect()->back()->with('error',__('information::informations.error'));
            }else{
                DB::commit();
                return redirect(route("informations.index"))->with('message',__('information::informations.update'));
            }

        }catch (\Exception $exception){
            DB::rollBack();
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param $token
     * @return Response
     */
    public function destroy($token)
    {
        try {
            DB::beginTransaction();
            $this->entity = $this->entity->whereToken($token)->firstOrFail();
            if ($this->entity->Hasmedia(config('cms.collection-image'))) {
                destroyMedia($this->entity, config('cms.collection-image'));
            }
            $this->entity->seo()->delete();
            $deleted = $this->entity->delete();

            if (!$deleted) {
                DB::rollBack();
                return redirect()->back()->with('error', __('information::informations.error'));
            } else {
                DB::commit();
                return redirect(route("informations.index"))->with('message', __('information::informations.delete'));
            }

        } catch (\Exception $exception) {
            DB::rollBack();
            sendMailErrorController($exception);
            return abort('500');
        }
    }


    public  function languageShow(Request $request,$lang,$token){
        $item=Information::with('translates')->where('token',$token)->first();
        return view('information::informations.language',compact('item','lang'));

    }
    public  function languageUpdate(Request $request,$lang,$token){

        DB::beginTransaction();
        $item=Information::with('translates')->where('token',$token)->first();
        if( $item->translates->where('lang',$lang)->first()){
            $changed=$item->translates->where('lang',$lang)->first()->update([
                'symbol'=>$request->symbol,
                'slug'=>$request->slug,
                'title'=>$request->title,
                'text'=>$request->text,
                'description_seo'=>$request->description_seo,
                'title_seo'=>$request->title_seo,
                'keyword_seo'=>$request->keyword_seo,
                'canonical_seo'=>$request->canonical_seo,
            ]);
        }else{
            $changed=$item->translates()->create([
                'symbol'=>$request->symbol,
                'slug'=>$request->slug,
                'title'=>$request->title,
                'text'=>$request->text,
                'description_seo'=>$request->description_seo,
                'title_seo'=>$request->title_seo,
                'keyword_seo'=>$request->keyword_seo,
                'canonical_seo'=>$request->canonical_seo,
                'lang'=>$lang
            ]);
        }


        if(!$changed){
            DB::rollBack();
            return redirect()->back()->with('error',__('information::informations.error'));
        }else{
            DB::commit();
            return redirect(route("informations.index"))->with('message',__('information::informations.update'));
        }


    }
}
