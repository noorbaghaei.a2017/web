<?php

namespace Modules\Member\Entities;

use Illuminate\Database\Eloquent\Model;

class MemberBot extends Model
{
    protected $table="member_bot";

    protected $fillable = ['title','description','botable_id','botable_type','count','member'];

    public function botable()
    {
        return $this->morphTo();
    }
}
