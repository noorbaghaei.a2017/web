<!-- Left Sidebar -->
<aside id="leftsidebar" class="sidebar">
    <div class="navbar-brand">
        <button class="btn-menu ls-toggle-btn" type="button"><i class="zmdi zmdi-menu"></i></button>
        <a href="index.html">
            @if(!$setting->Hasmedia('logo'))
                <img src="{{asset('assets/images/logo.png')}}" alt="{{config('app.name')}}"  width="60">
            @else
                <img src="{{$setting->getFirstMediaUrl('logo')}}" alt="{{config('app.name')}}" width="60">
            @endif
            <span class="m-l-10">{{config('app.name')}}</span></a>
    </div>
    <div class="menu">
        <ul class="list">
            <li>
                <div class="user-info text-right">
                    <div class="detail">
                        <h4>{{$member->full_name}}</h4>
                        <small>{{$member->role_name}}</small>
                    </div>
                    <a class="image" href="#">
                        @if(!$member->Hasmedia('images'))
                            <img src="{{asset('assets/images/logo.png')}}" alt="{{$member->full_name}}" title="{{$member->full_name}}" width="60">
                        @else
                            <img src="{{$member->getFirstMediaUrl('images')}}"  alt="{{$member->full_name}}" title="{{$member->full_name}}" width="60">
                        @endif
                    </a>

                </div>
            </li>
            <li class="active open"><a href="{{route('member.dashboard')}}"><span>{{__('cms.dashboard')}}</span><i class="zmdi zmdi-home"></i></a></li>
            <li class="active open"><a href="{{route('member.logout')}}"><span>{{__('cms.logout')}}</span><i class="zmdi zmdi-power"></i></a></li>
        </ul>
    </div>
</aside>
