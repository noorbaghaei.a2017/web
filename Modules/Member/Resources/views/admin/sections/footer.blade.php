
<!-- Jquery Core Js -->
<script src="{{asset('member/bundles/libscripts.bundle.js')}}"></script> <!-- Lib Scripts Plugin Js ( jquery.v3.2.1, Bootstrap4 js) -->
<script src="{{asset('member/bundles/vendorscripts.bundle.js')}}"></script> <!-- slimscroll, waves Scripts Plugin Js -->

<script src="{{asset('member/bundles/jvectormap.bundle.js')}}"></script> <!-- JVectorMap Plugin Js -->
<script src="{{asset('member/bundles/sparkline.bundle.js')}}"></script> <!-- Sparkline Plugin Js -->
<script src="{{asset('member/bundles/c3.bundle.js')}}"></script>

<script src="{{asset('member/bundles/mainscripts.bundle.js')}}"></script>
<script src="{{asset('member/js/pages/index.js')}}"></script>

@yield('script')

</body>


</html>
