<?php
return [
    "text-create"=>"you can create your Menüs",
    "text-edit"=>"you can edit your Menüs",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"Menüs list",
    "error"=>"error",
    "singular"=>"Menüs",
    "collect"=>"Menüs",
    "permission"=>[
        "menu-full-access"=>"menu full access",
        "menu-list"=>"Menüs list",
        "menu-delete"=>"Menüs delete",
        "menu-create"=>"Menüs create",
        "menu-edit"=>"edit Menüs",
    ]
];
