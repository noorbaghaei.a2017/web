@include('core::layout.modules.index',[

'title'=>__('menu::menus.index'),
'items'=>$items,
'parent'=>'menu',
'model'=>'menu',
'directory'=>'menus',
'collect'=>__('menu::menus.collect'),
'singular'=>__('menu::menus.singular'),
'create_route'=>['name'=>'menus.create'],
'edit_route'=>['name'=>'menus.edit','name_param'=>'menu'],
'destroy_route'=>['name'=>'menus.destroy','name_param'=>'menu'],
 'search_route'=>true,
 'setting_route'=>true,
'pagination'=>true,
'datatable'=>[
__('cms.title')=>'title',
__('cms.symbol')=>'symbol',
__('cms.href')=>'href',
__('cms.order')=>'order',
  __('cms.update_date')=>'AgoTimeUpdate',
__('cms.create_date')=>'TimeCreate',
],
    'detail_data'=>[
__('cms.title')=>'symbol',
__('cms.symbol')=>'symbol',
__('cms.href')=>'href',
__('cms.order')=>'order',
    __('cms.create_date')=>'created_at',
__('cms.update_date')=>'updated_at',
],


])





