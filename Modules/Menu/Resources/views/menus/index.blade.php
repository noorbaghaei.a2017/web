
@extends('core::dashboard.main')

@section('content')
   
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{__('cms.menus')}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard.website')}}">{{__('cms.dashboard')}}</a></li>
              <li class="breadcrumb-item active">{{__('cms.menus')}}</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <!-- /.container-fluid -->


  <!-- Main content -->
  <section class="content">
      <div class="row">
      
      <div class="col-md-12">

      <div class="card">
              <div class="card-header">
              <a href="{{route('menus.create')}}" class="btn btn-info"><i class="fas fa-plus"></i></a>
<br>
<br>
                <h3 class="card-title">{{__('cms.menus')}}</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              @include('core::layout.alert-danger')
              @include('core::layout.alert-success')
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr><th colspan="8">{{__('cms.menus')}}</th></tr>

                      <tr>
                      <th style="width: 10px">#</th>
                      <th>{{__('cms.title')}}</th>
                      <th>{{__('cms.symbol')}}</th>
                      <th>{{__('cms.order')}}</th>
                      <th>{{__('cms.href')}}</th>
                      <th>{{__('cms.location')}}</th>
                      <th>{{__('cms.status')}}</th>
                      <th>{{__('cms.option')}}</th>

                      </tr>
                  </thead>
                  <tbody>
                  @foreach ($items as $key=>$item)
                 
                  <tr>
                  <td>{{++$key}}</td>
                      <td>{{$item->title}}</td>
                      <td>{{$item->symbol}}</td>
                      <td>{{$item->order}}</td>
                      <td>{{$item->href}}</td>
                      <td><a target="_blank" href="{{route('front.website')}}{{$item->href}}">{{__('cms.preview')}}</a></td>
                      <td><span class="{{$item->status==1 ? 'badge bg-success' : 'badge bg-danger'}}">{{statusTitleMenu($item->status)}}</span></td>

                     <td>

                     <div class="btn-group btn-group-sm">
                     <a href="{{route('menus.edit',['menu'=>$item->token])}}" class="btn btn-warning"><i class="fas fa-edit"></i></a>
                        
                      </div>
                    </td>
</tr>

                  @endforeach
                    
                 
                 
                  </tbody>
                  <tfoot>
                  <tr>
                  <th style="width: 10px">#</th>
                      <th>{{__('cms.title')}}</th>
                      <th>{{__('cms.symbol')}}</th>
                      <th>{{__('cms.order')}}</th>
                      <th>{{__('cms.href')}}</th>
                      <th>{{__('cms.location')}}</th>
                      <th>{{__('cms.status')}}</th>
                      <th>{{__('cms.option')}}</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>


     
        </div>
      </div>
    
    </section>
    <!-- /.content -->
 
  


    


@endsection


@section('scripts')

<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>



@endsection










