<?php

namespace Modules\Order\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Client\Entities\Client;
use Modules\Core\Helper\Trades\TimeAttribute;
use Modules\Order\Helper\OrderHelper;
use Modules\Payment\Entities\Payment;

class Order extends Model
{


    use TimeAttribute;

    protected $fillable = ['order_id','total_price','status'];



    public function payment(){

//        return $this->belongsTo(Payment::class,'order','id')->with('info_client');
        return $this->belongsTo(Payment::class,'id','order');
    }

    public function getPriceAttribute(){
        return number_format($this->total_price);
    }

    public function getTitlePaymentAttribute(){
        return $this->payment->title;
    }

    public function getFullNameUserAttribute(){
        return $this->payment->info_client->first_name . " ".$this->payment->info_client->last_name;
    }
    public function getMobileUserAttribute(){
        return $this->payment->info_client->mobile ;
    }

    public function getEmailUserAttribute(){
        return $this->payment->info_client->email ;
    }

    public  function getShowStatusAttribute(){

        return OrderHelper::status($this->payment->status);
    }
}
