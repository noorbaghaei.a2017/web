<?php
return [
    "text-create"=>"you can create your Bestellung",
    "text-edit"=>"you can edit your Bestellung",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"Bestellung list",
    "error"=>"error",
    "singular"=>"Bestellung",
    "collect"=>"Bestellung",
    "permission"=>[
        "order-full-access"=>"Bestellung full access",
        "order-list"=>"Bestellung list",
        "order-delete"=>"Bestellung delete",
        "order-create"=>"Bestellung create",
        "order-edit"=>"edit Bestellung",
    ]


];
