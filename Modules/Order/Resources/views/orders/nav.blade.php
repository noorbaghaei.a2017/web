
    <li>
        <a href="{{route('orders.index')}}" class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="{{config('order.icons.order')}}"></i>
                              </span>

            <span class="nav-text">{{__('order::orders.collect')}}</span>
        </a>
    </li>

