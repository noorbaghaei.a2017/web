<?php
return [
    "text-create"=>"you can create your Seiten",
    "text-edit"=>"you can edit your Seiten",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "error"=>"error",
    "index"=>"Seiten list",
    "singular"=>"Seiten",
    "collect"=>"Seiten",
    "permission"=>[
        "page-full-access"=>"Seiten full access",
        "page-list"=>"Seiten list",
        "page-delete"=>"Seiten delete",
        "page-create"=>"Seiten create",
        "page-edit"=>"edit Seiten",
    ]
];
