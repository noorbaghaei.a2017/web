<?php


namespace Modules\Payment\Http\Controllers\Facade;


use Illuminate\Support\Facades\Facade;

class MelatFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'melat';
    }

}
