<?php
return [
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"customers listc",
    "singular"=>"payment",
    "collect"=>"payments",
    "permission"=>[
        "payment-full-access"=>"payment full access",
        "payment-list"=>"payments list",
        "payment-delete"=>"payment delete",
        "payment-create"=>"payment create",
        "payment-edit"=>"edit payment",
    ]
];
