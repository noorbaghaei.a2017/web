<?php


namespace Modules\Plan\Entities\Repository;


use Modules\Plan\Entities\Plan;

class PlanRepository implements PlanRepositoryInterface
{

    public function getAll()
    {
        return Plan::latest()->get();
    }
}
