@include('core::layout.modules.category.create',[

    'title'=>__('core::categories.create'),
    'parent'=>'portfolio',
    'model'=>'portfolio',
    'directory'=>'portfolios',
    'collect'=>__('core::categories.collect'),
    'singular'=>__('core::categories.singular'),
   'store_route'=>['name'=>'portfolio.category.store'],

])








