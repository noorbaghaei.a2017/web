<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user')->nullable();
            $table->foreign('user')->references('id')->on('clients')->onDelete('cascade');
            $table->unsignedBigInteger('category');
            $table->foreign('category')->references('id')->on('categories')->onDelete('cascade');
            $table->unsignedBigInteger('store_category')->nullable();
            $table->foreign('store_category')->references('id')->on('categories')->onDelete('cascade');
            $table->bigInteger('currency')->nullable()->unsigned();
            $table->foreign('currency')->references('id')->on('currencies')->onDelete('cascade');
            $table->string('title');
            $table->string('code')->unique();
            $table->string('inventory')->nullable();
            $table->integer('parent')->default(0);
            $table->string('level')->default(0);
            $table->string('sell')->default(0);
            $table->string('slug');
            $table->string('postal_code');
            $table->string('country');
            $table->string('city');
            $table->string('mobile')->nullable();
            $table->text('address')->nullable();
            $table->json('option')->nullable();
            $table->tinyInteger('special')->default(0)->nullable();
            $table->json('ability')->nullable();
            $table->longText('text')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->string('excerpt')->nullable();
            $table->string('token')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
