<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Core\Entities\Category;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Routing\Controller;

class ProductCategoryController extends Controller
{
    public  function languageShow(Request $request,$lang,$token){
        $item=Category::with('translates')->where('token',$token)->where('model','Modules\Product\Entities\Product')->first();
        return view('product::categories.language',compact('item','lang'));

    }
    public  function languageUpdate(Request $request,$lang,$token){

        try{

           
        DB::beginTransaction();
        $item=Category::with('translates')->where('token',$token)->where('model','Modules\Product\Entities\Product')->first();
        if( $item->translates->where('lang',$lang)->first()){
            $changed=$item->translates->where('lang',$lang)->first()->update([
                'title'=>$request->title
            ]);
        }else{
            $changed=$item->translates()->create([
                'title'=>$request->title,
                'lang'=>$lang
            ]);
        }


        if(!$changed){
            DB::rollBack();
            return redirect()->back()->with('error',__('product::category_job.error'));
        }else{
            DB::commit();
            return redirect(route("product.categories"))->with('message',__('product::category_job.update'));
        }

        }catch(Exeption $exception){
            return redirect()->back()->with('error',__('product::category_job.error'));

        }

    }
}
