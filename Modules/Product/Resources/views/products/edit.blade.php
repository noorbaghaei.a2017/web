@extends('core::dashboard.main')

@section('content')
   



    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{__('cms.edit')}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard.website')}}">{{__('cms.dashboard')}}</a></li>
              <li class="breadcrumb-item active">{{__('cms.edit')}}</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>


    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">


                @if(!$item->Hasmedia('images'))
                    <img id="show-image" class="profile-user-img img-fluid img-circle"
                       src="{{asset('template/images/no-image.jpg')}}"
                       alt="" style="border-radius:unset;width:100%;">

    @else
    <img id="show-image" class="profile-user-img img-fluid img-circle"
                       src="{{$item->getFirstMediaUrl('images')}}"
                       alt="" style="border-radius:unset;width:100%;">

    @endif
                 


                </div>

                <h3 class="profile-username text-center">{{$item->title}}</h3>

                <p class="text-muted text-center">Manager</p>

               

               

             
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">About Me</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
               
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                 
                  <li class="nav-item"><a class="nav-link active" href="#settings" data-toggle="tab">{{__('cms.setting_info')}}</a></li>
                  <li class="nav-item"><a class="nav-link" href="#privacy" data-toggle="tab">{{__('cms.privacy_info')}}</a></li>
                  <li class="nav-item"><a class="nav-link" href="#translate" data-toggle="tab">{{__('cms.translate_lang')}}</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
              @include('core::layout.alert-danger')
              @include('core::layout.alert-success')
                <div class="tab-content">
                 
                 

                  <div class="tab-pane active" id="settings">
                    <form action="{{route('products.update', ['product' => $item->token])}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    {{method_field('PATCH')}}
                    <div class="form-group row">
                    <div class="col-sm-10">
                      <input name="image" type="file" onchange="loadFile(event)" class="custom-file-input" id="customFile">
                      <label class="custom-file-label" for="customFile">Choose Image</label>
                    </div>
                    </div>  
                    
                    
                      <div class="form-group row">
                        <label for="title" class="col-sm-2 col-form-label">{{__('cms.title')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="title" value="{{$item->title}}" class="form-control" id="title" placeholder="{{__('cms.title')}}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="text" class="col-sm-2 col-form-label">{{__('cms.text')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="text" value="{{$item->text}}" class="form-control" id="text" placeholder="{{__('cms.text')}}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="excerpt" class="col-sm-2 col-form-label">{{__('cms.excerpt')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="excerpt" value="{{$item->excerpt}}" class="form-control" id="excerpt" placeholder="{{__('cms.excerpt')}}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="price" class="col-sm-2 col-form-label">{{__('cms.price')}}</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" value="{{$item->price->amount}}" name="price" id="price" placeholder="{{__('cms.price')}}">
                        </div>
                      </div>
                     
                     
                    
                      <div class="form-group row">
                        <label for="status" class="col-sm-2 col-form-label">{{__('cms.status')}}</label>
                        <div class="col-sm-10">
                        <select name="status" class="form-control">
                        <option  value="1" {{$item->status==1 ? 'selected' : ''}}>{{__('cms.active')}}</option>
                                        <option  value="0" {{$item->status==0 ? 'selected' : ''}}>{{__('cms.inactive')}}</option>
                        </select>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="special" class="col-sm-2 col-form-label">{{__('cms.special')}}</label>
                        <div class="col-sm-10">
                        <select name="special" class="form-control">
                        <option  value="1" {{$item->special==1 ? 'selected' : ''}}>{{__('cms.active')}}</option>
                                        <option  value="0" {{$item->special==0 ? 'selected' : ''}}>{{__('cms.inactive')}}</option>
                        </select>
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="category" class="col-sm-2 col-form-label">{{__('cms.category')}}</label>
                        <div class="col-sm-10">
                        <select name="category" class="form-control">
                        <option  value="-1" {{$item->category==0 ? 'selected' : ''}}>{{__('cms.self')}}</option>    

@foreach($categories as $list)
                    <option value="{{$list->token}}" {{$item->category==$list->id ? 'selected' : ''}}>{!! convert_lang($list,LaravelLocalization::getCurrentLocale(),'title') !!}</option>
                @endforeach
                        </select>
                        </div>
                      </div>
                     
                      <div class="form-group row">
                        <label for="postal_code" class="col-sm-2 col-form-label">{{__('cms.postal_code')}}</label>
                        <div class="col-sm-10">
                          <input id="postal_code" onkeyup="loadCity(event)" type="text" name="postal_code" value="{{$item->postal_code}}" class="form-control" id="postal_code" placeholder="{{__('cms.postal_code')}}">
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="city" class="col-sm-2 col-form-label">{{__('cms.city')}}</label>
                        <div class="col-sm-10">
                        <select id="city"  name="city" class="form-control">
                       
                       <option value="{{$item->city}}">{{$item->city}}</option>
                        
                        </select>
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="address" class="col-sm-2 col-form-label">{{__('cms.address')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="address" value="{{$item->address}}" class="form-control" id="address" placeholder="{{__('cms.address')}}">
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="mobile" class="col-sm-2 col-form-label">{{__('cms.mobile')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="mobile" value="{{$item->mobile}}" class="form-control" id="mobile" placeholder="{{__('cms.mobile')}}">
                        </div>
                      </div>


                    
                    
                     
                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">{{__('cms.clients')}}</label>
                        <div class="col-sm-10">
                        <select name="client" class="form-control">
                        <option value="0" {{$item->user==null ? 'selected' : ''}}>{{__('cms.empty')}}</option>

@foreach($clients as $client)
<option value="{{$client->token}}" {{$item->user==$client->id ? 'selected' : ''}}>{{$client->full_name}}</option>
@endforeach
                        </select>
                        </div>
                      </div>
                    
                     
                     
                     
                     
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" class="btn btn-danger">{{__('cms.save')}}</button>
                        </div>
                      </div>
                    </form>
                  </div>

                  <div class="tab-pane" id="privacy">
                   
                  </div>


                  <div class="tab-pane" id="translate">
                  <form action="#" method="POST">
                  @csrf
                            @method('PATCH')
                  <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">{{__('cms.title_fa')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="title" value="{{isset($item->translates->where('lang','fa')->first()->title) ? $item->translates->where('lang','fa')->first()->title : ''}}" class="form-control" id="inputName" placeholder="{{__('cms.title_fa')}}" autocomplete="off">
                        </div>
                      </div>
                    
                      
                      
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" class="btn btn-danger">{{__('cms.save')}}</button>
                        </div>
                      </div>

                      </form>
                      <form action="#" method="POST">
                  @csrf
                  @method('PATCH')
                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">{{__('cms.title_en')}}</label>
                        <div class="col-sm-10">
                          <input type="text" name="title" value="{{isset($item->translates->where('lang','en')->first()->title) ? $item->translates->where('lang','en')->first()->title : ''}}" class="form-control" id="inputName" placeholder="{{__('cms.title_en')}}" autocomplete="off">
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" class="btn btn-danger">{{__('cms.save')}}</button>
                        </div>
                      </div>

                      </form>

                   </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
    </section>





@endsection




@section('scripts')


<script>

function loadFile(e) {
	 image = $('#show-image');
    
    image.attr('src',URL.createObjectURL(event.target.files[0]));
    
};



function loadCity(e) {

var myInput = document.getElementById("postal_code");
    


if(myInput.value.length==5){

    $code=$("#postal_code").val();


$.ajax({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    type:'POST',
    contentType:'application/json; charset=utf-8',
    url:'/ajax/load/city/postalcode/'+$code,
    data: { field1:$code} ,
    beforeSend:function(){

        $('#city').hide();
    
},
    success: function (response) {

       
    if(response.data.status){

        console.log(response.data);
        $('#city').empty();
       
            $('#city').append("<option value='"+response.data.result[0].fields.plz_name+"' selected>"+response.data.result[0].fields.plz_name+"</option>");
        

    }
    else{
        console.log('no');
    }
    },
    error: function (xhr,ajaxOptions,thrownError) {
    
    console.log(xhr,ajaxOptions,thrownError);
        // $("#result-ajax").empty()
        //     .append("<span>"+msg+"</span>");
    },
    complete:function(){
        $('#city').show();
}

});

}
}



</script>

@endsection
 
 
 
