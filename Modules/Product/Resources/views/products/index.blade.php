
@extends('core::dashboard.main')

@section('content')
   
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{__('cms.products')}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('dashboard.website')}}">{{__('cms.dashboard')}}</a></li>
              <li class="breadcrumb-item active">{{__('cms.products')}}</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <!-- /.container-fluid -->


  <!-- Main content -->
  <section class="content">
      <div class="row">
      
      <div class="col-md-12">

      <div class="card">
              <div class="card-header">
              <a href="{{route('products.create')}}" class="btn btn-info"><i class="fas fa-plus"></i></a>
<br>
<br>
                <h3 class="card-title">{{__('cms.products')}}</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              @include('core::layout.alert-danger')
              @include('core::layout.alert-success')
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr><th colspan="9">{{__('cms.products')}}</th></tr>

                      <tr>
                      <th style="width: 10px">#</th>
                      <th>{{__('cms.thumbnail')}}</th>
                      <th>{{__('cms.title')}}</th>
                      <th>{{__('cms.price')}}</th>
                      <th>{{__('cms.special')}}</th>
                      <th>{{__('cms.token')}}</th>
                      <th>{{__('cms.status')}}</th>
                      <th>{{__('cms.option')}}</th>

                      </tr>
                  </thead>
                  <tbody>
                  @foreach ($items as $key=>$item)
                  <tr>
                      <td>{{++$key}}</td>
                      <td>
                      @if($item->Hasmedia('images'))

<img src="{{$item->getFirstMediaUrl('images')}}" width="50" style="object-fit:cover;height: 53px;">

                      @else
                      
<img src="{{asset('template/images/no-image.jpg')}}" width="50" style="object-fit:cover;height: 53px;">

                      @endif
                      </td>
                      <td>{{$item->title}}</td>    
                      <td>{{$item->price->amount}}€</td>     
                      <td><span class="{{$item->special==1 ? 'badge bg-success' : 'badge bg-danger'}}">{{statusTitleMenu($item->special)}}</span></td>
                      <td>{{$item->token}}</td>  
                      <td><span class="{{$item->status==2 ? 'badge bg-success' : 'badge bg-danger'}}">{{statusTitleProduct($item->status)}}</span></td>
                      <td>
                      <div class="btn-group btn-group-sm">
                        <a href="{{route('products.edit',['product'=>$item->token])}}" class="btn btn-warning"><i class="fas fa-edit"></i></a>
                       
                      </div>
                      
                       
                      @if($item->status!="2")
                      <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-lg">
                      <i class="fas fa-check"></i>
                </button>
                <form action="{{route('product.approved',['token'=>$item->token])}}" method="POST" class="modal fade" id="modal-lg">
@csrf       
<div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">{{__('cms.note')}}</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>{{__('cms.are-you-sure')}}</p>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">{{__('cms.no')}}</button>
              <input type="submit" value="{{__('cms.yes')}}" class="btn btn-primary">
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
</form>
                @endif
</td>
                    </tr>

                  @endforeach
                    
                 
                 
                  </tbody>
                  <tfoot>
                  <tr>
                  <th style="width: 10px">#</th>
                      <th>{{__('cms.thumbnail')}}</th>
                      <th>{{__('cms.title')}}</th>
                      <th>{{__('cms.price')}}</th>
                      <th>{{__('cms.special')}}</th>
                      <th>{{__('cms.token')}}</th>
                      <th>{{__('cms.status')}}</th>
                      <th>{{__('cms.option')}}</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>


     
        </div>
      </div>
    
    </section>
    <!-- /.content -->
 
  


    


@endsection


@section('scripts')

<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>



@endsection






