<?php

namespace Modules\Product\Transformers\ProductOption;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Request;

class ProductOptionCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(
                function ( $item ) {
                    return new ProductOptionResource($item);
                }
            ),
            'filed' => [
                'title','slug','symbol','order','update_date','create_date',
            ],
            'public_route'=>[

                [
                    'name'=>'products.index',
                    'param'=>[null],
                    'icon'=>'fa fa-reply',
                    'title'=>__('cms.add'),
                    'class'=>'btn btn-sm text-sm text-sm-center btn-warning pull-right',
                    'method'=>'GET',
                ],
                [
                    'name'=>Request::segment(4).'.option.create',
                    'param'=>[
                        'token'=>Request::segment(5)
                    ],
                    'icon'=>config('cms.icon.add'),
                    'title'=>__('cms.add'),
                    'class'=>'btn btn-sm text-sm text-sm-center btn-primary pull-right',
                    'method'=>'GET',
                ]

            ],
            'private_route'=>
                [
                    [
                        'name'=>Request::segment(4).'.option.edit',
                        'param'=>[
                            'option'=>'token'
                        ],
                        'icon'=>config('cms.icon.edit'),
                        'title'=>__('cms.edit'),
                        'class'=>'btn btn-warning btn-sm text-sm',
                        'modal'=>false,
                        'method'=>'GET',
                    ],

                ],
            'search_route'=>[

                'name'=>'search.product.attribute',
                'filter'=>[
                    'title','slug','order'
                ],
                'icon'=>config('cms.icon.add'),
                'title'=>__('cms.add'),
                'class'=>'btn btn-sm text-sm text-sm-center btn-primary pull-right',
                'method'=>'POST',

            ],
            'count' => $this->collection->count(),
            'links' => [
                'self' => 'link-value',
            ],
            'collect'=>__('product::options.collect'),
            'title'=>__('product::options.index'),
        ];
    }
}
