<?php

namespace Modules\Product\Transformers\ProductOption;

use Illuminate\Http\Resources\Json\Resource;

class ProductOptionResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'title'=>$this->title ,
            'slug'=>$this->slug ,
            'symbol'=>$this->symbol ,
            'order'=>$this->order ,
            'update_date'=>$this->AgoTimeUpdate,
            'create_date'=>$this->TimeCreate,
            'token'=>$this->token,

        ];
    }
}
