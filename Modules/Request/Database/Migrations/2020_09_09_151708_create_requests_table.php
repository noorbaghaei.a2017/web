<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('list_requests')->nullable()->unsigned();
            $table->foreign('list_requests')->references('id')->on('list_requests')->onDelete('cascade');
            $table->string('email');
            $table->string('name');
            $table->string('phone')->nullable();
            $table->string('ip')->nullable();
            $table->string('title')->nullable();
            $table->string('country')->nullable();
            $table->longtext('text')->nullable();
            $table->string('gender')->nullable();
            $table->string('loss')->nullable();
            $table->string('color')->nullable();
            $table->string('fall_time')->nullable();
            $table->string('transplantation')->nullable();
            $table->string('feeling')->nullable();
            $table->string('execution_time')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
    }
}
