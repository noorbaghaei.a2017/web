<?php


namespace Modules\Service\Entities\Repository;


use Modules\Service\Entities\Service;

class ServiceRepository implements ServiceRepositoryInterface
{

    public function getAll()
    {
        return Service::latest()->get();
    }
}
