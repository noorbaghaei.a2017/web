<?php

namespace Modules\Service\Entities;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\Analyzer;
use Modules\Core\Entities\Translate;
use Modules\Core\Entities\User;
use Modules\Core\Helper\Trades\CommonAttribute;
use Modules\Core\Helper\Trades\TimeAttribute;
use Modules\Question\Entities\Question;
use Modules\Seo\Entities\Seo;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Permission\Models\Role;

class Service extends Model implements HasMedia
{
    use Sluggable,CommonAttribute,HasMediaTrait,TimeAttribute;

    protected $fillable = ['title','text','excerpt','token','user','icon','slug','order','parent','level','excerpt'];

    public function getRouteKeyName()
    {
        return multiRouteKey();
    }
    public function seo()
    {
        return $this->morphOne(Seo::class, 'seoable');
    }
    public function analyzer()
    {
        return $this->morphOne(Analyzer::class, 'analyzerable');
    }
    public function translates()
    {
        return $this->morphMany(Translate::class, 'translateable');
    }
    public function questions()
    {
        return $this->morphMany(Question::class, 'questionable');
    }
    public function user_info()
    {
        return $this->belongsTo(User::class,'user','id');
    }

    /**
     * @inheritDoc
     */
    public function sluggable(): array
    {
        return [
            'slug'=>[
                'source'=>'title'
            ]
        ];
    }


}
