<?php

namespace Modules\Service\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;
use Modules\Core\Entities\Category;
use Modules\Core\Http\Controllers\HasCategory;
use Modules\Core\Http\Controllers\HasQuestion;
use Modules\Core\Http\Requests\CategoryRequest;
use Modules\Question\Entities\Question;
use Modules\Question\Http\Requests\QuestionRequest;
use Modules\Service\Entities\Repository\ServiceRepositoryInterface;
use Modules\Service\Entities\Service;
use Modules\Service\Http\Requests\ServiceRequest;
use Modules\Service\Transformers\ServiceCollection;
use Spatie\MediaLibrary\Models\Media;

class ServiceController extends Controller
{
    use HasQuestion,HasCategory;

    protected $entity;
    protected $class;
    private $repository;

    //category

    protected $route_categories_index='service::categories.index';
    protected $route_categories_create='service::categories.create';
    protected $route_categories_edit='service::categories.edit';
    protected $route_categories='service.categories';


//question

    protected $route_questions_index='service::questions.index';
    protected $route_questions_create='service::questions.create';
    protected $route_questions_edit='service::questions.edit';
    protected $route_questions='services.index';


//notification

    protected $notification_store='service::services.store';
    protected $notification_update='service::services.update';
    protected $notification_delete='service::services.delete';
    protected $notification_error='service::services.error';



    public function __construct(ServiceRepositoryInterface $repository)
    {
        $this->entity=new Service();
        $this->class=Service::class;
        $this->repository=$repository;

        $this->middleware('permission:service-list')->only('index');
        $this->middleware('permission:service-create')->only(['create','store']);
        $this->middleware('permission:service-edit' )->only(['edit','update']);
        $this->middleware('permission:service-delete')->only(['destroy']);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */

    public function index()
    {
        try {
            $items=$this->repository->getAll();

            $services = new ServiceCollection($items);

            $data= collect($services->response()->getData())->toArray();

            return view('core::response.index',compact('data'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }

    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
                !isset($request->title) &&
                !isset($request->slug) &&
                !isset($request->order)
            ){
                $items=$this->entity->latest()->paginate(config('cms.paginate'));

                $result = new ServiceCollection($items);

                $data= collect($result->response()->getData())->toArray();

                return view('core::response.index',compact('data'));
            }
            $items=$this->entity
                ->where("title",trim($request->title))
                ->where("slug",trim($request->slug))
                ->where("order",trim($request->order))
                ->paginate(config('cms.paginate'));

            $result = new ServiceCollection($items);

            $data= collect($result->response()->getData())->toArray();

            return view('core::response.index',compact('data'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $categories=Category::latest()->where('model',Service::class)->get();
        $parent_services=$this->entity->latest()->whereParent(0)->get();
        return view('service::services.create',compact('parent_services','categories'));
    }

    /**
     * Store a newly created resource in storage.
     * @param ServiceRequest $request
     * @return Response
     */
    public function store(ServiceRequest $request)
    {
        try {

            $parent=-1;
            if($request->input('parent')!==-1){
                $parent=$this->entity->whereToken($request->input('parent'))->first();
            }

            $this->entity->user=auth('web')->user()->id;
            $this->entity->title=$request->input('title');
            $this->entity->excerpt=$request->input('excerpt');
            $this->entity->category=Category::whereToken($request->input('category'))->first()->id;
            $this->entity->parent=($request->input('parent')==-1) ? 0: $parent->id;
            $this->entity->icon=$request->input('icon');
            $this->entity->text=$request->input('text');
            $this->entity->order=orderInfo($request->input('order'));
            $this->entity->token=tokenGenerate();


            $saved=$this->entity->save();

            $this->entity->seo()->create([
                'title'=>$request->input('title-seo'),
                'description'=>$request->input('description-seo'),
                'keyword'=>$request->input('keyword-seo'),
                'canonical'=>$request->input('canonical-seo'),
                'robots'=>(is_null(json_encode($request->input('robots'))) ? [] : json_encode($request->input('robots'))),
                'author'=>$request->input('author-seo'),
                'publisher'=>$request->input('publisher-seo'),
            ]);

            $this->entity->analyzer()->create();


            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            if(!$saved){
                return redirect()->back()->with('error',__('service::services.error'));
            }else{
                return redirect(route("services.index"))->with('message',__('service::services.store'));
            }


        }catch (Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }


    /**
     * Show the form for editing the specified resource.
     * @param $token
     * @return Response
     */
    public function edit($token)
    {
        try {
            $categories=Category::latest()->where('model',Service::class)->get();
            $parent_services=$this->entity->latest()->whereParent(0)->get();
            $item=$this->entity->whereToken($token)->firstOrFail();
            return view('service::services.edit',compact('item','categories','parent_services'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param $token
     * @return void
     */
    public function update(ServiceRequest $request, $token)
    {
        try {
            $parent=-1;
            if($request->input('parent')!==-1){
                $parent=$this->entity->whereToken($request->input('parent'))->first();
            }

            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            $updated=$this->entity->update([
                "user"=>auth('web')->user()->id,
                "title"=>$request->input('title'),
                "slug"=>null,
                "icon"=>$request->input('icon'),
                "category"=>Category::whereToken($request->input('category'))->first()->id,
                "parent"=>($request->input('parent')==-1) ? 0: $parent->id,
                "excerpt"=>$request->input('excerpt'),
                "text"=>$request->input('text'),
                "order"=>orderInfo($request->input('order')),
            ]);
            $this->entity->seo()->update([
                'title'=>$request->input('title-seo'),
                'description'=>$request->input('description-seo'),
                'keyword'=>$request->input('keyword-seo'),
                'canonical'=>$request->input('canonical-seo'),
                'robots'=>(is_null(json_encode($request->input('robots'))) ? [] : json_encode($request->input('robots'))),
                'author'=>$request->input('author-seo'),
                'publisher'=>$request->input('publisher-seo'),
            ]);

            $this->entity->replicate();
            if($request->has('image')){
                destroyMedia($this->entity,config('cms.collection-image'));
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            if(!$updated){
                return redirect()->back()->with('error',__('service::services.error'));
            }else{
                return redirect(route("services.index"))->with('message',__('service::services.update'));
            }


        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param $token
     * @return void
     */
    public function destroy($token)
    {
        try {

            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            if($this->entity->Hasmedia(config('cms.collection-image'))){
                destroyMedia($this->entity,config('cms.collection-image'));
            }
            $deleted=$this->entity->delete();

            if(!$deleted){
                return redirect()->back()->with('error',__('service::services.error'));
            }else{
                return redirect(route("services.index"))->with('message',__('service::services.delete'));
            }


        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    public  function languageShow(Request $request,$lang,$token){
        $item=Service::with('translates')->where('token',$token)->first();
        return view('service::services.language',compact('item','lang'));

    }
    public  function languageUpdate(Request $request,$lang,$token){

        DB::beginTransaction();
        $item=Service::with('translates')->where('token',$token)->first();
        if( $item->translates->where('lang',$lang)->first()){
            $changed=$item->translates->where('lang',$lang)->first()->update([
                'symbol'=>$request->symbol,
                'slug'=>$request->slug,
                'title'=>$request->title,
                'text'=>$request->text,
                'description_seo'=>$request->description_seo,
                'title_seo'=>$request->title_seo,
                'keyword_seo'=>$request->keyword_seo,
                'canonical_seo'=>$request->canonical_seo,
            ]);
        }else{
            $changed=$item->translates()->create([
                'symbol'=>$request->symbol,
                'slug'=>$request->slug,
                'title'=>$request->title,
                'text'=>$request->text,
                'description_seo'=>$request->description_seo,
                'title_seo'=>$request->title_seo,
                'keyword_seo'=>$request->keyword_seo,
                'canonical_seo'=>$request->canonical_seo,
                'lang'=>$lang
            ]);
        }


        if(!$changed){
            DB::rollBack();
            return redirect()->back()->with('error',__('service::services.error'));
        }else{
            DB::commit();
            return redirect(route("services.index"))->with('message',__('service::services.update'));
        }

    }

}
