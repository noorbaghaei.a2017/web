<?php


namespace Modules\Sms\Contract;


interface SmsGatewayInterface
{
    public static function getToken();
    public static function sendMessage($data);
}
