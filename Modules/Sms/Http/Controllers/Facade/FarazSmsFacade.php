<?php


namespace Modules\Sms\Http\Controllers\Facade;


use Illuminate\Support\Facades\Facade;

class FarazSmsFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'farazsms';
    }

}
