<?php
return [
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"sms list",
    "singular"=>"sms",
    "collect"=>"smses",
    "permission"=>[
        "sms-full-access"=>"smses full access",
        "sms-list"=>"smses list",
        "sms-delete"=>"sms delete",
        "sms-create"=>"sms create",
        "sms-edit"=>"edit sms",
    ]
];
