<?php

return [
    "text-create"=>' با استفاده از فرم زیر میتوانید  تخفیف ویژه جدید اضافه کنید.',
    "text-edit"=>' با استفاده از فرم زیر میتوانید  تخفیف ویژه خود را ویرایش کنید.',
    "store"=>"ثبت با موفقیت انجام شد.",
    "delete"=>"حذف با موفقیت انجام شد",
    "update"=>"بروز رسانی با موفقیت انجام شد",
    "index"=>"لیست تخفیفات ویژه",
    "error"=>"خطا",
    "singular"=>" تخفیف ویژه",
    "collect"=>"تخفیفات ویژه",
    "permission"=>[
        "specialdiscount-full-access"=>"دسترسی کامل به تخفیفات ویژه",
        "specialdiscount-list"=>"لیست تخفیفات ویژه",
        "specialdiscount-delete"=>"حذف  تخفیف ویژه",
        "specialdiscount-create"=>"ایجاد تخفیف ویژه",
        "specialdiscount-edit"=>"ویرایش  تخفیف ویژه",
    ]

];
