<?php

namespace Modules\Store\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Store\Entities\Store;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('permissions')->whereModel(Store::class)->delete();

        Permission::create(['name'=>'store-list','model'=>Store::class,'created_at'=>now()]);
        Permission::create(['name'=>'store-create','model'=>Store::class,'created_at'=>now()]);
        Permission::create(['name'=>'store-edit','model'=>Store::class,'created_at'=>now()]);
        Permission::create(['name'=>'store-delete','model'=>Store::class,'created_at'=>now()]);
    }
}
