<?php


namespace App\Facades\Rest;


use App\Enums\ResponseCode;
use App\Jobs\SendEmailJob;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class Rest
{
    public static $SARA = '#';

    /**
     * @param $code
     * @param $message
     * @param $data
     * @param $status
     * @param null $method
     * @param null $url
     * @return JsonResponse
     */
    public static function response($code, $message, $data, $status,$method=null,$url=null){
        return response()->json([
            'meta' => [
                'method' => $method,
                'url' => $url,
                'code' => $code,
                'message' => $message
            ],
            'data' => $data
        ], $status);
    }

    /**
     * @param $msg
     * @param $data
     * @return JsonResponse
     */
    public static function success($msg, $data){
        return self::response(ResponseCode::Success, $msg, $data, ResponseCode::Success);
    }

    /**
     * @param $exception
     * @return JsonResponse
     */
    public static function error($exception){
        if(env('APP_DEBUG')) {
            return self::response(ResponseCode::Error, $exception->getMessage(), null, ResponseCode::Error);
        }else{
            return self::response(ResponseCode::Error, 'INTERNAL_ERROR', null, ResponseCode::Error);
        }
    }

    public static function MailError($exception,$method,$url){
            return self::response(ResponseCode::Error, $exception, null, ResponseCode::Error,$method,$url);

    }
    public static function errorLog($error){
        $job=(new SendEmailJob($error->getMessage()))->delay(Carbon::now()->addMinutes(2));
        dispatch($job);
    }

    /**
     * @param string $msg
     * @param null $data
     * @return JsonResponse
     */
    public static function badRequest($msg='BAD_REQUEST', $data=null){
        return self::response(ResponseCode::Bad, $msg, $data, ResponseCode::Bad);
    }

    /**
     * @return JsonResponse
     */
    public static function unAuthentication(){
        return self::response(ResponseCode::Unauthorized, 'UnAuthentication', null, ResponseCode::Unauthorized);
    }

    /**
     * @param string $msg
     * @param null $data
     * @return JsonResponse
     */
    public static function notFound($msg='NOT_FOUND', $data=null){
        return self::response(ResponseCode::NotFound, $msg, $data, ResponseCode::NotFound);
    }

}
