<?php

use Illuminate\Http\Request;
use Illuminate\Support\Str;

function tokenGenerateSimple(){
   return Str::random(20);
  
}
function tokenGenerate(){
    $string=Str::random(20);
    $item=Uuid::generate(5,$string, Uuid::NS_DNS);
    return $item->string;
}
function tokenClient(){
    $string="client-".Str::random(5);
    $item=Uuid::generate(5,$string, Uuid::NS_DNS);
    return $item->string;
}
function codeProduct(){
    $string="product-".Str::random(5);
    $item=Uuid::generate(4,$string, Uuid::NS_DNS);
    return $item->string;
}
function codeQuestion(){
    $string="question-".Str::random(5);
    $item=Uuid::generate(4,$string, Uuid::NS_DNS);
    return $item->string;
}
function codeVip(){
    $string="vip-".Str::random(4);
    $item=Uuid::generate(3,$string, Uuid::NS_DNS);
    return "vip-".$item->string;
}

function tokenResetGenerate(){
    $string="reset-".Str::random(20);
    $item=Uuid::generate(4,$string, Uuid::NS_DNS);
    return $item->string;
}

function multiRouteKey(){
    return "token";
}

function orderInfo($order){
    return is_null(empty($order)) ? 1 : $order;
}

