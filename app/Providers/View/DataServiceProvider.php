<?php

namespace App\Providers\View;


use App\View\adminComposer;
use App\View\frontComposer;
use App\View\aboutComposer;
use App\View\searchWerbungComposer;
use App\View\generalComposer;
use App\View\searchCityBussinessComposer;
use App\View\searchBussinessComposer;
use App\View\searchProductComposer;
use App\View\listCategoryComposer;
use App\View\createBussinessDashboardComposer;
use App\View\createProductDashboardComposer;
use App\View\editBussinessDashboardComposer;
use App\View\editProductDashboardComposer;
use App\View\frontAdminComposer;
use App\View\coreAdminComposer;
use App\View\singleProductComposer;
use App\View\mainAdminDashboardComposer;
use App\View\generalAdminComposer;
use App\View\generalCategoryJobAdminComposer;
use App\View\contactComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class DataServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('template.index',frontComposer::class);
        View::composer('template.bussiness.index',searchBussinessComposer::class);
        View::composer('template.products.index',searchProductComposer::class);
        View::composer('template.jobs.index',searchWerbungComposer::class);
        View::composer('template.*',generalComposer::class);
        View::composer('template.pages.contact_us',contactComposer::class);
        View::composer('template.bussiness.index',searchCityBussinessComposer::class);
        View::composer('template.bussiness.single',singleProductComposer::class);
        View::composer('template.bussiness.categories',listCategoryComposer::class);
        View::composer('template.pages.about_us',aboutComposer::class);
        View::composer('template.auth.bussiness.create',createBussinessDashboardComposer::class);
        View::composer('template.auth.products.create',createProductDashboardComposer::class);
        View::composer('template.auth.bussiness.edit',editBussinessDashboardComposer::class);
        View::composer('template.auth.products.edit',editProductDashboardComposer::class);
        View::composer('Advertising::category_jobs.*',coreAdminComposer::class);
        // View::composer('core::dashboard.index',adminComposer::class);
        View::composer('core::dashboard.*',generalAdminComposer::class);
       
        View::composer('Advertising::category_jobs.index',generalCategoryJobAdminComposer::class);
         View::composer('core::dashboard.*',mainAdminDashboardComposer::class);
        View::composer('core::dashboard.index',frontAdminComposer::class);

    }
}
