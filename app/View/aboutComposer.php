<?php

namespace App\View;



use Modules\Member\Entities\Member;
use Illuminate\View\View;
use Carbon\Carbon;



class aboutComposer{


    public function compose(View $view){

        $view->with('members', Member::select('id','first_name','last_name','role')->orderBy('created_at','desc')->take(6)->get());
       
      
    }

}
