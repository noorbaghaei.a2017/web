<?php

namespace App\View;



use Illuminate\View\View;
use Carbon\Carbon;
use Modules\Client\Entities\Client;
use Modules\Core\Entities\Setting;
use Modules\Client\Entities\Vip;
use Modules\Product\Entities\Product;
use Modules\Core\Entities\UserServices;
use Modules\Advertising\Entities\Advertising;
use Modules\Member\Entities\Member;
use Modules\Comment\Entities\Comment;
use Modules\Core\Entities\User;



class generalAdminComposer{


    public function compose(View $view){

        $view->with('new_products_count', Product::select('id','status')->with('attributes','properties')->whereStatus(1)->count());
        $view->with('new_jobs_count', UserServices::select('id','status')->with('attributes')->whereStatus(1)->count());
        $view->with('new_advertisings_count', Advertising::select('id','status')->whereStatus(1)->count());
        $view->with('new_clients_count', Client::latest()
        ->whereHas('role',function ($query){
            $query->where('title', '=', 'user');
        })->where('is_active',2)->where('is_verify_email',0)
        ->count());

        $view->with('setting',Setting::select('id', 'name', 'address','email','mobile','phone','fax','domain','slogan','copy_right')->with('info')->first());
        $view->with('commentcount', Comment::select('id','status')->count());  
        $view->with('commentlast', Comment::select('id','status')->take(5)->get());  
        $view->with('requestcommentcount', Comment::select('id','status')->whereStatus(0)->count());          
        $view->with('memberscount', Member::select('id','status')->count());
        $view->with('formscount', Vip::select('id','status')->count());
        if(auth('web')->check()){
            $view->with('admin', User::find(auth('web')->user()->id));
        }
    }

}
