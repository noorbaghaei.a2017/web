<?php

namespace App\View;



use Illuminate\View\View;
use Modules\Core\Entities\AnalyticsClient;
use Modules\Article\Entities\Article;
use Modules\Carousel\Entities\Carousel;
use Modules\Chat\Entities\Chat;
use Modules\Client\Entities\Client;
use Modules\Core\Entities\ListServices;
use Modules\Core\Entities\UserServices;
use Modules\Core\Entities\Setting;
use Modules\Advertising\Entities\AttributeJob;
use Modules\Menu\Entities\Menu;



class generalComposer{


    public function compose(View $view){

  
        $service_active_category=UserServices::distinct()->select('id','service','user','excerpt','text','title','banner_status','postal_code','slug','city','phone','email','is_special','status','address','token')->whereStatus(2)->pluck('service')->toArray();
        $active_category=ListServices::distinct()->select('id','parent')->whereIn('id',$service_active_category)->where('parent','<>',0)->pluck('parent','id')->toArray();

        if(isset($active_category)){
            foreach($active_category as $key=>$value){
                $child_category_job[]=$key;
                $parent_category_job[]=$value;
            }
        }
       
        if(isset($child_category_job)){
        $view->with('child_active_category',$child_category_job);
        }
     

        $view->with('last_articles', Article::select('id','user','title','excerpt')->take(3)->get());
        $view->with('noExistClient',AnalyticsClient::select('ip')->whereIp($_SERVER['REMOTE_ADDR'])->first() ? false : true);
        // $view->with('all_category_bussiness_top_filter', ListServices::orderBy('order','asc')->whereIn('id',$active_category)->get());
        // $view->with('all_category_bussiness_top_filter_view', ListServices::select('id','title','slug','parent')->whereIn('id',$parent_category_job)->inRandomOrder()->limit(5)->get());
       
       
        if(auth('client')->check()){
            $view->with('client',Client::with('info','myCode','cart','jobs','products','werbungs')->find(\auth('client')->user()->id));
            $view->with('chats',Chat::where('client',\auth('client')->user()->id)->get());

        }
       
            $view->with('single_slider',Carousel::select('id')->first());
            $view->with('attributes_job',AttributeJob::select('id','icon','title')->get());


        $view->with('setting',Setting::select('id', 'name', 'address','email','mobile','phone','fax','domain','slogan','copy_right')->with('info')->first());
        $view->with('all_category_bussiness_top', ListServices::with('childs')->select('id','title','parent')->orderBy('order','asc')->whereIn('id',$active_category)->take(6)->get());
        $view->with('top_menus', Menu::select('title','id','symbol','href','parent','pattern','order','status','target','list_menus','token')->with('translates','childs')->orderBy('order','asc')->where('parent',0)->whereStatus(1)->where('list_menus',1)->get());
   
        $view->with('top_menus', Menu::select('title','id','symbol','href','parent','pattern','order','status','target','list_menus','token')->with('translates','childs')->orderBy('order','asc')->where('parent',0)->whereStatus(1)->where('list_menus',1)->get());
    }

}
