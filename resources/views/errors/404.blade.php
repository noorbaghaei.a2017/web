﻿<!doctype html>
<html lang="fa" dir="rtl">
<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="keywords" content="404 page" >
        <meta name="description" content="404 page">
        <meta name="author" content="404 page">

<link rel="stylesheet" href="{{ asset('template/css/bootstrap.min.css') }}">

<link rel="stylesheet" href="{{ asset('template/css/owl.theme.default.min.css') }}">

<link rel="stylesheet" href="{{ asset('template/css/owl.carousel.min.css')}}">

<link rel="stylesheet" href="{{ asset('template/css/magnific-popup.css') }}">

<link rel="stylesheet" href="{{ asset('template/css/animate.css') }}">

<link rel="stylesheet" href="{{ asset('template/css/boxicons.min.css') }}">

<link rel="stylesheet" href="{{ asset('template/css/flaticon.css') }}">

<link rel="stylesheet" href="{{ asset('template/css/meanmenu.css') }}">

<link rel="stylesheet" href="{{ asset('template/css/nice-select.css') }}">

<link rel="stylesheet" href="{{ asset('template/css/odometer.css') }}">

<link rel="stylesheet" href="{{ asset('template/css/style.css') }}">

<link rel="stylesheet" href="{{ asset('template/css/responsive.css') }}">

<link rel="stylesheet" href="{{ asset('template/css/rtl.css') }}">
<link rel="icon" type="image/png" href="{{ asset('template/img/brand-2-karbin.png')}}">

<title>صفحه خطا</title>
</head>
<body>

<div class="preloader preloader-five">
<div class="lds-ripple">
<div></div>
<div></div>
</div>
</div>


<div class="error-area">
<div class="d-table">
<div class="d-table-cell">
<div class="error-content-wrap">
<h1>4 <span>0</span> 4</h1>
<h3>اوه! صفحه یافت نشد</h3>
<p>صفحه مورد نظر شما یافت نشد ممکن است صفحه حذف شده باشد.</p>
<a href="{{ route('front.website') }}" class="default-btn page-btn active">
بازگشت به صفحه اصلی
</a>
</div>
</div>
</div>
</div>


<div class="go-top">
<i class='bx bx-chevrons-up'></i>
<i class='bx bx-chevrons-up'></i>
</div>


<script src="{{ asset('template/js/jquery-3.5.1.slim.min.js') }}"></script>

<script src="{{ asset('template/js/popper.min.js') }}"></script>

<script src="{{ asset('template/js/bootstrap.min.js') }}"></script>

<script src="{{ asset('template/js/jquery.meanmenu.js') }}"></script>

<script src="{{ asset('template/js/wow.min.js') }}"></script>

<script src="{{ asset('template/js/owl.carousel.js') }}"></script>

<script src="{{ asset('template/js/jquery.magnific-popup.min.js') }}"></script>

<script src="{{ asset('template/js/jquery.nice-select.min.js') }}"></script>

<script src="{{ asset('template/js/parallax.min.js') }}"></script>

<script src="{{ asset('template/js/jquery.mixitup.min.js') }}"></script>

<script src="{{ asset('template/js/jquery.appear.js') }}"></script>

<script src="{{ asset('template/js/odometer.min.js') }}"></script>

<script src="{{ asset('template/js/jquery.ajaxchimp.min.js') }}"></script>

<script src="{{ asset('template/js/form-validator.min.js') }}"></script>

<script src="{{ asset('template/js/contact-form-script.js') }}"></script>

<script src="{{ asset('template/js/custom.js') }}"></script>
</body>
</html>