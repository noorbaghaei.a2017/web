@extends('template.app')

@section('content')

 <!-- content-->
 <div class="content">
                    <!--  section  -->
                    <section class="parallax-section single-par" data-scrollax-parent="true">
                        <div class="bg par-elem "  data-bg="{{asset('template/images/bg/5.jpg')}}" data-scrollax="properties: { translateY: '30%' }"></div>
                        <div class="overlay op7"></div>
                        <div class="container">
                            <div class="section-title center-align big-title">
                             
                            </div>
                        </div>
                        <div class="header-sec-link">
                            <a href="#sec1" class="custom-scroll-link"><i class="fal fa-angle-double-down"></i></a> 
                        </div>
                    </section>
                    <!--  section  end-->
                    <section class="gray-bg no-top-padding-sec" id="sec1">
                        <div class="container">
                            <div class="breadcrumbs inline-breadcrumbs fl-wrap block-breadcrumbs">
                                <div  class="showshare brd-show-share color2-bg"> <i class="fas fa-share"></i>اشتراک</div>
                            </div>
                            <div class="share-holder hid-share sing-page-share top_sing-page-share">
                                <div class="share-container  isShare"></div>
                            </div>
                            <div class="post-container fl-wrap">
                                <div class="row">
                                    <!-- blog content-->
                                    <div class="col-md-8">
                                    @foreach($items as $item)
                                        <!-- article> --> 
                                        <article class="post-article">
                                            <div class="list-single-main-media fl-wrap">
                                            <a href="{{route('articles.single',['article'=>convert_lang($item,LaravelLocalization::getCurrentLocale(),'slug')])}}">
                                            @if(!$item->Hasmedia('images'))
                                                <img class="respimg" src="{{asset('img/no-img.gif')}}" alt="{{convert_lang($item,LaravelLocalization::getCurrentLocale(),'title')}}" title="{{convert_lang($item,LaravelLocalization::getCurrentLocale(),'title')}}" >
                                            @else
                                                <img class="respimg" src="{{$item->getFirstMediaUrl('images')}}" alt="{{convert_lang($item,LaravelLocalization::getCurrentLocale(),'title')}}" title="{{convert_lang($item,LaravelLocalization::getCurrentLocale(),'title')}}" >
                                            @endif

                                </a>
                                               
                                            </div>
                                            <div class="list-single-main-item fl-wrap block_box">
                                                <h2 class="post-opt-title"><a href="{{route('articles.single',['article'=>convert_lang($item,LaravelLocalization::getCurrentLocale(),'slug')])}}">{!! convert_lang($item,LaravelLocalization::getCurrentLocale(),'title') !!}</a></h2>
                                                {!! convert_lang($item,LaravelLocalization::getCurrentLocale(),'text') !!}
                                                <span class="fw-separator"></span>
                                                <div class="post-author"><a href="{{route('articles.single',['article'=>convert_lang($item,LaravelLocalization::getCurrentLocale(),'slug')])}}">{!! convert_lang($item,LaravelLocalization::getCurrentLocale(),'title') !!}"><img src="{{asset('template/images/avatar/5.jpg')}}" alt=""><span>توسط ، آلیسا نوری</span></a></div>
                                                <div class="post-opt">
                                                    <ul class="no-list-style">
                                                        <li><i class="fal fa-calendar"></i> <span>25 اسفند 1399</span></li>
                                                        <li><i class="fal fa-eye"></i> <span>264</span></li>
                                                        <li><i class="fal fa-tags"></i> <a href="#">عکاسی</a> , <a href="#">طرح</a> </li>
                                                    </ul>
                                                </div>
                                                <a href="{{route('articles.single',['article'=>convert_lang($item,LaravelLocalization::getCurrentLocale(),'slug')])}}" class="btn  color2-bg float-btn">{{__('cms.read_more')}} <i class="fal fa-angle-left"></i></a>
                                            </div>
                                        </article>
                                        <!-- article end -->    
                                        
                                     @endforeach   
                                                                            
                                   
                                    </div>
                                    <!-- blog conten end-->
                                    <!-- blog sidebar -->
                                    <div class="col-md-4">
                                        <div class="box-widget-wrap fl-wrap fixed-bar">
                                            <!--box-widget-item -->
                                            <div class="box-widget-item fl-wrap block_box">
                                                <div class="box-widget-item-header">
                                                    <h3>جستجو در وبلاگ</h3>
                                                </div>
                                                <div class="box-widget fl-wrap">
                                                    <div class="box-widget-content">
                                                        <div class="search-widget">
                                                            <form action="#" class="fl-wrap">
                                                                <input name="se" id="se" type="text" class="search" placeholder="جستجو..." value="" />
                                                                <button class="search-submit color2-bg" id="submit_btn"><i class="fal fa-search"></i> </button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--box-widget-item end -->                                    
                                            <!--box-widget-item -->
                                            <div class="box-widget-item fl-wrap block_box">
                                                <div class="box-widget-item-header">
                                                    <h3> {{__('cms.popular')}} </h3>
                                                </div>
                                                <div class="box-widget  fl-wrap">
                                                    <div class="box-widget-content">
                                                        <!--widget-posts-->
                                                        <div class="widget-posts  fl-wrap">
                                                            <ul class="no-list-style">
                                                                @foreach ($popular_articles as $popular)
                                                                <li>
                                                                    <div class="widget-posts-img"><a href="{{route('articles.single',['article'=>convert_lang($popular,LaravelLocalization::getCurrentLocale(),'slug')])}}"><img src="{{asset('template/images/gallery/thumbnail/1.png')}}" alt=""></a></div>
                                                                    <div class="widget-posts-descr">
                                                                        <h4><a href="{{route('articles.single',['article'=>convert_lang($popular,LaravelLocalization::getCurrentLocale(),'slug')])}}">  {{convert_lang($popular,LaravelLocalization::getCurrentLocale(),'title')}} </a></h4>
                                                                        <div class="geodir-category-location fl-wrap"><a href="#"><i class="fal fa-calendar"></i> 27 اسفند 1399</a></div>
                                                                    </div>
                                                                </li>
                                                                @endforeach
                                                               
                                                              
                                                            </ul>
                                                        </div>
                                                        <!-- widget-posts end-->
                                                    </div>
                                                </div>
                                            </div>
                                            <!--box-widget-item end -->                                     
                                            <!--box-widget-item -->
                                            <div class="box-widget-item fl-wrap">
                                                <div class="banner-wdget fl-wrap">
                                                    <div class="overlay"></div>
                                                    <div class="bg"  data-bg="{{asset(asset('template/images/bg/13.jpg'))}}"></div>
                                                    <div class="banner-wdget-content fl-wrap">
                                                        <a href="#subscribe" class="custom-scroll-link color-bg" > ثبت نام</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--box-widget-item end -->                                        
                                            <!--box-widget-item -->
                                            <div class="box-widget-item fl-wrap block_box">
                                                <div class="box-widget-item-header">
                                                    <h3> {{__('cms.tags')}}</h3>
                                                </div>
                                                <div class="box-widget fl-wrap">
                                                    <div class="box-widget-content">
                                                        <div class="list-single-tags tags-stylwrap">

                                                       
                                                            <a href="#"></a>

                                                           
                                                                                                                                     
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--box-widget-item end -->                                     
                                            <!--box-widget-item -->
                                            <div class="box-widget-item fl-wrap block_box">
                                                <div class="box-widget-item-header">
                                                    <h3>دسته بندی ها</h3>
                                                </div>
                                                <div class="box-widget fl-wrap">
                                                    <div class="box-widget-content">
                                                        <ul class="cat-item no-list-style">
                                                            <li><a href="#">استاندارد</a> <span>3</span></li>
                                                            <li><a href="#">فیلم</a> <span>6 </span></li>
                                                            <li><a href="#">گالری</a> <span>12 </span></li>
                                                            <li><a href="#">نقل قول ها</a> <span>4</span></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--box-widget-item end -->                                   
                                        </div>
                                    </div>
                                    <!-- blog sidebar end -->
                                </div>
                            </div>
                        </div>
                    </section>
                    <div class="limit-box fl-wrap"></div>
                </div>
                <!--content end-->





@endsection
