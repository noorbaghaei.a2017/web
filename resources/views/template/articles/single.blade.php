@extends('template.app')

@section('content')


 <!-- content-->
 <div class="content">
                    <section class="gray-bg no-top-padding-sec" id="sec1">
                        <div class="container">
                            <div class="breadcrumbs inline-breadcrumbs fl-wrap block-breadcrumbs">
                            
                                <div  class="showshare brd-show-share color2-bg"> <i class="fas fa-share"></i> اشتراک </div>
                            </div>
                            <div class="share-holder hid-share sing-page-share top_sing-page-share">
                                <div class="share-container  isShare"></div>
                            </div>
                            <div class="post-container fl-wrap">
                                <div class="row">
                                    <!-- blog content-->
                                    <div class="col-md-8">
                                        <!-- article> --> 
                                        <article class="post-article single-post-article">
                                            <div class="list-single-main-media fl-wrap">
                                                <div class="single-slider-wrap">
                                                    <div class="single-slider fl-wrap">
                                                    <a href="{{route('articles.single',['article'=>convert_lang($item,LaravelLocalization::getCurrentLocale(),'slug')])}}">
                                            @if(!$item->Hasmedia('images'))
                                                <img class="respimg" src="{{asset('img/no-img.gif')}}" alt="{{convert_lang($item,LaravelLocalization::getCurrentLocale(),'title')}}" title="{{convert_lang($item,LaravelLocalization::getCurrentLocale(),'title')}}" >
                                            @else
                                                <img class="respimg" src="{{$item->getFirstMediaUrl('images')}}" alt="{{convert_lang($item,LaravelLocalization::getCurrentLocale(),'title')}}" title="{{convert_lang($item,LaravelLocalization::getCurrentLocale(),'title')}}" >
                                            @endif

                                </a>
                                               
                                                    </div>
                                                  
                                                   
                                                </div>
                                            </div>
                                            <div class="list-single-main-item fl-wrap block_box">
                                                <h2 class="post-opt-title"><a href="blog-single.html"> {{convert_lang($item,LaravelLocalization::getCurrentLocale(),'title')}}</a></h2>
                                                <div class="post-author"><a href="#"><img src="{{asset('template/images/avatar/5.jpg')}}" alt=""><span>توسط ، آلیسا نوری</span></a></div>
                                               
                                                <div class="post-opt">
                                                    <ul class="no-list-style">
                                                        <li><i class="fal fa-calendar"></i> <span>25 اسفند 1399</span></li>
                                                        <li><i class="fal fa-eye"></i> <span>264</span></li>
                                                        <li><i class="fal fa-tags"></i> <a href="#">عکاسی</a> , <a href="#">طرح</a> </li>
                                                    </ul>
                                                </div>
                                                <span class="fw-separator"></span> 
                                                <div class="clearfix"></div>
                                                {{convert_lang($item,LaravelLocalization::getCurrentLocale(),'title')}}
                                               
                                                {!!convert_lang($item,LaravelLocalization::getCurrentLocale(),'text') !!}

                                            </div>
                                        </article>
                                        <!-- article end --> 
                                        <!-- post nav --> 
                                        <div class="post-nav-wrap fl-wrap">
                                            <a class="post-nav post-nav-prev" href="blog-single.html"><span class="post-nav-img"><img src="{{asset('template/images/all/7.jpg')}}" alt=""></span><span class="post-nav-text"><strong>پست قبلی</strong> <br>نحوه انتخاب فروشگاه مناسب</span></a> 
                                            <a class="post-nav post-nav-next" href="blog-single.html"><span class="post-nav-img"><img src="{{asset('template/images/all/4.jpg')}}" alt=""></span><span class="post-nav-text"><strong>پست بعدی</strong><br>بهترین هتل برای خانواده شما</span></a> 
                                        </div>
                                        <!-- post nav end --> 
                                        <!-- list-single-main-item -->   
                                        <div class="list-single-main-item fl-wrap block_box">
                                            <div class="list-single-main-item-title">
                                                <h3>نظرات -  <span> 2 </span></h3>
                                            </div>
                                            <div class="list-single-main-item_content fl-wrap">
                                                <div class="reviews-comments-wrap">
                                                    <!-- reviews-comments-item -->  
                                                    <div class="reviews-comments-item only-comments">
                                                        <div class="review-comments-avatar">
                                                            <img src="{{asset('template/images/avatar/4.jpg')}}" alt=""> 
                                                        </div>
                                                        <div class="reviews-comments-item-text fl-wrap">
                                                            <div class="reviews-comments-header fl-wrap">
                                                                <h4><a href="#">لیزا رز</a></h4>
                                                            </div>
                                                            <p>" لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز. "</p>
                                                            <div class="reviews-comments-item-footer fl-wrap">
                                                                <div class="reviews-comments-item-date"><span><i class="far fa-calendar-check"></i>12 اسفند 1399</span></div>
                                                                <a href="#" class="reply-item">پاسخ</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--reviews-comments-item end--> 
                                                    <!-- reviews-comments-item -->  
                                                    <div class="reviews-comments-item only-comments">
                                                        <div class="review-comments-avatar">
                                                            <img src="{{asset('template/images/avatar/6.jpg')}}" alt=""> 
                                                        </div>
                                                        <div class="reviews-comments-item-text fl-wrap">
                                                            <div class="reviews-comments-header fl-wrap">
                                                                <h4><a href="#">آدام کونسی</a></h4>
                                                            </div>
                                                            <p>" لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز. "</p>
                                                            <div class="reviews-comments-item-footer fl-wrap">
                                                                <div class="reviews-comments-item-date"><span><i class="far fa-calendar-check"></i>12 اسفند 1399</span></div>
                                                                <a href="#" class="reply-item">پاسخ</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--reviews-comments-item end-->                                                                  
                                                </div>
                                            </div>
                                        </div>
                                        <!-- list-single-main-item end -->                                       
                                        <!-- list-single-main-item -->   
                                        <div class="list-single-main-item fl-wrap block_box" id="sec6">
                                            <div class="list-single-main-item-title fl-wrap">
                                                <h3>ارسال نظرات</h3>
                                            </div>
                                            <!-- Add Review Box -->
                                            <div id="add-review" class="add-review-box">
                                                <!-- Review Comment -->
                                                <form id="add-comment" class="add-comment  custom-form" name="rangeCalc" >
                                                    <fieldset>
                                                        <div class="list-single-main-item_content fl-wrap">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <label><i class="fal fa-user"></i></label>
                                                                    <input type="text" placeholder="نام *" value=""/>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label><i class="fal fa-envelope"></i>  </label>
                                                                    <input type="text" placeholder="آدرس ایمیل*" value=""/>
                                                                </div>
                                                            </div>
                                                            <textarea cols="40" rows="3" placeholder="نظر شما:"></textarea>
                                                            <div class="clearfix"></div>
                                                            <button class="btn  color2-bg  float-btn" style="margin-top:30px;">ارسال <i class="fal fa-paper-plane"></i></button>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                            </div>
                                            <!-- Add Review Box / End -->
                                        </div>
                                        <!-- list-single-main-item end -->                                               
                                    </div>
                                    <!-- blog conten end-->
                                    <!-- blog sidebar -->
                                    <div class="col-md-4">
                                        <div class="box-widget-wrap fl-wrap fixed-bar">
                                            <!--box-widget-item -->
                                            <div class="box-widget-item fl-wrap block_box">
                                                <div class="box-widget-item-header">
                                                    <h3>جستجو در وبلاگ</h3>
                                                </div>
                                                <div class="box-widget fl-wrap">
                                                    <div class="box-widget-content">
                                                        <div class="search-widget">
                                                            <form action="#" class="fl-wrap">
                                                                <input name="se" id="se" type="text" class="search" placeholder="جستجو ..." value="" />
                                                                <button class="search-submit color2-bg" id="submit_btn"><i class="fal fa-search"></i> </button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--box-widget-item end -->  
                                            <!--box-widget-item -->
                                            <div class="box-widget-item fl-wrap block_box">
                                                <div class="box-widget-item-header">
                                                    <h3>پست های محبوب</h3>
                                                </div>
                                                <div class="box-widget  fl-wrap">
                                                    <div class="box-widget-content">
                                                        <!--widget-posts-->
                                                        <div class="widget-posts  fl-wrap">
                                                            <ul class="no-list-style">
                                                                <li>
                                                                    <div class="widget-posts-img"><a href="blog-single.html"><img src="{{asset('template/images/gallery/thumbnail/1.png')}}" alt=""></a></div>
                                                                    <div class="widget-posts-descr">
                                                                        <h4><a href="listing-single.html">لورم ایپسوم متن ساختگی</a></h4>
                                                                        <div class="geodir-category-location fl-wrap"><a href="#"><i class="fal fa-calendar"></i> 27 اسفند 1399</a></div>
                                                                    </div>
                                                                </li>
                                                             
                                                            </ul>
                                                        </div>
                                                        <!-- widget-posts end-->
                                                    </div>
                                                </div>
                                            </div>
                                            <!--box-widget-item end -->                                     
                                            <!--box-widget-item -->
                                            <div class="box-widget-item fl-wrap">
                                                <div class="banner-wdget fl-wrap">
                                                    <div class="overlay"></div>
                                                    <div class="bg"  data-bg="{{asset('template/images/bg/13.jpg')}}"></div>
                                                    <div class="banner-wdget-content fl-wrap">
                                                        <a href="#subscribe" class="custom-scroll-link color-bg" > ثبت نام</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--box-widget-item end -->                                        
                                            <!--box-widget-item -->
                                            <div class="box-widget-item fl-wrap block_box">
                                                <div class="box-widget-item-header">
                                                    <h3> {{__('cms.tags')}} </h3>
                                                </div>
                                                <div class="box-widget fl-wrap">
                                                    <div class="box-widget-content">
                                                        <div class="list-single-tags tags-stylwrap">
                                                            <a href="#"></a>
                                                                                                                                     
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--box-widget-item end -->                                     
                                            <!--box-widget-item -->
                                            <div class="box-widget-item fl-wrap block_box">
                                                <div class="box-widget-item-header">
                                                    <h3>  {{__('cms.category')}}</h3>
                                                </div>
                                                <div class="box-widget fl-wrap">
                                                    <div class="box-widget-content">
                                                        <ul class="cat-item no-list-style">
                                                            <li><a href="#">استاندارد</a> <span>3</span></li>
                                                           
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--box-widget-item end -->                                   
                                        </div>
                                    </div>
                                    <!-- blog sidebar end -->
                                </div>
                            </div>
                        </div>
                    </section>
                    <div class="limit-box fl-wrap"></div>
                </div>
                <!--content end-->





@endsection
