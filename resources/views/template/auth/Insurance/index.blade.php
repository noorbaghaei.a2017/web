@extends('template.auth.app')


@section('content')




  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    

    <!-- Main content -->
    <section class="content">



      <!-- Main row -->
      <div class="row">
        <!-- right col -->
       
        <div class="col-xs-12">
            <div class="box">

                <div class="box-header">
                    <h3 class="box-title">لیست بیمه</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                      <thead>
                      <tr>
                        <th>نوع بیمه</th>
                        <th>ثبت</th>
                        <th>وضعیت</th>
                      </tr>
                      </thead>
                      <tbody>
                     
                        @foreach ($items as $item)
                          
                        <tr>
                          <td> طرح Vip </td>
             
                          <td>{{ $item->created_at->ago() }}</td>
                          <td><span class="label label-warning">در حال بررسی<span></td>
                      </tr>

                        @endforeach
                     
                     
                     
                      </tbody>
                      <tfoot>
                        <tr>
                            <th>نوع بیمه</th>
                            <th>ثبت</th>
                            <th>وضعیت</th>
                          </tr>
                      </tfoot>
                    </table>
                  </div>
                  <!-- /.box-body -->
                </div>


       
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  
  @endsection


  @section('heads')


  <link rel="stylesheet" href="{{ asset('template/user/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">


  @endsection

  @section('scripts')


  <script src="{{asset('template/user/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>

  

  <script>
    $(function () {
      $('#example1').DataTable()
      $('#example2').DataTable({
        'paging'      : true,
        'lengthChange': false,
        'searching'   : false,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false
      })
    })
  </script>


  @endsection