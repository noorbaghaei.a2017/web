@extends('template.app')


@section('content')

 <!-- content-->
 <div class="content">

 @include('template.auth.header-welcome')

                    <!--  section  -->
                    <section class="gray-bg main-dashboard-sec" id="sec1">
                        <div class="container">
                        @include('template.auth.menu')
                           <!-- dashboard content-->
                           
                               
                                    <div class="col-md-9">
                                       
                                        <!-- dashboard-list-box--> 
                                        <div class="dashboard-list-box mar-dash-list fl-wrap">
                                          
                                        <table class="table table-border checkout-table">
                                    <tbody>
                                        <tr>
                                            <th class="hidden-xs">Item</th>
                                            <th>Description</th>
                                            <th class="hidden-xs">Price</th>
                                            <th>Quantity</th>
                                            <th>Total</th>
                                            <th></th>
                                        </tr>
                                     
                                      
                                    </tbody>
                                </table>
                                                                              
                                        </div>
                                        <!-- dashboard-list-box end--> 
                                      
                                    </div>
                                   
                             
                           
                            <!-- dashboard content end-->
                        </div>
                    </section>
                    <!--  section  end-->
                    <div class="limit-box fl-wrap"></div>
                </div>
                <!--content end-->


@endsection


@section('heads')

@if(LaravelLocalization::getCurrentLocale()=="fa" || LaravelLocalization::getCurrentLocale()=="ar")

<link type="text/css" rel="stylesheet" href="{{asset('template/css/dashboard-style.css')}}">
@else

<link type="text/css" rel="stylesheet" href="{{asset('template/css/ltr-dashboard-style.css')}}">
@endif
@endsection




