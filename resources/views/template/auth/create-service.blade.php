@extends('template.app')

@section('content')




<br>
<br>

<section>

@include('core::layout.alert-danger')

<h1>Edit your service</h1>



<form action="{{route('client.store.service')}}" method="POST" enctype="multipart/form-data">

@csrf

<div class="text-center">
            <ul class="nav nav-tabs" style="display: inline-block;">
                <li class="active"><a data-toggle="tab" href="#home">information service</a></li>
                <li><a data-toggle="tab" href="#menu1">food</a></li>
                <li><a data-toggle="tab" href="#menu2">drink</a></li>

            </ul>

            <div class="tab-content">
                <div id="home" class="tab-pane fade in active">


                <!-- start form information service -->
                  



<input type="file" name="image">
<br>
<label>  type : </label>
 <select  class="form-group">

      <option  class="form-control" value="restaurant">restaurant</option>

 </select>
<br>
<label>  country : </label>
 <select  class="form-group" name="country">

<option  class="form-control" value="German">german</option>

</select>

<br>
<label> city :</label>
<select  class="form-group" name="city">

<option data-subtext="" value="berlin">Berlin</option>
                <option  value="hamburg">Hamburg</option>
                <option value="Munich">Munich</option>
                <option  value="Cologne">Cologne</option>
                <option  value="Frankfurt">Frankfurt</option>
                <option  value="Stuttgart">Stuttgart</option>
                <option  value="Düsseldorf">Düsseldorf</option>
                <option  value="Dortmund">Dortmund</option>

</select>
<br>
<label>  title :</label>
<input type="text" name="title"  placeholder="title" value="">

<br>
<label>  excerpt :</label>
<input type="text" name="excerpt"  placeholder="excerpt" value="">

<br>
<label>  google map url :</label>
<input type="text" name="google_map"  placeholder="copy url " value="">

<br>

<label>  email :</label>
<input type="text" name="email"  placeholder="email " value="">



<br>

<label>  phone :</label>
<input type="text" name="phone"  placeholder="phone " value="">


<br>

<label>  address :</label>
<input type="text" name="address"  placeholder="address " value="">




<!-- end form information service -->


                </div>
                <div id="menu1" class="tab-pane fade">



                <!-- start form food -->
                  
   
  
  
  
  <input type="file" name="image-food">

 <br>
 <label>  title :</label>
  <input type="text" name="title-food"  placeholder="title" value="">
  
  <br>
  <label>  excerpt :</label>
  <input type="text" name="excerpt-food"  placeholder="excerpt" value="">
  
  <br>

  <label>  price :</label>
  <input type="text" name="price-food"  placeholder="price" value="">
  
 
  
  
  <!-- end form food    -->





                </div>

                <div id="menu2" class="tab-pane fade">



                      
                <!-- start form drink -->
                  
                  <form action="{{route('client.store.drink.service')}}" method="POST" enctype="multipart/form-data">
  
  @csrf
  
  
  
  <input type="file" name="image-drink">

  
  <br>
 
  <label>  title :</label>
  <input type="text" name="title-drink"  placeholder="title" value="">
  
  <br>
  <label>  excerpt :</label>
  <input type="text" name="excerpt-drink"  placeholder="excerpt" value="">
  

  <br>

  <label>  price :</label>
  <input type="text" name="price-drink"  placeholder="price" value="">
  
  
  
  
  <!-- end form drink -->




</div>

            </div>
<br>
            <input type="submit" value="Create" style="margin-bottom:10px;" class="btn btn-primary">
            <br>
        </div>




        
  
  </form>

</section>



@endsection

@section('heads')

<style>

.bottom-image{
    display:none;
}

</style>

@endsection




