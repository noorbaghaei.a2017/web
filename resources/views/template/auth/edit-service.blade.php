@extends('template.app')

@section('content')


@include('core::layout.alert-danger')

  <!-- content-->

  @include('template.auth.header-welcome')

                    <!--  section  -->
                    <section class="gray-bg main-dashboard-sec" id="sec1">
                        <div class="container">
                            <!--  dashboard-menu-->
                            <div class="col-md-3">
                                <div class="mob-nav-content-btn color2-bg init-dsmen fl-wrap"><i class="fal fa-bars"></i> منوی داشبورد</div>
                                <div class="clearfix"></div>
                                <div class="fixed-bar fl-wrap" id="dash_menu">
                                    <div class="user-profile-menu-wrap fl-wrap block_box">
                                        <!-- user-profile-menu-->
                                        <div class="user-profile-menu">
                                            <h3>اصلی</h3>
                                            <ul class="no-list-style">
                                                <li><a href="dashboard.html"><i class="fal fa-chart-line"></i>داشبورد</a></li>
                                                <li><a href="dashboard-feed.html"><i class="fal fa-rss"></i>اطلاعیه <span>7</span></a></li>
                                                <li><a href="dashboard-myprofile.html" class="user-profile-act"><i class="fal fa-user-edit"></i> ویرایش پروفایل</a></li>
                                                <li><a href="dashboard-messages.html"><i class="fal fa-envelope"></i> پیام ها <span>3</span></a></li>
                                                <li><a href="dashboard-password.html"><i class="fal fa-key"></i>تغییر رمز عبور</a></li>
                                            </ul>
                                        </div>
                                        <!-- user-profile-menu end-->
                                        <!-- user-profile-menu-->
                                        <div class="user-profile-menu">
                                            <h3>تبلیغ ها</h3>
                                            <ul  class="no-list-style">
                                                <li><a href="dashboard-listing-table.html"><i class="fal fa-th-list"></i> تبلیغ های من  </a></li>
                                                <li><a href="dashboard-bookings.html"> <i class="fal fa-calendar-check"></i> رزرو <span>2</span></a></li>
                                                <li><a href="dashboard-review.html"><i class="fal fa-comments-alt"></i> نظرات </a></li>
                                                <li><a href="dashboard-add-listing.html"><i class="fal fa-file-plus"></i> تبلیغ جدید</a></li>
                                            </ul>
                                        </div>
                                        <!-- user-profile-menu end-->                                        
                                        <button class="logout_btn color2-bg">خروج <i class="fas fa-sign-out"></i></button>
                                    </div>
                                </div>
                                <a class="back-tofilters color2-bg custom-scroll-link fl-wrap" href="#dash_menu">بازگشت<i class="fas fa-caret-up"></i></a>
                            </div>
                            <!-- dashboard-menu  end-->
                            <!-- dashboard content-->
                            <div class="col-md-9">
                                <div class="dashboard-title fl-wrap">
                                    <h3>پروفایل شما</h3>
                                </div>
                                <!-- profile-edit-container--> 
                                <div class="profile-edit-container fl-wrap block_box">
                                    <div class="custom-form">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label>نام <i class="fal fa-user"></i></label>
                                                <input type="text" placeholder="آلیسا" value=""/>                                                
                                            </div>
                                            <div class="col-sm-6">
                                                <label>نام خانوادگی <i class="fal fa-user"></i></label>
                                                <input type="text" placeholder="نوری" value=""/>                                                
                                            </div>
                                            <div class="col-sm-6">
                                                <label>آدرس ایمیل<i class="far fa-envelope"></i>  </label>
                                                <input type="text" placeholder="Alisa15236@domain.com" value=""/>                                                
                                            </div>
                                            <div class="col-sm-6">
                                                <label>تلفن<i class="far fa-phone"></i>  </label>
                                                <input type="text" placeholder="0211234567" value=""/>                                                
                                            </div>
                                            <div class="col-sm-6">
                                                <label> آدرس <i class="fas fa-map-marker"></i>  </label>
                                                <input type="text" placeholder="ایران , تهران , زعفرانیه" value=""/>                                                
                                            </div>
                                            <div class="col-sm-6">
                                                <label> سایت <i class="far fa-globe"></i>  </label>
                                                <input type="text" placeholder="rtl-theme.com" value=""/>                                                
                                            </div>
                                        </div>
                                        <label> یادداشت</label>                                              
                                        <textarea cols="40" rows="3" placeholder="درمورد من" style="margin-bottom:20px;"></textarea>
                                        <label>تغییر آواتار</label> 
                                        <div class="photoUpload">
                                            <span><i class="fal fa-image"></i> <strong>اضافه کردن عکس</strong></span>
                                            <input type="file" class="upload">
                                        </div>
                                    </div>
                                </div>
                                <!-- profile-edit-container end--> 
                                <div class="dashboard-title dt-inbox fl-wrap">
                                    <h3>شبکه اجتماعی شما</h3>
                                </div>
                                <!-- profile-edit-container--> 
                                <div class="profile-edit-container fl-wrap block_box">
                                    <div class="custom-form">
                                        <label>فیس بوک <i class="fab fa-facebook"></i></label>
                                        <input type="text" placeholder="https://www.facebook.com/" value=""/>
                                        <label>توییتر<i class="fab fa-twitter"></i>  </label>
                                        <input type="text" placeholder="https://twitter.com/" value=""/>
                                        <label>Vkontakte<i class="fab fa-vk"></i>  </label>
                                        <input type="text" placeholder="https://vk.com" value=""/>
                                        <label> اینستاگرام <i class="fab fa-instagram"></i>  </label>
                                        <input type="text" placeholder="https://www.instagram.com/" value=""/>
                                        <button class="btn    color2-bg  float-btn">ذخیره تغییرات<i class="fal fa-save"></i></button>
                                    </div>
                                </div>
                                <!-- profile-edit-container end-->                                    
                            </div>
                            <!-- dashboard content end-->
                        </div>
                    </section>
                    <!--  section  end-->
                    <div class="limit-box fl-wrap"></div>
                </div>
                <!--content end-->


@endsection





