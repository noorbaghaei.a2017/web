@extends('template.auth.app')


@section('content')




  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    

    <!-- Main content -->
    <section class="content">

        <div class="row">
          <div class="col-md-3">
  
            <!-- Profile Image -->
            <div class="box box-primary">
              <div class="box-body box-profile">

                @if($client->Hasmedia(config('cms.collection-image')))
               
                <img class="profile-user-img img-responsive img-circle" src="{{$client->getFirstMediaUrl(config('cms.collection-image'))}}" alt="User profile picture">


                
                         @else
                       

                         <img class="profile-user-img img-responsive img-circle"  src="{{asset('template/user/img/no-user.png')}}" alt="User profile picture">


                    @endif


                <h3 class="profile-username text-center"> {{ $client->full_name }} </h3>
  
                <p class="text-muted text-center"> {{ $client->mobile }} </p>
  
                <ul class="list-group list-group-unbordered">
                  <li class="list-group-item">
                    <b>زیر مجموعه</b> <a class="pull-left">0</a>
                  </li>
                
                </ul>
  
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
  
            <!-- About Me Box -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">درباره من</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
               
  
                <strong><i class="fa fa-file-text-o margin-r-5"></i> درباره من</strong>
  
                <p>{{ $client->about }}</p>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#settings" data-toggle="tab"> تنظیمات</a></li>
               
              </ul>
              <div class="tab-content">

              
  
                <div class="active tab-pane" id="settings">

                  @include('template.alert.error')
                  @include('template.alert.success')
                
                    <form action="{{route('foreign.client.update',['client'=>$client])}}" method="POST" class="form-horizontal" enctype="multipart/form-data">
                      @csrf
                      @method('PATCH')

                      <div class="form-group">
                        <label for="inputName"   class="col-sm-2 control-label">عکس پروفایل </label>
    
                        <div class="col-sm-10">
                          <input type="file" class="form-control"  name="image" >
                        </div>
                      </div>

                     

                    <div class="form-group">
                      <label for="inputName" class="col-sm-2 control-label">نام</label>
  
                      <div class="col-sm-10">
                        <input type="text" name="first_name"  value="{{ $client->first_name }}" class="form-control" id="inputName" placeholder="نام">
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputName"   class="col-sm-2 control-label">نام خانوادگی</label>
  
                      <div class="col-sm-10">
                        <input type="text" name="last_name" class="form-control" value="{{ $client->last_name }}" id="inputName" placeholder="نام خانوادگی">
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputName" class="col-sm-2 control-label">آدرس</label>
  
                      <div class="col-sm-10">
                        <input type="text" name="address"  value="{{ $client->address }}" class="form-control" id="inputName" placeholder="آدرس">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputName" class="col-sm-2 control-label">کد پستی</label>
  
                      <div class="col-sm-10">
                        <input type="text" name="postal_code"  value="{{ $client->postal_code }}" class="form-control" id="inputName" placeholder="کد پستی">
                      </div>
                    </div>
                   

                  
                   

                    <div class="form-group">
                        <label for="inputEmail" class="col-sm-2 control-label">ایمیل</label>
    
                        <div class="col-sm-10">
                          <input type="email" name="email" class="form-control" value="{{ $client->email }}" id="inputEmail" placeholder="ایمیل">
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="inputEmail" class="col-sm-2 control-label">موبایل</label>
    
                        <div class="col-sm-10">
                          <input type="text" name="mobile" class="form-control" value="{{ $client->mobile }}" id="inputEmail" placeholder="موبایل">
                        </div>
                      </div>

                     
                    
                    <div class="form-group">
                      <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-danger">بروز رسانی</button>
                      </div>
                    </div>
                  </form>
                </div>
                <!-- /.tab-pane -->
              </div>
              <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
  
      </section>
      <!-- /.content -->

    </div>
    <!-- /.content-wrapper -->
  




@endsection



@section('scripts')




@endsection




