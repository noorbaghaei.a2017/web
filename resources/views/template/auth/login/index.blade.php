@extends('template.app')

@section('content')



@include('template.sections.modal-menu')
    
  
    
    
    <section class="user-area-all-style log-in-area ptb-100">
    <div class="container">
    <div class="row">
    <div class="col-12">
    <div class="contact-form-action">
   
        <form action="{{ route('client.login.submit') }}" method="POST" class="login100-form validate-form">
            @csrf
    <div class="row">
    
    <div class="col-12">
    <div class="form-group">
        @error('mobile')
        <span class="invalid-feedback" role="alert" style="display: block">
                            <strong>{{ $message }}</strong>
                        </span>
              

        @enderror
    <input class="form-control" type="text" name="mobile" style="{{ $errors->has('mobile') ? ' border:1px solid red' : '' }}" placeholder="موبایل" autocomplete="off">
    </div>
    </div>
    <div class="col-12">
    <div class="form-group">
        @error('password')
        <span class="invalid-feedback" role="alert" style="display: block">
                            <strong>{{ $message }}</strong>
                        </span>
        @enderror
    <input class="form-control" type="password" name="password" style="{{ $errors->has('password') ? ' border:1px solid red' : '' }}" placeholder="کلمه عبور" autocomplete="off">
    </div>
    </div>
   
    
    <div class="col-12">
    <button class="default-btn btn-two" type="submit">
    وارد شوید
    </button>
    </div>
    <div class="col-12">
    <p class="account-desc">
    
    <a href="{{route('client.register')}}">ثبت نام</a>
    </p>
    </div>
    </div>
    </form>
    </div>
    </div>
    </div>
    </div>
    </section>
    
    

@endsection




