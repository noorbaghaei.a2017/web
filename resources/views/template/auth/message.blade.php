@extends('template.app')


@section('content')

 <!-- content-->
 <div class="content">

 @include('template.auth.header-welcome')

                    <!--  section  -->
                    <section class="gray-bg main-dashboard-sec" id="sec1">
                        <div class="container">
                        @include('template.auth.menu')
                           <!-- dashboard content-->
                           <div class="col-md-9">
                           @include('template.alert.error')
                            @include('template.alert.success')
                                <!-- dashboard-list-box-->         
                                <div class="dashboard-list-box fl-wrap">
                                    <div class="dashboard-header color-bg fl-wrap">
                                        <h3>{{__('cms.message')}}</h3>
                                    </div>
                                    <div class="chat-wrapper fl-wrap">
                                        <div class="row">
                                            <!-- chat-box--> 
                                            <div class="col-sm-12">
                                  
                                            <form action="{{route('client.send.chat')}}" method="GET">
                                                <div class="chat-box fl-wrap">

                                                @foreach($chats as $chat)



                                                        @if($chat->user==null)

                                                              <!-- message--> 
                                                    <div class="chat-message chat-message_user fl-wrap">
                                                        <div class="dashboard-message-avatar">
                                                        @if(!$client->Hasmedia('images'))
                                                        <img src="{{asset('template/images/user-icon-2.jpg')}}" alt="">
                                                    
                                                        @else
                                                        <img src="{{$client->getFirstMediaUrl('images')}}" alt="">
                                                        @endif
                                                           
                                                            <span class="chat-message-user-name cmun_sm">you</span>
                                                        </div>
                                                        <span class="massage-date">{{$chat->created_at->ago()}}</span>
                                                        <p>{{$chat->text}}</p>
                                                    </div>
                                                    <!-- message end-->                                             
                                                                                                                                
                                                        @else


                                                <!-- message--> 
                                                <div class="chat-message chat-message_guest fl-wrap">
                                                        <div class="dashboard-message-avatar">
                                                            <img src="{{asset('template/images/user-icon-2.jpg')}}" alt="">
                                                            <span class="chat-message-user-name cmun_sm">iniaz</span>
                                                        </div>
                                                        <span class="massage-date">{{$chat->created_at->ago()}}  </span>
                                                        <p>{{$chat->text}}</p>
                                                    </div>
                                                    <!-- message end-->
                                                    @endif

                                                    @endforeach






                                                                  
                                                          
                                                    



                                                </div>
                                                <div class="chat-widget_input fl-wrap">
                                                    <textarea name="message" placeholder="{{__('cms.text')}}"></textarea>                                                 
                                                    <button type="submit"><i class="fal fa-paper-plane"></i></button>
                                                </div>
                                                </form>
                                            </div>
                                            <!-- chat-box end--> 
                                           
                                        </div>
                                    </div>
                                    <!-- dashboard-list-box end--> 
                                </div>
                            </div>
                            <!-- dashboard content end-->
                        </div>
                    </section>
                    <!--  section  end-->
                    <div class="limit-box fl-wrap"></div>
                </div>
                <!--content end-->



@endsection


@section('heads')


@if(LaravelLocalization::getCurrentLocale()=="fa" || LaravelLocalization::getCurrentLocale()=="ar")

<link type="text/css" rel="stylesheet" href="{{asset('template/css/dashboard-style.css')}}">
@else

<link type="text/css" rel="stylesheet" href="{{asset('template/css/ltr-dashboard-style.css')}}">
@endif

@endsection


