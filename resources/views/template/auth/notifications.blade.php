@extends('template.app')


@section('content')

 <!-- content-->
 <div class="content">
 
 @include('template.auth.header-welcome')

                    <!--  section  -->
                    <section class="gray-bg main-dashboard-sec" id="sec1">
                        <div class="container">
                        @include('template.auth.menu')
                           <!-- dashboard content-->
                           
                               
                                    <div class="col-md-9">
                                    @include('template.alert.success')
                                    @include('template.alert.error')
                                        <!-- dashboard-list-box--> 
                                        <div class="dashboard-list-box mar-dash-list fl-wrap">

    
    @if(itemNotificationEmailClient())
                                <!-- dashboard-list end-->    
                                <div class="dashboard-list jobs fl-wrap" style="margin-bottom:0;">
                                        <div class="dashboard-message">
                                        <form action="{{route('client.verify.request.email',['token'=>auth('client')->user()->token])}}" method="POST">
                                        @csrf
                                        <button class="new-dashboard-item" type="submit" style="color:#fdfdfd;"  ><i class="fal fa-plus"></i></button>
                                        </form>    
                                            <div class="dashboard-message-text">
                                                <i class="far fa-exclamation-triangle red-bg"></i> 
                                                <p> {{__('cms.you-dont-have-verify-email')}} </p>
                                            </div>
                                            <div class="dashboard-message-time"></div>
                                        </div>
                                    </div>
                                    <!-- dashboard-list end--> 
                                    @endif 
    @if(itemNotificationJobClient())
 <!-- dashboard-list end-->    
 <div class="dashboard-list jobs fl-wrap" style="margin-bottom:0;">
                                        <div class="dashboard-message">
                                        <span class="new-dashboard-item"><a style="color:#fdfdfd;" href="{{route('client.create.bussiness')}}"><i class="fal fa-plus"></i></a></span>
                                            <div class="dashboard-message-text">
                                                <i class="far fa-exclamation-triangle red-bg"></i> 
                                                <p> {{__('cms.you-dont-have-job')}} </p>
                                            </div>
                                            <div class="dashboard-message-time"></div>
                                        </div>
                                    </div>
                                    <!-- dashboard-list end-->  

    @endif   
    @if(itemNotificationProductClient())
 <!-- dashboard-list end-->    
 <div class="dashboard-list jobs fl-wrap" style="margin-bottom:0;">
                                        <div class="dashboard-message">
                                        <span class="new-dashboard-item"><a style="color:#fdfdfd;" href="{{route('client.create.product')}}"><i class="fal fa-plus"></i></a></span>
                                            <div class="dashboard-message-text">
                                                <i class="far fa-exclamation-triangle red-bg"></i> 
                                                <p> {{__('cms.you-dont-have-product')}} </p>
                                            </div>
                                            <div class="dashboard-message-time"></div>
                                        </div>
                                    </div>
                                    <!-- dashboard-list end-->  

    @endif   
    @if(itemNotificationWerbungClient())
 <!-- dashboard-list end-->    
 <div class="dashboard-list jobs fl-wrap" style="margin-bottom:0;">
                                        <div class="dashboard-message">
                                        <span class="new-dashboard-item"><a style="color:#fdfdfd;" href="{{route('client.pre.create.werbung')}}"><i class="fal fa-plus"></i></a></span>
                                            <div class="dashboard-message-text">
                                                <i class="far fa-exclamation-triangle red-bg"></i> 
                                                <p> {{__('cms.you-dont-have-werbung')}} </p>
                                            </div>
                                            <div class="dashboard-message-time"></div>
                                        </div>
                                    </div>
                                    <!-- dashboard-list end-->  

    @endif                                 
                               


                          


                                           
                                                                              
                                        </div>
                                        <!-- dashboard-list-box end--> 
                                      
                                    </div>
                                   
                             
                           
                            <!-- dashboard content end-->
                        </div>
                    </section>
                    <!--  section  end-->
                    <div class="limit-box fl-wrap"></div>
                </div>
                <!--content end-->



@endsection


@section('heads')


@if(LaravelLocalization::getCurrentLocale()=="fa" || LaravelLocalization::getCurrentLocale()=="ar")

<link type="text/css" rel="stylesheet" href="{{asset('template/css/dashboard-style.css')}}">
@else

<link type="text/css" rel="stylesheet" href="{{asset('template/css/ltr-dashboard-style.css')}}">
@endif

@endsection


