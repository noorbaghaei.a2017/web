@extends('template.app')


@section('content')




  <!-- content-->
  <div class="content">
  <input id="lang_form" type="text" value="{{LaravelLocalization::getCurrentLocale()}}" hidden>

  @include('template.auth.header-welcome')
                    <!--  section  -->
                    <section class="gray-bg main-dashboard-sec" id="sec1">
                        <div class="container">
                            @include('template.auth.menu')
                            <!-- dashboard content-->
                            <div class="col-md-9">
                             
                            <form action="{{route('client.update.product',['product'=>$item->token])}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    @method('PATCH')
                                  
                                <!-- profile-edit-container--> 
                                <div class="profile-edit-container fl-wrap block_box">
                                    <div class="dashboard-title  dt-inbox fl-wrap">

<h3>{{__('cms.info')}}</h3>
</div>
                                @include('template.alert.error')
                                <div style="margin-bottom:10px"></div>
                                    <div class="custom-form">

                                    <div class="row">
                                                <div class="col-sm-12">
                                                <label>{{__('cms.title')}} <i class="fal fa-user"></i></label>
                                                <input type="text" name="title" class="{{$errors->has('title') ? 'error-input' : ''}}" placeholder="{{__('cms.title')}}" value="{{old('title') ? old('title') : $item->title}}" autocomplete="off"/>                                                
                                            </div>
                                           
                                            <div class="col-sm-12">
                                                <label>{{__('cms.excerpt')}}  <i class="fal fa-user"></i></label>
                                                <input type="text" name="excerpt" class="{{$errors->has('excerpt') ? 'error-input' : ''}}"  placeholder="{{__('cms.excerpt')}}" value="{{old('excerpt') ? old('excerpt') : $item->excerpt}}" autocomplete="off"/>                                                
                                            </div>
                                            <div class="col-sm-12" style="margin-bottom:15px">
                                                <label>{{__('cms.text')}}  <i class="fal fa-text"></i></label>
                                                <textarea type="text" name="text" class="{{$errors->has('text') ? 'error-input' : ''}}" placeholder="{{__('cms.text')}}">{{old('text') ? old('text') : $item->text}}</textarea>                                                
                                            </div>

                                    <div class="col-sm-6">
                                             <label> {{__('cms.category')}} <i class="far fa-globe"></i>  </label>
                                             <select  class="chosen-select no-search-select {{$errors->has('category') ? 'error-input' : ''}}" type="text" name="category" onchange="loadSubCategory(event)">
                                             @if(showServiceProduct($item->category)->parent==0)
                                             @foreach ($all_parent_category_products as $category)

                                            
                                             <option value="{{$category->id}}" {{$item->category==$category->id ? "selected" : ""}}>{!! convert_lang($category,LaravelLocalization::getCurrentLocale(),'title') !!}</option>

                                            

                                             @endforeach
                                             @else
                                            
                                            

                                                 @foreach ($all_parent_category_products as $category)
                                                 <option value="{{$category->id}}" {{showServiceProduct($item->category)->parent==$category->id ? "selected" : ""}}>{!! convert_lang($category,LaravelLocalization::getCurrentLocale(),'title') !!}</option>
                                                 @endforeach

                                                 
                                                

                                             @endif


                                             </select>                                                
                                             </div>

                                             <div class="col-sm-6" >
                                             <label> {{__('cms.sub_category')}} <i class="far fa-globe"></i>  </label>
                                             <select id="sub_category"   class="chosen-select no-search-select {{$errors->has('sub_category') ? 'error-input' : ''}}" type="text" name="sub_category" >

                                           
                                             

                                             @foreach($all_parent_category_products_child->where('parent',showServiceProduct($item->category)->parent) as $sub_category)
                                             
                                             <option value="{{$sub_category->id}}" {{$item->category==$sub_category->id ? "selected" : ""}}>{!! convert_lang($sub_category,LaravelLocalization::getCurrentLocale(),'title') !!}</option>
                                             

                                             @endforeach

                                            


                                             </select>                                                
                                             </div>


                                          
                                           
</div>

</div>
</div>


<div class="dashboard-title  dt-inbox fl-wrap">

<h3>{{__('cms.thumbnail')}} </h3>
</div>

   <!-- profile-edit-container--> 
   <div class="profile-edit-container fl-wrap block_box">


    <div style="margin-bottom:10px"></div>
    <div class="custom-form">

            <div class="row">

<!--col --> 
<div class="col-md-12">
<div>
<br>
<div class="info-image">
                        <p style="color:red">{{__('cms.max-pixel')}} : 468px * 376px</p>
                        <p style="color:red">{{__('cms.max-size')}} : 3MB</p>
                        </div>
                        </div>
       <div class="add-list-media-wrap">
             
              @if(!$item->Hasmedia('images'))
                      <div id="bg" class="fuzone">
                   <div id="text-image" class="fu-text">
                       <span><i class="fal fa-image"></i>  {{__('cms.add')}}</span>
                   </div>
                       <input type="file" name="image" class="upload" onchange="loadFile(event)">
                      

               </div>
                       
                        @else

                        <div id="bg" class="fuzone" style="background:url({{$item->getFirstMediaUrl('images')}});background-size:contain;background-repeat: no-repeat;">
                   <div id="text-image" class="fu-text" style="display:none">
                       <span><i class="fal fa-image"></i>  {{__('cms.add')}}</span>
                   </div>
                       <input type="file" name="image" class="upload" onchange="loadFile(event)">
                      

               </div>
                       
                       
                        @endif
               

         
       </div>
   </div>
   <!--col end--> 

            </div>                                  
                                            
                                      
    </div>
</div>                                       






                           <!-- profile-edit-container--> 
                           <div class="profile-edit-container fl-wrap block_box">
                                    <div class="dashboard-title  dt-inbox fl-wrap">

<h3>{{__('cms.location')}} / {{__('cms.contacts')}}</h3>
</div>
                              
                                <div style="margin-bottom:10px"></div>
                                    <div class="custom-form">
                                    <div class="row">

                                           <div style="margin-bottom:15px"></div>
                                           
                                           <div class="col-sm-6">
                                                <label>{{__('cms.email')}}  <i class="fal fa-user"></i></label>
                                                <input type="text" name="email" class="{{$errors->has('email') ? 'error-input' : ''}}"  placeholder="{{__('cms.email')}}" value="{{old('email') ? old('email') : $item->email}}" autocomplete="off"/>                                                
                                            </div>
                                           
                                            <div class="col-sm-6">
                                                <label >{{__('cms.price')}} * <i class="fas fa-euro-sign"></i></label>
                                                <input type="text" name="price" class="{{$errors->has('price') ? 'error-input' : ''}}" placeholder="{{__('cms.price')}}" value="{{old('price') ? old('price') : $item->price}}" autocomplete="off"/>                                                
                                            </div>
                                            <div class="col-sm-6">
                                                <label>{{__('cms.mobile')}}  <i class="fal fa-user"></i></label>
                                                <input type="text" name="mobile" class="{{$errors->has('mobile') ? 'error-input' : ''}}"  placeholder="{{__('cms.mobile')}}" value="{{old('mobile') ? old('mobile') : $item->mobile}}" autocomplete="off"/>                                                
                                            </div>
                                           
                                         
                                    </div>

                                    <div class="row">
                                         
                                         <div class="col-sm-6">
                                             <label> {{__('cms.address')}} <i class="fas fa-map-marker"></i>  </label>
                                             <input type="text" name="address" class="{{$errors->has('address') ? 'error-input' : ''}}" placeholder="{{__('cms.address')}}" value="{{old('address') ? old('address') : $item->address}}" autocomplete="off"/>                                                
                                         </div>
                                        
                                         <div class="col-sm-6">
                                             <label> {{__('cms.postal_code')}} <i class="far fa-globe"></i>  </label>
                                             <input id="postal_code" class="{{$errors->has('postal_code') ? 'error-input' : ''}}" onkeyup="loadCity(event)" type="text" name="postal_code" placeholder="{{__('cms.postal_code')}}" value="{{old('postal_code') ? old('postal_code') : $item->postal_code}}" autocomplete="off"/>                                                
                                         </div>
                                         <div class="col-sm-6">
                                         <input id="longitude" hidden name="longitude" type="text" value="{{$item->longitude}}">
                                         <input id="latitude" hidden name="latitude" type="text" value="{{$item->latitude}}">
                                             <label> {{__('cms.city')}}  </label>
                                             <select id="city" class="chosen-select no-search-select {{$errors->has('city') ? 'error-input' : ''}}" type="text" name="city">
                                             @if(!empty(old('city')))
                                             <option value="{{old('city')}}" selected>{{old('city')}}</option>
                                             
                                             @else
                                             <option value="{{$item->city}}" selected>{{$item->city}}</option>
                                                @endif
                                             </select>                                                
                                         </div>
                                        



                                       
                                        
                                     </div>
                                       


                                    </div>

                                </div>
                                <!-- profile-edit-container end--> 




<button type="submit" class="logout_btn color2-bg">{{__('cms.save')}} <i class="fas fa-sign-out"></i></button>


                                </form>     
                        </div>
                    </section>
                    <!--  section  end-->
                    <div class="limit-box fl-wrap"></div>
                </div>
                <!--content end-->


@endsection


@section('heads')

@if(LaravelLocalization::getCurrentLocale()=="fa" || LaravelLocalization::getCurrentLocale()=="ar")

<link type="text/css" rel="stylesheet" href="{{asset('template/css/dashboard-style.css')}}">
@else

<link type="text/css" rel="stylesheet" href="{{asset('template/css/ltr-dashboard-style.css')}}">
@endif
@endsection



@section('scripts')

<script>


$('input[name=price]').keyup(function(){
   
   $(this).val($(this).val().replace(/[^\d.]/,''));
});




$('#showCheckoutHistory').change(function() {
        if ($(this).prop('checked')) {
            alert("You have elected to show your checkout history."); //checked
        }
        else {
            alert("You have elected to turn off checkout history."); //not checked
        }
    });

    function changeStatusBanner(e) {

        $('#banner_info').hide();
        $('#slide_info').hide();
        $('#video_info').hide();
      $target="#"+e.target.dataset.info;
      $($target).show();
	 
};

function loadFile(e) {
	 image = $('#bg');
     text = $('#text-image');
    text.hide();
    
    image.attr('style','background-image:url("'+URL.createObjectURL(event.target.files[0])+'");background-size:contain;background-repeat: no-repeat');
    
};

function loadSlide1(e) {
	 image = $('#slide_1');
     text = $('#text-slide-1');
    text.hide();
    
    image.attr('style','background-image:url("'+URL.createObjectURL(event.target.files[0])+'");background-size:contain;background-repeat: no-repeat;');
    
};
function loadSlide2(e) {
	 image = $('#slide_2');
     text = $('#text-slide-2');
    text.hide();
    
    image.attr('style','background-image:url("'+URL.createObjectURL(event.target.files[0])+'");background-size:contain;background-repeat: no-repeat;');
    
};
function loadSlide3(e) {
	 image = $('#slide_3');
     text = $('#text-slide-3');
    text.hide();
    
    image.attr('style','background-image:url("'+URL.createObjectURL(event.target.files[0])+'");background-size:contain;background-repeat: no-repeat;');
    
};

function clearFileBanner(){
    image = $('#banner');
     text = $('#text-banner');
    text.show();
    
    image.attr('style','background-image:url("")');
    
    $banner=$('input[name="banner"]');
    $banner.val('');
   
}
function clearFileImage(){
    image = $('#bg');
     text = $('#text-image');
    text.show();
    
    image.attr('style','background-image:url("")');
    
    $banner=$('input[name="image"]');
    $banner.val('');
   
}
function clearFileSlide1(){
    image = $('#slide_1');
     text = $('#text-slide-1');
    text.hide();
    
    image.attr('style','background-image:url("")');
    
    
    $banner=$('input[id="slide_1"]');
    $banner.val('');
   
}

function clearFileSlide2(){
    image = $('#slide_2');
     text = $('#text-slide-2');
    text.hide();
    
    image.attr('style','background-image:url("")');
    
    
    $banner=$('input[id="slide_2"]');
    $banner.val('');
   
}
function clearFileSlide3(){
    image = $('#slide_3');
     text = $('#text-slide-3');
    text.hide();
    
    image.attr('style','background-image:url("")');
    
    
    $banner=$('input[id="slide_3"]');
    $banner.val('');
   
}
function clearFileGallery1(){
    image = $('#photo_1');
     text = $('#text-photo-1');
    text.hide();
    
    image.attr('style','background-image:url("")');
    
    
    $banner=$('input[id="photo_1"]');
    $banner.val('');
   
}
function clearFileGallery2(){
    image = $('#photo_2');
     text = $('#text-photo-2');
    text.hide();
    
    image.attr('style','background-image:url("")');
    
    
    $banner=$('input[id="photo_2"]');
    $banner.val('');
   
}
function clearFileGallery3(){
    image = $('#photo_3');
     text = $('#text-photo-3');
    text.hide();
    
    image.attr('style','background-image:url("")');
    
    
    $banner=$('input[id="photo_3"]');
    $banner.val('');
   
}



function loadPhoto1(e) {
	 image = $('#photo_1');
     text = $('#text-photo-1');
    text.hide();
    
    image.attr('style','background-image:url("'+URL.createObjectURL(event.target.files[0])+'");background-size:contain;background-repeat: no-repeat;');
    
};
function loadPhoto2(e) {
	 image = $('#photo_2');
     text = $('#text-photo-2');
    text.hide();
    
    image.attr('style','background-image:url("'+URL.createObjectURL(event.target.files[0])+'");background-size:contain;background-repeat: no-repeat;');
    
};
function loadPhoto3(e) {
	 image = $('#photo_3');
     text = $('#text-photo-3');
    text.hide();
    
    image.attr('style','background-image:url("'+URL.createObjectURL(event.target.files[0])+'");background-size:contain;background-repeat: no-repeat;');
    
};

function loadBanner(e) {
	 image = $('#banner');
     text = $('#text-banner');
    text.hide();
    
    image.attr('style','background-image:url("'+URL.createObjectURL(event.target.files[0])+'");background-size:contain;background-repeat: no-repeat;');
    
};



function loadSubCategory(e) {


     
$category= e.target.value;
$lang=$("html").attr("lang");




$.ajax({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    type:'POST',
    contentType:'application/json; charset=utf-8',
    url:'/ajax/load/sub/category/product/'+$category+"/"+$lang,
    data: { field1:$category,field2:$lang} ,
    beforeSend:function(){
    
},
beforeSend:function(){
    $('#sub_category + .nice-select').attr('style','opacity:0');
},

success: function (response) {


   if(response.data.status){

       console.log(response.data);

       $('#sub_category').empty();
     
       $('#sub_category + .nice-select .list').empty();

       $('#sub_category + .nice-select > span.current').text('');

       $('#sub_category + .nice-select .list').empty();
       
     
     //start for each for load select input 

       $.each(response.data.result, function(key, index) {
   
                        if(key==0){
                                $('#sub_category').append("<option value='"+index.id+"'   selected>"+index.translate+"</option>");
                                $('#sub_category + .nice-select > span.current').text(index.translate);
                            }
                            else{
                                $('#sub_category').append("<option value='"+index.id+"'   >"+index.translate+"</option>");
                                $('#sub_category + .nice-select  ul.list ').append("<li class='option'  data-value='"+index.id+"'   >"+index.translate+"</li>"); 
                            }
         
 
        });

       }


  //end for each for load select input 

   
},
error: function (xhr,ajaxOptions,thrownError) {

console.log(xhr,ajaxOptions,thrownError);

    $("#sub_category").empty();
    $('#sub_category + .nice-select .list').empty();
    $('#sub_category + .nice-select > span.current').text('');
    $('#sub_category + .nice-select').attr('style','opacity:0');

  
},
complete:function(){
    $('#sub_category + .nice-select').attr('style','opacity:1');
}

});
}

function loadCity(e) {

var myInput = document.getElementById("postal_code");
     


if(myInput.value.length==5){



console.log($('#latitude').val());
          

    $code=$("#postal_code").val();
   

$.ajax({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    type:'POST',
    contentType:'application/json; charset=utf-8',
    url:'/ajax/load/city/postalcode/'+$code,
    data: { field1:$code} ,
    beforeSend:function(){
        $('#city + .nice-select').attr('style','opacity:0');
},
    success: function (response) {
       if(response.data.status){

           console.log(response.data);
           $('#city').empty();
           $('#city + .nice-select .list').empty();
           $('#city + .nice-select > span.current').text('');
         
            $('#city').append("<option value='"+response.data.result[0].fields.plz_name+"' selected>"+response.data.result[0].fields.plz_name+"</option>");
           
           
            $('#city + .nice-select .list').empty();
                $('#city + .nice-select .list').append("<li data-value='"+response.data.result[0].fields.plz_name+"' class='option  selected focus'>"+response.data.result[0].fields.plz_name+"</li>");
           
                $('#city + .nice-select > span.current').text(response.data.result[0].fields.plz_name);

              

       }
       else{
        console.log('no');
       }
    },
    error: function (xhr,ajaxOptions,thrownError) {
    
    console.log(xhr,ajaxOptions,thrownError);
    $('#city + .nice-select > span.current').text('');
    $('#city + .nice-select').attr('style','opacity:0');

    },
    complete:function(){
        $('#city + .nice-select').attr('style','opacity:1');
}

});

}
}

</script>

@endsection




