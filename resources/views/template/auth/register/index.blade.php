@extends('template.app')

@section('content')


@include('template.sections.modal-menu')
    
    
   
    
    
    <section class="user-area-all-style sign-up-area ptb-100">
    <div class="container">
    <div class="row">
    <div class="col-12">
    <div class="contact-form-action">
    <div class="form-heading text-center">
    <h3 class="form-title">ایجاد حساب کاربری!</h3>
  
    </div>
    <form action="{{route('client.register.submit.client')}}" method="POST" class="login100-form validate-form">
        @csrf
    <div class="row">
    
    
   
    <div class="col-md-12 col-sm-12">
    <div class="form-group">
        @error('first_name')
        <span class="invalid-feedback" role="alert" style="display: block">
                            <strong>{{ $message }}</strong>
                        </span>
        @enderror
       
    <input class="form-control" type="text" name="first_name" value="{{ old('first_name') }}" style="{{ $errors->has('first_name') ? ' border:1px solid red' : 'border:1px solid orange' }}" placeholder=" نام ">
    </div>
    </div>
    <div class="col-md-12 col-sm-12 ">
    <div class="form-group">
        @error('last_name')
        <span class="invalid-feedback" role="alert" style="display: block">
                            <strong>{{ $message }}</strong>
                        </span>
        @enderror
    <input class="form-control" type="text" name="last_name" value="{{ old('last_name') }}" style="{{ $errors->has('last_name') ? ' border:1px solid red' : 'border:1px solid orange' }}" placeholder="نام خانوادگی">
    </div>
    </div>
    <div class="col-md-12 col-sm-12">
    <div class="form-group">
        @error('mobile')
        <span class="invalid-feedback" role="alert"  style="display: block">
                            <strong>{{ $message }}</strong>
                        </span>
        @enderror
    <input class="form-control" type="text" name="mobile" value="{{ old('mobile') }}" style="{{ $errors->has('mobile') ? ' border:1px solid red' : 'border:1px solid orange' }}" placeholder="09121234567 موبایل " autocomplete="off">
    </div>
    </div>
    
    <div class="col-md-12 col-sm-12">
    <div class="form-group">
        @error('password')
        <span class="invalid-feedback" role="alert" style="display: block">
                            <strong>{{ $message }}</strong>
                        </span>
        @enderror
    <input class="form-control" type="text" name="password" style="{{ $errors->has('password') ? ' border:1px solid red' : 'border:1px solid orange' }}" placeholder="کلمه عبور" autocomplete="off">
    </div>
    </div>
   
  
    <div class="col-12">
    <button class="default-btn btn-two" type="submit">
    ثبت نام
    </button>
    </div>
    <div class="col-12">
    <p class="account-desc">
    حساب کاربری داری؟
    <a href="{{route('client.login')}}"> ورود</a>
    </p>
    </div>
    </div>
    </form>
    </div>
    </div>
    </div>
    </div>
    </section>


@endsection


