  <!-- right side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-right image">

       
          @if($client->Hasmedia(config('cms.collection-image')))
               
          <img src="{{$client->getFirstMediaUrl(config('cms.collection-image'))}}" class="img-circle" alt="User Image">

                   @else

                   <img src="{{asset('template/user/img/no-user.png')}}" class="img-circle" alt="User Image">


              @endif
       
        </div>
        <div class="pull-right info">
          <p>{{ $client->full_name}}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> آنلاین</a>
        </div>
      </div>
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">منو</li>
        <li class="active">
          <a href="{{route('client.dashboard')}}">
            <i class="fa fa-dashboard"></i> <span>پنل کاربری</span>
           
          </a>
         
        </li>
        <li >
            <a href="{{ route('client.list.insurance') }}">
              <i class="fa fa-dashboard"></i>
               <span>بیمه</span>
               <span class="pull-left-container">
                <span class="label label-primary pull-left">{{ $client->countInsur() }}</span>
              </span>
            </a>
           
          </li>
        
      
       
      
       
       
        
  
    </ul>
    </section>
    <!-- /.sidebar -->
  </aside>