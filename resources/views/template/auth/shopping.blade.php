<!DOCTYPE html>
<html lang="fa">

<head>
    <meta charset="utf-8" />
    @if(!$setting->Hasmedia('logo'))
        <link rel="shortcut icon" href="{{asset('img/no-img.gif')}}">
        <link rel="apple-touch-icon" href="{{asset('img/no-img.gif')}}">
    @else
        <link rel="shortcut icon" href="{{$setting->getFirstMediaUrl('logo')}}">
        <link rel="apple-touch-icon" href="{{$setting->getFirstMediaUrl('logo')}}">
    @endif
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport' />
    <title>تکمیل سفارش</title>
    <!--     Fonts and icons     -->
    <link rel="stylesheet" href="{{asset('template/fonts/font-awesome/css/font-awesome.min.css')}}" />
    <!-- CSS Files -->
    <link href="{{asset('template/css/bootstrap.min.css')}}" rel="stylesheet" />
    <link href="{{asset('template/css/now-ui-kit.css')}}" rel="stylesheet" />
    <link href="{{asset('template/css/plugins/owl.carousel.css')}}" rel="stylesheet" />
    <link href="{{asset('template/css/plugins/owl.theme.default.min.css')}}" rel="stylesheet" />
    <link href="{{asset('template/css/main.css')}}" rel="stylesheet" />
</head>

<body>

<div class="wrapper default shopping-page">
    <!-- header-shopping -->
    <header class="header-shopping default">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center pt-2">
                    <div class="header-shopping-logo default">
                        <a href="{{route('front.website')}}">
                            @if($setting->Hasmedia('logo'))
                                <img src="{{$setting->getFirstMediaUrl('logo')}}" width="100">
                            @endif
                        </a>
                    </div>
                </div>
                <div class="col-12 text-center">
                    <ul class="checkout-steps">
                        <li>
                            <a href="#" class="active">
                                <span>اطلاعات ارسال</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span>پرداخت</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span>اتمام خرید و ارسال</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <!-- header-shopping -->
    <form action="{{route('register.receiver.information.product')}}" method="POST">
    @csrf
    <!-- main-shopping -->
    <main class="cart-page default">
        <div class="container">
            <div class="row">

                <div class="cart-page-content col-xl-9 col-lg-8 col-md-12 order-1">

                    <div class="cart-page-title">
                        <h1>انتخاب آدرس تحویل سفارش</h1>
                    </div>
                    <section class="page-content default">
                        <div class="address-section">
                            <div class="checkout-contact">
                                <div class="checkout-contact-content">
                                    <div class="checkout-contact-items">
                                        <div>
                                            @if ($errors->any())
                                                <div class="alert alert-danger">
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif
                                        </div>
                                      <div class="row">
                                          <div class="col-lg-8 col-md-12">
                                              <div class="form-group">
                                                      <label>نام </label>
                                                          <input class="form-control" type="text" name="first_name" value="{{$client->first_name}}" autocomplete="off">
                                              </div>
                                              <div class="form-group">
                                                      <label>نام خانوادگی </label>
                                                          <input class="form-control" type="text" name="last_name" value="{{$client->last_name}}" autocomplete="off">
                                              </div>
                                              <div class="form-group">

                                                  <label>موبایل </label>
                                                          <input id="EnglishDataMobile" class="form-control EnglishData" type="text" name="mobile" value="{{$client->mobile}}" autocomplete="off" maxlength="11">


                                              </div>
                                              <div class="form-group">

                                                      <label>کد پستی </label>
                                                          <input  id="EnglishDataPostal" class="form-control" type="text" name="postal_code" value="{{$client->postal_code}}" autocomplete="off" maxlength="10">

                                              </div>
                                              <div class="form-group">
                                                      <label>آدرس </label>
                                                          <textarea class="form-control"  placeholder="تهران تهران ..." name="address" rows="3" cols="5" >{{$client->address}}</textarea>
                                              </div>
                                          </div>
                                          <div class="col-lg-4 col-md-12">
                                              <img src="{{asset('template/img/delivery.jpg')}}" width="100%">
                                          </div>

                                      </div>


                                    </div>
                                    <div class="checkout-contact-badge">
                                        <i class="now-ui-icons ui-1_check"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <form method="post" id="shipping-data-form">
                            <div class="headline">
                                <span>انتخاب نحوه ارسال</span>
                            </div>
                            <div class="checkout-shipment">
                                <div class="radio">
                                    <input type="radio" name="radio1" id="radio1" value="option1">
                                    <label for="radio1">
                                        عادی
                                    </label>
                                </div>

                                <div class="radio">
                                    <input type="radio" name="radio1" id="radio2" value="option2" checked="">
                                    <label for="radio2">
                                        سریع‌ (مرسوله‌ها در سریع‌ترین زمان ممکن ارسال می‌شوند)
                                    </label>
                                </div>
                            </div>
                            <div class="headline">
                                <span>مرسوله ۱ از ۱</span>
                            </div>
                            <div class="checkout-pack">
                                <section class="products-compact">
                                    <div class="box">
                                        <div class="row">
                                            @foreach($client->cart->items as $item)
                                            <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                                                <div class="product-box-container">
                                                    <div class="product-box product-box-compact">
                                                        <a class="product-box-img">
                                                            <img src="{{$item->product_item->getFirstMediaUrl('images')}}">
                                                        </a>
                                                        <div class="product-box-title">
                                                            {{$item->product_item->title}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </section>
                                <div class="row">
                                    <div class="checkout-time-table checkout-time-table-time">
                                        <span class="checkout-additional-options-checkbox-image"></span>
                                        <div>
                                            <div
                                                class="checkout-time-table-title-bar checkout-time-table-title-bar-city">
                                                بازه تحویل سفارش: زمان تقریبی تحویل ۴۸ ساعته
                                             </div>
                                            <ul class="checkout-time-table-subtitle-bar">
                                                <li>شیوه ارسال : پست پیشتاز با ظرفیت اختصاصی برای آسون کالا</li>
                                                <li>هزینه ارسال:
                                                    @if(calcSend($client->cart->items)!=0)

                                                        <span class=""> {{number_format(calcSend($client->cart->items))}} تومان</span>
                                                    @else
                                                        <span class="">رایگان</span>
                                                    @endif
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </section>
                </div>
                <aside class="cart-page-aside col-xl-3 col-lg-4 col-md-6 center-section order-2">
                    <div class="checkout-aside">
                        <div class="checkout-summary">
                            <div class="checkout-summary-main">
                                <ul class="checkout-summary-summary">
                                    <li>
                                        @if(calcSend($client->cart->items)!=0)

                                            <span>هزینه ارسال</span>
                                            <span style="color: red;margin: 2px">{{number_format(calcSend($client->cart->items))}} تومان</span>
                                            <span>وابسته به آدرس<span class="wiki wiki-holder"><span
                                                        class="wiki-sign"></span>
                                                @else
                                                        <span>هزینه ارسال رایگان</span>
                                                    @endif
                                                    <div class="wiki-container js-dk-wiki is-right">
                                                        <div class="wiki-arrow"></div>
                                                        <p class="wiki-text">
                                                            هزینه ارسال مرسولات می‌تواند وابسته به شهر و آدرس گیرنده
                                                            متفاوت باشد. در
                                                            صورتی که هر
                                                            یک از مرسولات حداقل ارزشی برابر با ۱۰۰هزار تومان داشته باشد،
                                                            آن مرسوله
                                                            بصورت رایگان
                                                            ارسال می‌شود.<br>
                                                            "حداقل ارزش هر مرسوله برای ارسال رایگان، می تواند متغیر
                                                            باشد."
                                                        </p>
                                                    </div>
                                                </span></span>
                                    </li>
                                </ul>
                                <div class="checkout-summary-devider">
                                    <div></div>
                                </div>
                                <div class="checkout-summary-content">
                                    <div class="checkout-summary-price-title">قیمت کالا  :</div>
                                    <div class="checkout-summary-price-value">
                                        <span class="checkout-summary-price-value-amount">{{number_format($client->cart->items->sum('price'))}}</span>تومان
                                    </div>
                                    <div class="checkout-summary-price-title">مبلغ قابل پرداخت :</div>
                                    <div class="checkout-summary-price-value">
                                        <span class="checkout-summary-price-value-amount">{{number_format(totalPrice($client->cart->items))}}</span>تومان
                                    </div>
                                    <a href="{{route('client.show.payment')}}" class="selenium-next-step-shipping">
                                        <div class="parent-btn">
                                            <button class="dk-btn dk-btn-info" type="submit">
                                                ادامه ثبت سفارش
                                                <i class="now-ui-icons shopping_basket"></i>
                                            </button>
                                        </div>
                                    </a>
                                    <div>
                                            <span>
                                                کالاهای موجود در سبد شما ثبت و رزرو نشده‌اند، برای ثبت سفارش مراحل بعدی
                                                را تکمیل
                                                کنید.
                                            </span>
                                        <span class="wiki wiki-holder"><span class="wiki-sign"></span>
                                                <div class="wiki-container is-right">
                                                    <div class="wiki-arrow"></div>
                                                    <p class="wiki-text">
                                                        محصولات موجود در سبد خرید شما تنها در صورت ثبت و پرداخت سفارش
                                                        برای شما رزرو
                                                        می‌شوند. در
                                                        صورت عدم ثبت سفارش، آسون کالا هیچگونه مسئولیتی در قبال تغییر
                                                        قیمت یا موجودی
                                                        این کالاها
                                                        ندارد.
                                                    </p>
                                                </div>
                                            </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="checkout-feature-aside">
                            <ul>
                                <li class="checkout-feature-aside-item checkout-feature-aside-item-guarantee">
                                    هفت روز
                                    ضمانت تعویض
                                </li>
                                <li class="checkout-feature-aside-item checkout-feature-aside-item-cash">
                                    پرداخت در محل با
                                    کارت بانکی
                                </li>
                                <li class="checkout-feature-aside-item checkout-feature-aside-item-express">
                                    تحویل اکسپرس
                                </li>
                            </ul>
                        </div>
                    </div>
                </aside>

            </div>
        </div>
    </main>
    <!-- main-shopping -->

    </form>

    <footer class="main-footer default" xmlns="http://www.w3.org/1999/html">
        <div class="back-to-top">
            <a href="#"><span class="icon"><i class="now-ui-icons arrows-1_minimal-up"></i></span> <span>بازگشت به
                        بالا</span></a>
        </div>
        <div class="container">
            <div class="footer-services">
                <div class="row">
                    @foreach($properties as $property)
                        <div class="service-item col">
                            <a href="#" target="_blank">
                                @if(!$property->Hasmedia('images'))
                                    <img src="{{asset('img/no-img.gif')}}">
                                @else
                                    <img src="{{$property->getFirstMediaUrl('images')}}">
                                @endif

                            </a>
                            <p>{{$property->title}}</p>
                        </div>
                    @endforeach

                </div>
            </div>
            <div class="footer-widgets">
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-3">
                        <div class="widget-menu widget card">
                            <header class="card-header">
                                <h3 class="card-title">راهنمای خرید از درآسون کالا</h3>
                            </header>
                            <ul class="footer-menu">
                            </ul>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3">
                        <div class="widget-menu widget card">
                            <header class="card-header">
                                <h3 class="card-title">خدمات مشتریان</h3>
                            </header>
                            <ul class="footer-menu">

                            </ul>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3">
                        <div class="widget-menu widget card">
                            <header class="card-header">
                                <h3 class="card-title">آسون کالا</h3>
                            </header>
                            <ul class="footer-menu">
                            </ul>>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3">

                        <div class="socials">
                            <p>ما را در شبکه های اجتماعی دنبال کنید.</p>
                            <div class="footer-social">
                                <a href="https://www.instagram.com/{{$setting->info->instagram}}" target="_blank"><i class="fa fa-instagram"></i>اینستاگرام آسون کالا</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="info">
                <div class="row">
                    <div class="col-12 col-lg-4">
                        <span>هفت روز هفته ، 24 ساعت شبانه‌روز پاسخگوی شما هستیم.</span>
                    </div>
                    <div class="col-12 col-lg-2"> تماس: <a href="tel:{{json_decode($setting->mobile,true)[0]}}">{{json_decode($setting->mobile,true)[0]}}</a></div>
                    <div class="col-12 col-lg-2"> ایمیل:<a href="mailto:{{$setting->email}}">{{$setting->email}}</a></div>

                </div>
            </div>
        </div>
        <div class="description">
            <div class="container">
                <div class="row">
                    <div class="site-description col-12 col-lg-7">
                        <h1 class="site-title">فروشگاه اینترنتی آسون کالا، بررسی، انتخاب و خرید آنلاین</h1>
                        <p>
                            آسون کالا به عنوان یکی از قدیمی‌ترین فروشگاه های اینترنتی با بیش از یک دهه تجربه، با
                            پایبندی به سه اصل کلیدی، پرداخت در
                            محل، 7 روز ضمانت بازگشت کالا و تضمین اصل‌بودن کالا، موفق شده تا همگام با فروشگاه‌های
                            معتبر جهان، به بزرگ‌ترین فروشگاه
                            اینترنتی ایران تبدیل شود. به محض ورود به آسون کالا با یک سایت پر از کالا رو به رو
                            می‌شوید! هر آنچه که نیاز دارید و به
                            ذهن شما خطور می‌کند در اینجا پیدا خواهید کرد.
                        </p>

                        <br>

                        {!! $setting->google_map !!}

                    </div>
                    <div>
                        <a referrerpolicy="origin" target="_blank" href="https://trustseal.enamad.ir/?id=196328&amp;Code=kkVCLK3RSjb2ZfPeFy6z"><img referrerpolicy="origin" src="https://Trustseal.eNamad.ir/logo.aspx?id=196328&amp;Code=kkVCLK3RSjb2ZfPeFy6z" alt="" style="cursor:pointer" id="kkVCLK3RSjb2ZfPeFy6z"></a>
                        <img id = 'nbqejxlzesgtjzpefukzwlao' style = 'cursor:pointer' onclick = 'window.open("https://logo.samandehi.ir/Verify.aspx?id=210764&p=uiwkrfthobpdjyoegvkaaods", "Popup","toolbar=no, scrollbars=no, location=no, statusbar=no, menubar=no, resizable=0, width=450, height=630, top=30")' alt = 'logo-samandehi' src = 'https://logo.samandehi.ir/logo.aspx?id=210764&p=odrfnbpdlymayndtwlbqshwl' />

                    </div>
                </div>
            </div>
        </div>
        <div class="copyright">
            <div class="container">
                <p>
                    کلیه حقوق برای آسون کالا محفوظ می باشد.
                </p>
            </div>
        </div>
    </footer>
</div>

</body>
<!--   Core JS Files   -->
<script src="{{asset('template/js/core/jquery.3.2.1.min.js')}}" type="text/javascript"></script>
<script src="{{asset('template/js/core/popper.min.js')}}" type="text/javascript"></script>
<script src="{{asset('template/js/core/bootstrap.min.js')}}" type="text/javascript"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="{{asset('template/js/plugins/bootstrap-switch.js')}}"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="{{asset('template/js/plugins/nouislider.min.js')}}" type="text/javascript"></script>
<!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
<script src="{{asset('template/js/plugins/bootstrap-datepicker.js')}}" type="text/javascript"></script>
<!-- Share Library etc -->
<script src="{{asset('template/js/plugins/jquery.sharrre.js')}}" type="text/javascript"></script>
<!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
<script src="{{asset('template/js/now-ui-kit.js')}}" type="text/javascript"></script>
<!--  CountDown -->
<script src="{{asset('template/js/plugins/countdown.min.js')}}" type="text/javascript"></script>
<!--  Plugin for Sliders -->
<script src="{{asset('template/js/plugins/owl.carousel.min.js')}}" type="text/javascript"></script>
<!--  Jquery easing -->
<script src="{{asset('template/js/plugins/jquery.easing.1.3.min.js')}}" type="text/javascript"></script>
<!-- Main Js -->
<script src="{{asset('template/js/main.js')}}" type="text/javascript"></script>

<script>
    $('#EnglishDataPostal').on('keyup', function (event) {
        var arregex = /^[0-9]*$/;
        if (!arregex.test(event.key)) {
            $('#EnglishDataPostal').val("");
        }
    });
    $('#EnglishDataMobile').on('keyup', function (event) {
        var arregex = /^[0-9]*$/;
        if (!arregex.test(event.key)) {
            $('#EnglishDataMobile').val("");
        }
    });
</script>

</html>
