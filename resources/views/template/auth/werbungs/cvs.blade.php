@extends('template.app')


@section('content')




  <!-- content-->
  <div class="content">
  @include('template.auth.header-welcome')
                    <!--  section  -->
                    <section class="gray-bg main-dashboard-sec" id="sec1">
                        <div class="container">
                            @include('template.auth.menu')
                            <!-- dashboard content-->
                            <div class="col-md-9">
                            @include('template.alert.success')
                           
                             <!-- dashboard-list-box--> 
                                 <div class="dashboard-list-box  fl-wrap">
                                 @foreach ($cvs as $cv)
                                     
                                 <!-- dashboard-list -->    
                                 <div class="dashboard-list jobs fl-wrap" style="margin-bottom:20px;">
                                        <div class="dashboard-message">
                                           
                                          
                                            
                                            <div class="dashboard-message-text">
                                            <div class="option_button">
                                            @if($cv->Hasmedia('cv'))
                                                <a download class="translate_buttom btn btn-primary"  style="background: #4DB7FE !important;" target="_blank"  href="{{$cv->getFirstMediaUrl('cv')}}">{{__('cms.download')}}</a>
                                              @endif
                                            </div>
                                            <br>
                                                            @if($cv->selfClient->Hasmedia('images'))
                                                            <img  src="{{$cv->selfClient->getFirstMediaUrl('images')}}" alt="title" title="title" >
                                                               

                                                            
                                                                     @else
                                                                     <img  src="{{asset('template/images/user-icon-2.jpg')}}" alt="title" title="title" >
 
                                                              
 
                                                                @endif

                                               
                                                <h4>
                    
                                                </h4>
                                                <div class="geodir-category-location clearfix"><a href="#"> {{$cv->selfClient->full_name}} / {{$cv->selfClient->email}}</a></div>
                                            </div>
                                        </div>
                                    </div>
                                 
                                    <!-- dashboard-list end-->   
                                 @endforeach
                                     
                                                                           
                                </div>
                                <!-- dashboard-list-box end--> 
                           
                            </div>
                            <!-- dashboard content end-->
                        </div>
                    </section>
                    <!--  section  end-->
                    <div class="limit-box fl-wrap"></div>
                </div>
                <!--content end-->


@endsection


@section('heads')

@if(LaravelLocalization::getCurrentLocale()=="fa" || LaravelLocalization::getCurrentLocale()=="ar")

<link type="text/css" rel="stylesheet" href="{{asset('template/css/dashboard-style.css')}}">
@else

<link type="text/css" rel="stylesheet" href="{{asset('template/css/ltr-dashboard-style.css')}}">
@endif
@endsection



@section('scripts')


<script>

            function loadCity() {



                    $code=$("#postal_code").val();
                   
               
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type:'POST',
                    contentType:'application/json; charset=utf-8',
                    url:'/ajax/load/city/postalcode/'+$code,
                    data: { field1:$code} ,
                    beforeSend:function(){
                    
                },
                    success: function (response) {
                       if(response.data.status){

                           console.log(response.data);
                           $('#city').empty();
                           $('.nice-select .list').empty();
                           $.each(response.data.result, function(index, key) {
                         
                            $('#city').append("<option value='"+key.fields.plz_name+"-"+key.fields.krs_name+"'>"+key.fields.plz_name+"-"+key.fields.krs_name+"</option>");
                            });
                            $.each(response.data.result, function(index, key) {
                               
                                $('.nice-select .list').append("<li data-value='"+key.fields.plz_name+"-"+key.fields.krs_name+"' class='option'>"+key.fields.plz_name+"-"+key.fields.krs_name+"</li>");
                            });
                              

                              

                       }
                       else{
                        console.log('no');
                       }
                    },
                    error: function (xhr,ajaxOptions,thrownError) {
                    
                    console.log(xhr,ajaxOptions,thrownError);
                        // $("#result-ajax").empty()
                        //     .append("<span>"+msg+"</span>");
                    },
                    complete:function(){
                   
                }

                });

           }

</script>


@endsection




