@extends('template.app')


@section('content')




  <!-- content-->
  <div class="content">
  <input id="lang_form" type="text" value="{{LaravelLocalization::getCurrentLocale()}}" hidden>

  @include('template.auth.header-welcome')
                    <!--  section  -->
                    <section class="gray-bg main-dashboard-sec" id="sec1">
                        <div class="container">
                            @include('template.auth.menu')
                            <!-- dashboard content-->
                            <div class="col-md-9">
                             
                            <form action="{{route('client.update.question.werbung',['question'=>$item->token])}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    @method('PATCH')
                                  
                                <!-- profile-edit-container--> 
                                <div class="profile-edit-container fl-wrap block_box">

                                @include('template.alert.error')
                                <div style="margin-bottom:10px"></div>
                                    <div class="custom-form">

                                    <div class="row">
                                                <div class="col-sm-12">
                                                <label>{{__('cms.title')}} <i class="fal fa-user"></i></label>
                                                <input type="text" name="title" value="{{$item->title}}" class="{{$errors->has('title') ? 'error-input' : ''}}" placeholder="{{__('cms.title')}}" value="{{old('title') ? old('title') : $item->title}}" autocomplete="off"/>                                                
                                            </div>
                                           
                                           
                                          
                                            <div class="col-sm-12" style="margin-bottom:15px">
                                                <label>{{__('cms.text')}}  <i class="fal fa-text"></i></label>
                                                <textarea type="text" name="text" class="{{$errors->has('awnser') ? 'error-input' : ''}}" placeholder="{{__('cms.text')}}">{{$item->answer}}</textarea>                                                
                                            </div>

                               
                                           
</div>

</div>
</div>

 

<button type="submit" class="logout_btn color2-bg">{{__('cms.save')}} <i class="fas fa-sign-out"></i></button>


                                </form>     
                        </div>
                    </section>
                    <!--  section  end-->
                    <div class="limit-box fl-wrap"></div>
                </div>
                <!--content end-->


@endsection


@section('heads')

@if(LaravelLocalization::getCurrentLocale()=="fa" || LaravelLocalization::getCurrentLocale()=="ar")

<link type="text/css" rel="stylesheet" href="{{asset('template/css/dashboard-style.css')}}">
@else

<link type="text/css" rel="stylesheet" href="{{asset('template/css/ltr-dashboard-style.css')}}">
@endif
@endsection



@section('scripts')

@endsection




