


    <h5 style="margin-top: 50px">
        مشخصات بیمه گذار (حقیقی)
    </h5>
    
    <p>
        منظور از بیمه گذار فردی است که حق بیمه را پرداخت میکند.
    حداقل سن بیمه گزار 18 سال تمام می باشد.
    </p>
    <div class="row">
    
    
    <div class="col-lg-12">
    
    <form action="{{ route('get.vip.information') }}" method="POST" class="login100-form validate-form" enctype="multipart/form-data">
            
    @csrf
    
    <div class="row">
    
        <div class="col-lg-12 col-sm-12">
            <div class="form-group">
            
                <label>کد معرف (در صورت داشتن کد معرف از شخصی دیگر  میتوانید در این قسمت استفاده کنید وگدنه این قسمت الزامی نیست)</label>
            <input class="form-control" type="text" name="code_vip" style="{{ $errors->has('code') ? ' border:1px solid red' : '' }}" placeholder="    کد معرف" autocomplete="off">
            </div>
        </div>
    
    
    </div>
    
    
    
    
    <p>
        مشخصات  
    </p>
    <div class="row">
    
    
    
        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
               
                <label>نام و نام خانوادگی <span style="color:red">*</span>  </label>
            <input class="form-control" type="text" name="full_name_bime_gozar" style="{{ $errors->has('full_name_bime_shode') ? ' border:1px solid red' : '' }}" placeholder=" نام و نام خانوادگی" autocomplete="off">
            </div>
        </div>
    
        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
            
                <label>  نام پدر <span style="color:red">*</span>  </label>
            <input class="form-control" type="text" name="father_name_bime_gozar" style="{{ $errors->has('father_name_bime_shode') ? ' border:1px solid red' : '' }}" placeholder="نام پدر " autocomplete="off">
            </div>
        </div>
    
    
        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
            
                <label>  کد ملی <span style="color:red">*</span>  </label>
            <input class="form-control" type="text" name="code_meli_bime_gozar" style="{{ $errors->has('code_meli_bime_shode') ? ' border:1px solid red' : '' }}" placeholder=" کد ملی" autocomplete="off">
            </div>
        </div>
    
        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
            
                <label>   شماره شناسنامه <span style="color:red">*</span>   </label>
            <input class="form-control" type="text" name="shenasname_meli_bime_gozar" style="{{ $errors->has('shenasname_meli_bime_shode') ? ' border:1px solid red' : '' }}" placeholder=" شماره شناسنامه" autocomplete="off">
            </div>
        </div>
    
        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
            
                <label>   محل صدور  <span style="color:red">*</span>   </label>
            <input class="form-control" type="text" name="sodor_bime_gozar" style="{{ $errors->has('sodor_bime_shode') ? ' border:1px solid red' : '' }}" placeholder="  محل صدور" autocomplete="off">
            </div>
        </div>
    
        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
            
                <label>  محل تولد  <span style="color:red">*</span> </label>
            <input class="form-control" type="text" name="tavalod_bime_gozar" style="{{ $errors->has('tavalod_bime_shode') ? ' border:1px solid red' : '' }}" placeholder=" محل تولد" autocomplete="off">
            </div>
        </div>
    
        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
            
                <label>  تاریخ تولد   <span style="color:red">*</span> </label>
            <input class="form-control" type="text" name="tarikh_tavalod_bime_gozar" style="{{ $errors->has('tarikh_tavalod_bime_shode') ? ' border:1px solid red' : '' }}" placeholder="  تاریخ تولد  1365/05/12" autocomplete="off">
            </div>
        </div>
    
    
        
    
        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
                <label>جنسیت <span style="color:red">*</span></label>
                <select  name="jensiat_bime_gozar" class="form-control">
                        <option value="زن">زن</option>
                        <option value="مرد">مرد</option>
                </select>
            </div>
        </div>
    
    
        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
                <label> سطح تحصیلات <span style="color:red">*</span></label>
                <select name="tahsilat_bime_gozar" class="form-control">
                        <option value="زیر دیپلم">زیر دیپلم</option>
                        <option value="دیپلم">دیپلم</option>
                        <option value="لیسانس">لیسانس</option>
                        <option value="فوق لیسانس">فوق لیسانس</option>
                        <option value="دکترا">دکترا</option>
                </select>
            </div>
        </div>
    
    
    
        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
                <label> وضعیت تاهل <span style="color:red">*</span> </label>
                <select name="taahol_bime_gozar" class="form-control">
                        <option name="متاهل">متاهل </option>
                        <option name="مجرد">مجرد</option>
                        <option name="طلاق گرفته">طلاق گرفته</option>
                  
                </select>
            </div>
        </div>
        
    
      
    
        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
                <label>  وضعیت خدمت سربازی <span style="color:red">*</span> </label>
                <select name="vaziate_sarbazi_bime_gozar" class="form-control">
                        <option value="کارت پایان خدمت">کارت پایان خدمت </option>
                        <option value="معاف">معاف</option>
                        <option value="ندارم">ندارم </option>
                  
                </select>
            </div>
        </div>
    
    
        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
            
                <label>    عکس کارت پایان خدمت در صورت معافیت    </label>
            <input class="form-control" type="file" name="pic_karte_payan_khedmat_bime_gozar" >
            </div>
        </div>
    
        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
            
                <label>   عکس کارت  ملی <span style="color:red">*</span>    </label>
            <input class="form-control" type="file" name="pic_karte_meli_bime_gozar" >
            </div>
        </div>
    
        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
            
                <label>   عکس شناسنامه   <span style="color:red">*</span>   </label>
            <input class="form-control" type="file" name="pic_shenasname_bime_gozar" >
            </div>
        </div>
    
        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
            
                <label>   تلفن همراه  <span style="color:red">*</span>   </label>
            <input class="form-control" type="text" name="telephone_hamrah_bime_gozar" style="{{ $errors->has('telephone_hamrah_bime_shode') ? ' border:1px solid red' : '' }}" placeholder=" تلفن همراه " autocomplete="off">
            </div>
        </div>
    
    
        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
            
                <label>   ایمیل     </label>
            <input class="form-control" type="text" name="email_bime_gozar" style="{{ $errors->has('email_bime_shode') ? ' border:1px solid red' : '' }}" placeholder="  ایمیل " autocomplete="off">
            </div>
        </div>
    
    
        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
            
                <label>   تلفن منزل  <span style="color:red">*</span>   </label>
            <input class="form-control" type="text" name="phone_bime_gozar" style="{{ $errors->has('phone_bime_shode') ? ' border:1px solid red' : '' }}" placeholder=" تلفن منزل  " autocomplete="off">
            </div>
        </div>
    
        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
            
                <label>   کد پستی <span style="color:red">*</span>   </label>
            <input class="form-control" type="text" name="code_posti_bime_gozar" style="{{ $errors->has('code_posti_bime_shode') ? ' border:1px solid red' : '' }}" placeholder=" کد پستی  " autocomplete="off">
            </div>
        </div>
    
        <div class="col-lg-12 col-sm-12">
            <div class="form-group">
            
                <label>   نشانی محل کار <span style="color:red">*</span>    </label>
            <input class="form-control" type="text" name="address_work_bime_gozar" style="{{ $errors->has('address_work_bime_shode') ? ' border:1px solid red' : '' }}" placeholder=" نشانی محل کار  " autocomplete="off">
            </div>
        </div>
    
        <div class="col-lg-12 col-sm-12">
            <div class="form-group">
            
                <label>   نشانی محل سکونت   <span style="color:red">*</span>   </label>
            <input class="form-control" type="text" name="address_home_bime_gozar" style="{{ $errors->has('address_home_bime_shode') ? ' border:1px solid red' : '' }}" placeholder=" نشانی محل سکونت  " autocomplete="off">
            </div>
        </div>
    
        <div class="col-lg-12 col-sm-12">
            <div class="form-group">
            
                <label>   شغل <span style="color:red">*</span>    </label>
            <input class="form-control" type="text" name="job_bime_gozar" style="{{ $errors->has('job_bime_shode') ? ' border:1px solid red' : '' }}" placeholder=" شغل به صورت کامل توضیح داده شود  " autocomplete="off">
            </div>
        </div>
    
    
    
       
    
    
    
        
    </div> 


     
    
    
 





