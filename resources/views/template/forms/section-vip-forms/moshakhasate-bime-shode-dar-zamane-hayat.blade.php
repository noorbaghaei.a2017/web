<div style="border:1px solid #12b48b;padding:15px">


<h5>
    مشخصات استفاده کنندگان از سرمایه بیمه نامه
</h5>

<p>

    استفاده کنندگان از سرمایه بیمه نامه در صورت حیات و فوت بیمه شده در مدت بیمه نامه ( تکمیل کل اطلاعات ردیف الزامی است)
</p>

<p>

    در صورت حیات (زنده بودن) بیمه شده به چه اشخاصی برسد؟
</p>


<p>
    <strong>تذکر : </strong>
    در صورت حیات بیمه شده در انقضای مدت بیمه،چنانچه استفاده کننده مشخص نشده باشد سرمایه بیمه به بیمه گذار پرداخت خواهد شد.

</p>

<h6>

    مشخصات نفر اول
</h6>



<div class="row">


    <div class="col-lg-4 col-sm-12">
        <div class="form-group">
        
            <label>نام و نام خانوادگی <span style="color:red">*</span> </label>
        <input class="form-control" type="text" name="full_name_bime_shode_hayat" style="{{ $errors->has('full_name_bime_shode_hayat') ? ' border:1px solid red' : '' }}" placeholder=" نام و نام خانوادگی" autocomplete="off">
        </div>
    </div>

    <div class="col-lg-4 col-sm-12">
        <div class="form-group">
        
            <label>  نام پدر <span style="color:red">*</span> </label>
        <input class="form-control" type="text" name="father_name_bime_shode_hayat" style="{{ $errors->has('father_name_bime_shode_hayat') ? ' border:1px solid red' : '' }}" placeholder="نام پدر " autocomplete="off">
        </div>
    </div>


    <div class="col-lg-4 col-sm-12">
        <div class="form-group">
        
            <label>  کد ملی <span style="color:red">*</span> </label>
        <input class="form-control" type="text" name="code_meli_bime_shode_hayat" style="{{ $errors->has('code_meli_bime_shode_hayat') ? ' border:1px solid red' : '' }}" placeholder=" کد ملی" autocomplete="off">
        </div>
    </div>

    <div class="col-lg-4 col-sm-12">
        <div class="form-group">
        
            <label>  شماره شناسنامه <span style="color:red">*</span>  </label>
        <input class="form-control" type="text" name="shenasname_bime_shode_hayat" style="{{ $errors->has('shenasname_bime_shode_hayat') ? ' border:1px solid red' : '' }}" placeholder=" شماره شناسنامه" autocomplete="off">
        </div>
    </div>


    <div class="col-lg-4 col-sm-12">
        <div class="form-group">
        
            <label>  تاریخ تولد  <span style="color:red">*</span> </label>
        <input class="form-control" type="text" name="tarikh_tavalod_bime_shode_hayat" style="{{ $errors->has('tarikh_tavalod_bime_shode_hayat') ? ' border:1px solid red' : '' }}" placeholder="  تاریخ تولد  1365/05/12" autocomplete="off">
        </div>
    </div>

   

    <div class="col-lg-12 col-sm-12">
        <div class="form-group">
            <label>نسبت بیمه شده <span style="color:red">*</span></label>
            <select name="nesbat_bime_shode_hayat" class="form-control">
                    <option value="خودم">خودم</option>
                    <option value="غیر خودم (همسر، فرزند)">غیر خودم (همسر، فرزند)</option>
            </select>
        </div>
    </div>



    <div class="col-lg-12 col-sm-12">
        <div class="form-group">
        
            <label>   درصد استفاده از سرمایه بیمه عمر در صورت حیات <span style="color:red">*</span> </label>
        <input class="form-control" type="text" name="darsad_bime_shode_hayat" style="{{ $errors->has('darsad_bime_shode_hayat') ? ' border:1px solid red' : '' }}" placeholder=" درصد " autocomplete="off">
        </div>
    </div>
    



</div>



<h5>

    در صورتی که استفاده کنندگان بیش از یک نفر هستند اطلاعات خواسته شده زیر را باید یادداشت کنید
</h5>



<div class="row">

    <div class="col-lg-12 col-sm-12">
        <div class="form-group">
        
            <label>اطلاعات بیشتر افراد بیمه شده به صورت کامل مشابه بالا  </label>
        <input class="form-control" type="text" name="info_user_bime_shode_hayat" style="{{ $errors->has('info_user_bime_shode_hayat') ? ' border:1px solid red' : '' }}" placeholder="   نام و نام خانوادگی ، محل تولد ، تاریخ تولد ، نام پدر ، تاریخ تولد شماره شناسنامه و مد ملی" autocomplete="off">
        </div>
    </div>


</div>

</div>
<br>
