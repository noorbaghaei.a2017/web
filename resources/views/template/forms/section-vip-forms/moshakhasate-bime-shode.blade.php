<div style="border:1px solid #12b48b;padding:15px">

    <h6>
        در صورتیکه بیمه گذار و بیمه شده یک شخص باشد، تکمیل این قسمت ضرورتی ندارد
</h6>
    <div class="row">

       
        <br>

        <div class="col-lg-12 col-sm-12">
            <div class="form-group">
                <label>نسبت بیمه شده</label>
                <select name="nesbate_bime_bime_shode" class="form-control">
                        <option value="خودم">خودم</option>
                        <option value="غیر خودم (همسر، فرزند)">غیر خودم (همسر، فرزند)</option>
                </select>
            </div>
        </div>

    </div>

    <p>
        مشخصات بیمه شده
    </p>
    <div class="row">



        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
            
                <label>نام و نام خانوادگی  </label>
            <input class="form-control" type="text" name="full_name_bime_shode" style="{{ $errors->has('full_name_bime_shode') ? ' border:1px solid red' : '' }}" placeholder=" نام و نام خانوادگی" autocomplete="off">
            </div>
        </div>

        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
            
                <label>  نام پدر  </label>
            <input class="form-control" type="text" name="father_name_bime_shode" style="{{ $errors->has('father_name_bime_shode') ? ' border:1px solid red' : '' }}" placeholder="نام پدر " autocomplete="off">
            </div>
        </div>


        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
            
                <label>  کد ملی  </label>
            <input class="form-control" type="text" name="code_meli_bime_shode" style="{{ $errors->has('code_meli_bime_shode') ? ' border:1px solid red' : '' }}" placeholder=" کد ملی" autocomplete="off">
            </div>
        </div>

        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
            
                <label>  شماره شناسنامه   </label>
            <input class="form-control" type="text" name="shenasname_bime_shode" style="{{ $errors->has('shenasname_bime_shode') ? ' border:1px solid red' : '' }}" placeholder=" شماره شناسنامه" autocomplete="off">
            </div>
        </div>

        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
            
                <label>   محل صدور    </label>
            <input class="form-control" type="text" name="sodor_bime_shode" style="{{ $errors->has('sodor_bime_shode') ? ' border:1px solid red' : '' }}" placeholder="  محل صدور" autocomplete="off">
            </div>
        </div>

        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
            
                <label>  محل تولد  </label>
            <input class="form-control" type="text" name="mahale_tavalod_bime_shode" style="{{ $errors->has('mahale_tavalod_bime_shode') ? ' border:1px solid red' : '' }}" placeholder=" محل تولد" autocomplete="off">
            </div>
        </div>

        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
            
                <label>  تاریخ تولد   </label>
            <input class="form-control" type="text" name="tarikh_tavalod_bime_shode" style="{{ $errors->has('tarikh_tavalod_bime_shode') ? ' border:1px solid red' : '' }}" placeholder="  تاریخ تولد  1365/05/12" autocomplete="off">
            </div>
        </div>


        

        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
                <label>جنسیت</label>
                <select name="jensiat_bime_shode" class="form-control">
                        <option value="زن">زن</option>
                        <option value="مرد">مرد</option>
                </select>
            </div>
        </div>


        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
                <label> سطح تحصیلات</label>
                <select name="sathe_tahsilat_bime_shode" class="form-control">
                        <option value="زیر دیپلم">زیر دیپلم</option>
                        <option value="دیپلم">دیپلم</option>
                        <option value="لیسانس">لیسانس</option>
                        <option value="فوق لیسانس">فوق لیسانس</option>
                        <option value="دکترا">دکترا</option>
                </select>
            </div>
        </div>

    

        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
                <label> وضعیت تاهل </label>
                <select name="vaziate_taahol_bime_shode" class="form-control">
                        <option value="متاهل">متاهل </option>
                        <option value="مجرد">مجرد</option>
                        <option value="طلاق گرفته">طلاق گرفته</option>
                  
                </select>
            </div>
        </div>
        

      

        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
                <label>  وضعیت خدمت سربازی </label>
                <select name="vaziate_khedmat_bime_shode" class="form-control">
                        <option value="کارت پایان خدمت">کارت پایان خدمت </option>
                        <option value="معاف">معاف</option>
                        <option value="ندارم">ندارم </option>
                  
                </select>
            </div>
        </div>


        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
            
                <label>   تلفن همراه    </label>
            <input class="form-control" type="text" name="telephone_hamrah_bime_shode" style="{{ $errors->has('telephone_hamrah_bime_shode') ? ' border:1px solid red' : '' }}" placeholder=" تلفن همراه " autocomplete="off">
            </div>
        </div>


        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
            
                <label>   ایمیل     </label>
            <input class="form-control" type="text" name="email_bime_shode" style="{{ $errors->has('email_bime_shode') ? ' border:1px solid red' : '' }}" placeholder="  ایمیل " autocomplete="off">
            </div>
        </div>


        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
            
                <label>   تلفن منزل     </label>
            <input class="form-control" type="text" name="phone_bime_shode" style="{{ $errors->has('phone_bime_shode') ? ' border:1px solid red' : '' }}" placeholder=" تلفن منزل  " autocomplete="off">
            </div>
        </div>

        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
            
                <label>   کد پستی   </label>
            <input class="form-control" type="text" name="code_posti_bime_shode" style="{{ $errors->has('code_posti_bime_shode') ? ' border:1px solid red' : '' }}" placeholder=" کد پستی  " autocomplete="off">
            </div>
        </div>

        <div class="col-lg-12 col-sm-12">
            <div class="form-group">
            
                <label>   نشانی محل کار     </label>
            <input class="form-control" type="text" name="address_work_bime_shode" style="{{ $errors->has('address_work_bime_shode') ? ' border:1px solid red' : '' }}" placeholder=" نشانی محل کار  " autocomplete="off">
            </div>
        </div>

        <div class="col-lg-12 col-sm-12">
            <div class="form-group">
            
                <label>   نشانی محل سکونت     </label>
            <input class="form-control" type="text" name="address_home_bime_shode" style="{{ $errors->has('address_home_bime_shode') ? ' border:1px solid red' : '' }}" placeholder=" نشانی محل سکونت  " autocomplete="off">
            </div>
        </div>

        <div class="col-lg-12 col-sm-12">
            <div class="form-group">
            
                <label>   شغل     </label>
            <input class="form-control" type="text" name="job_bime_shode" style="{{ $errors->has('job_bime_shode') ? ' border:1px solid red' : '' }}" placeholder=" شغل به صورت کامل توضیح داده شود  " autocomplete="off">
            </div>
        </div>



       



        
    </div>  
    




</div>

<br>