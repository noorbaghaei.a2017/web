<div style="border:1px solid #12b48b;padding:15px">

<h5>


    مشخصات سوابق بیمه عمر 
</h5>

<p>

    آیا در حال حاضر بیمه عمر دیگری دارید؟

</p>



<div class="row">

    <div class="col-lg-12 col-sm-12">
        <div class="form-group">
        
            <label>  نام و نوع بیمه <span style="color:red">*</span>  </label>
        <input class="form-control" type="text" name="name_type_bime_shode_detail" style="{{ $errors->has('name_type_bime_shode_detail') ? ' border:1px solid red' : '' }}" placeholder="نام و نوع بیمه  " autocomplete="off">
        </div>
    </div>

</div>

<p>

    بیمه عمر و سرمایه گذاری، بیمه عمر، بیمه حوادث انفرادی 

</p>


<div class="row">

    <div class="col-lg-4 col-sm-12">
        <div class="form-group">
        
            <label>   سرمایه فوت بیمه قبلی  <span style="color:red">*</span>   </label>
        <input class="form-control" type="text" name="sarmaye_bime_shode_detail" style="{{ $errors->has('sarmaye_bime_shode_detail') ? ' border:1px solid red' : '' }}" placeholder="سرمایه فوت بیمه    " autocomplete="off">
        </div>
    </div>

    <div class="col-lg-4 col-sm-12">
        <div class="form-group">
        
            <label>   تاریخ شروع   <span style="color:red">*</span>  </label>
        <input class="form-control" type="text" name="tarikh_shoro_bime_shode_detail" style="{{ $errors->has('tarikh_shoro_bime_shode_detail') ? ' border:1px solid red' : '' }}" placeholder=" تاریخ شروع      " autocomplete="off">
        </div>
    </div>


    <div class="col-lg-4 col-sm-12">
        <div class="form-group">
        
            <label>   مدت قرار داد    <span style="color:red">*</span>   </label>
        <input class="form-control" type="text" name="modate_bime_shode_detail" style="{{ $errors->has('modate_bime_shode_detail') ? ' border:1px solid red' : '' }}" placeholder="مدت قرار داد        " autocomplete="off">
        </div>
    </div>

    <div class="col-lg-4 col-sm-12">
        <div class="form-group">
        
            <label>   نام شرکت بیمه    <span style="color:red">*</span>   </label>
        <input class="form-control" type="text" name="name_sherkat_bime_shode_detail" style="{{ $errors->has('name_sherkat_bime_shode_detail') ? ' border:1px solid red' : '' }}" placeholder="  نام شرکت بیمه         " autocomplete="off">
        </div>
    </div>

    <div class="col-lg-4 col-sm-12">
        <div class="form-group">
        
            <label>   نام واحد صدور   <span style="color:red">*</span>  </label>
        <input class="form-control" type="text" name="name_vahede_sodor_bime_shode_detail" style="{{ $errors->has('name_vahede_sodor_bime_shode_detail') ? ' border:1px solid red' : '' }}" placeholder="      نام واحد صدور           " autocomplete="off">
        </div>
    </div>

</div>


</div>
<br>

<div style="border:1px solid #12b48b;padding:15px">

<h5>

    مشخصات بیمه سپرده زندگی
</h5>

<div class="row">

    <div class="col-lg-4 col-sm-12">
        <div class="form-group">
            <label>نحوه پرداخت سرمایه <span style="color:red">*</span></label>
            <select name="name_pardakht_bime_shode_detail" class="form-control">
                    <option value="پرداخت سرمایه یک جا">پرداخت سرمایه یک جا </option>
                  
            </select>
        </div>
    </div>

    <div class="col-lg-4 col-sm-12">
        <div class="form-group">
        
            <label>   مدت بیمه      <span style="color:red">*</span> </label>
        <input class="form-control" type="text" name="modat_bime_shode_detail" style="{{ $errors->has('code') ? ' border:1px solid red' : '' }}" placeholder="         مدت بیمه            " autocomplete="off">
        </div>
    </div>
    



</div>





<br>
<h6>

    حداکثر مدت زمان انتخابی ۳۰ سال و حداکثر سن تا 67 سالگی است. مگر اینکه قوانین شرکت بیمه تغییر کند.
</h6>

<br>

<br>

<h6>

    سرمایه اولیه فوت (تعهد بیمه گر): 800،000،000 میلیون ریال 
</h6>
<br>


<div class="row">

    <div class="col-lg-12 col-sm-12">
        <div class="form-group">
            <label>پوشش فوت بر اثر حادثه <span style="color:red">*</span></label>
            <select name="poshesh_hadese_bime_shode_detail" class="form-control">
                    <option value="یک برابر">یک برابر </option>
                    <option value="دو برابر">دو برابر </option>
                    <option value="سه برابر">سه برابر </option>
                    <option value="چهار برابر">چهار برابر </option>
                  
                  
            </select>
        </div>
    </div>
</div>

    

        <br>
<h6>

    حداکثر سرمایه امراض خاص 240،000،000 ریال     
   </h6>

   <br>



<div class="row">

    <div class="col-lg-12 col-sm-12">
        <div class="form-group">
            <label>آیا پوشش امراض خاص (سکته قلبی, سکته مغزی، جراحی عروق (کرونر), سرطان و پیوند اعضای بدن) را می خواهید</label>
            <select name="poshesh_amraze_khas_bime_shode_detail" class="form-control">
                    <option value="بله"> بله </option>
                    <option value="خیر">خیر </option>
                  
            </select>
        </div>
    </div>

  
</div>

<br>
<h6>

    نرخ سرمایه امراض خاص 5%
</h6>

<br>
<h6>

    افزایش سالیانه سرمایه بیمه عمر5% است. بالای 60 سال صفر می باشد.
</h6>
<br>

<div class="row">

    <div class="col-lg-12 col-sm-12">
        <div class="form-group">
            <label>آیا معافیت از پرداخت حق بیمه به علت از کار افتادگی را می خواهید؟</label>
            <select name="moafiat_pardakht_bime_shode_detail" class="form-control">
                    <option value="بله">بله </option>
                    <option value="خیر">خیر </option>
                  
            </select>
        </div>
    </div>
</div>

   

        <br>
<h6>

    سرمایه بیمه تکمیلی از کار افتادگی 600،000،000 ریال می باشد.
</h6>

<br>









</div>

<br>