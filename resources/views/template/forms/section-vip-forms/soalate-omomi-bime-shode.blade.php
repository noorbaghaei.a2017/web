<div style="border:1px solid #12b48b;padding:15px">

    <h6>
    
        سوالات عمومی پزشکی بیمه شده

    </h6>


   

    <h6>


        بیماریهای دوران بزرگسالی در صورت ابتلا به هر نوع بیماری بیان نمایید

    </h6>

    <h6>

        بیماری های اعصاب و روان با ذکر نوع و مدت ابتلا و وضعیت بیمه سابقه‌ای از تصادفات و جراحات با ذکر نوع جراحات و زمان سابقه اعمال جراحی با ذکر علت و زمان عمل سابقه بستری در بیمارستان با ذکر علت و زمان بستری
    </h6>

    <div class="row">


        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
                <label>ابتلا به افسردگی  *   </label>
                <select name="afsordegi_pezeshki" class="form-control">
                        <option value="بله">بله </option>
                        <option value="خیر">خیر </option>
                      
                </select>
            </div>
        </div>

       

        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
                <label> * انواع سرطان بدخیم   </label>
                <select name="saratan_pezeshki" class="form-control">
                        <option value="بله">بله </option>
                        <option value="خیر">خیر </option>
                       
                      
                </select>
            </div>
        </div>


        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
                <label> سابقه شیمی درمانی یا رادیوتراپی (برق گذاشتن)*   </label>
                <select name="sabeghe_shimi_pezeshki" class="form-control">
                        <option value="بله">بله </option>
                        <option value="خیر">خیر </option>
                      
                </select>
            </div>
        </div>

        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
                <label> وضعیت سلامت جاری: سالم*   </label>
                <select name="vaziate_salamat_pezeshki" class="form-control">
                        <option value="بله">بله </option>
                        <option value="خیر">خیر </option>
                      
                </select>
            </div>
        </div>

        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
                <label> آزمایشات غربالگری   </label>
                <select name="azmayeshat_gharbalgari_pezeshki[]" class="form-control multi" multiple="multiple">
                        <option value="آزمایشات کامل خون">آزمایشات کامل خون </option>
                        <option value="آزمایش کامل ادرار">آزمایش کامل ادرار  </option>
                        <option value="آزمایش تست سل">آزمایش تست سل </option>
                        <option value="آزمایش پاپ اسمیر">آزمایش پاپ اسمیر </option>
                        <option value="آزمایش ماموگرام">آزمایش ماموگرام  </option>
                        <option value="آزمایش خون مخفی مدفوع">آزمایش خون مخفی مدفوع   </option>
                        <option value="میزان کلسترول خون">میزان کلسترول خون   </option>
                        <option value="هیچکدام">هیچکدام </option>

                      
                </select>
            </div>
        </div>


        <div class="col-lg-12 col-sm-12">
            <div class="form-group">
            
                <label>توضیحات مربوط به آزمایشات خود اعم از تاریخ و نتیجه را بنویسید.  </label>
            <input class="form-control" type="text" name="azmayeshat_tozihat_pezeshki" style="{{ $errors->has('azmayeshat_tozihat_pezeshki') ? ' border:1px solid red' : '' }}" placeholder=" توضیحات" autocomplete="off">
            </div>
        </div>



    </div>   

    <h5>

        استعمال دخانیات

    </h5>


    <div class="row">

        <div class="col-lg-12 col-sm-12">
            <div class="form-group">
            
                <label>دخانیات به هرشکل استنشاقی، جویدنی، سیگار و... و همچنین میزان استفاده روزانه و تعداد سال استفاده خود را بنویسید</label>
            <input class="form-control" type="text" name="mashrobat_tozihat_pezeshki" style="{{ $errors->has('mashrobat_tozihat_pezeshki') ? ' border:1px solid red' : '' }}" placeholder=" توضیحات" autocomplete="off">
            </div>
        </div>

    </div>





    <h5>

        مواد مخدر

    </h5>


    <div class="row">

        <div class="col-lg-12 col-sm-12">
            <div class="form-group">
            
                <label>حشیش کراک تریاک و مشتقات آن داروهای شبه مخدر و مخدر مانند ترامادول و مرفین میزان استفاده روزانه و تعداد سال استفاده خود را بنویسید.</label>
            <input class="form-control" type="text" name="dokhaniyat_tozihat_pezeshki" style="{{ $errors->has('dokhaniyat_tozihat_pezeshki') ? ' border:1px solid red' : '' }}" placeholder=" توضیحات" autocomplete="off">
            </div>
        </div>

    </div>





    <h5>

        تاریخچه فامیلی

    </h5>


    <div class="row">

        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
                <label> آیا پدر شما در قید حیات است؟*  </label>
                <select name="father_hayat_pezeshki" class="form-control">
                        <option value="بله">بله </option>
                        <option value="خیر">خیر </option>
                      
                </select>
            </div>
        </div>

        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
                <label> آیا مادر شما در حیات است؟* </label>
                <select name="mother_hayat_pezeshki" class="form-control">
                    <option value="بله">بله </option>
                    <option value="خیر">خیر </option>
                      
                </select>
            </div>
        </div>


      

        <div class="col-lg-12 col-sm-12">
            <div class="form-group">
            
                <label>سن فعلی پدر در صورت حیات و در صورت فوت، سن و علت دقیق فوت نوشته شود*  </label>
            <input class="form-control" type="text" name="father_age_pezeshki" style="{{ $errors->has('father_age_pezeshki') ? ' border:1px solid red' : '' }}" placeholder=" توضیحات" autocomplete="off">
            </div>
        </div>


        <div class="col-lg-12 col-sm-12">
            <div class="form-group">
            
                <label>سن مادر در صورت حیات و در صورت فوت، حتما سن و علت دقیق فوت نوشته شود*</label>
            <input class="form-control" type="text" name="mother_age_pezeshki" style="{{ $errors->has('mother_age_pezeshki') ? ' border:1px solid red' : '' }}" placeholder=" توضیحات" autocomplete="off">
            </div>
        </div>


        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
                <label>سابقه وجود بیماریهای خاص در خانواده: </label>
                <select name="sabeghe_bimari_pezeshki[]" class="form-control multi" multiple="multiple">
                     
                        <option value="بالا بودن فشار خون ">بالا بودن فشار خون </option>
                        <option value="بیماری کلیوی"> بیماری کلیوی  </option>
                        <option value="سردردها"> سردردها   </option>
                        <option value="انواع سرطان"> انواع سرطان   </option>
                        <option value=" تشنج (صرع)"> تشنج (صرع)   </option>
                        <option value="انواع بیماری قلبی "> انواع بیماری قلبی   </option>
                        <option value="سل"> سل  </option>
                        <option value="تورم مفاصل"> تورم مفاصل  </option>
                        <option value="عقب ماندگی ذهنی">  عقب ماندگی ذهنی  </option>
                        <option value="سکته قلبی">  سکته قلبی     </option>
                        <option value="انواع کم خونی">  انواع کم خونی      </option>
                        <option value="دیابت">  دیابت        </option>
                        <option value="هیچکدام">هیچکدام </option>

                </select>
            </div>
        </div>


        <div class="col-lg-12 col-sm-12">
            <div class="form-group">
            
                <label>در صورت داشتن سابقه بیماری های خاص دیگر، درقسمت زیر بیماری خانوادگی خود را یادداشت کنید:</label>
            <input class="form-control" type="text" name="sabeghe_bimari_khas_pezeshki" style="{{ $errors->has('sabeghe_bimari_khas_pezeshki') ? ' border:1px solid red' : '' }}" placeholder=" توضیحات" autocomplete="off">
            </div>
        </div>
    </div>

        <h6>

            وضعیت عمومی
        </h6>

        <div class="row">

        <div class="col-lg-12 col-sm-12">
            <div class="form-group">
            
                <label>قد دقیق(سانتی متر)* </label>
            <input class="form-control" type="text" name="ghad_pezeshki" style="{{ $errors->has('ghad_pezeshki') ? ' border:1px solid red' : '' }}" placeholder=" توضیحات" autocomplete="off">
            </div>
        </div>

        <div class="col-lg-12 col-sm-12">
            <div class="form-group">
            
                <label>وزن دقیق(کیلوگرم)*</label>
            <input class="form-control" type="text" name="vazn_pezeshki" style="{{ $errors->has('vazn_pezeshki') ? ' border:1px solid red' : '' }}" placeholder=" توضیحات" autocomplete="off">
            </div>
        </div>

        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
                <label>آیا اخیراً تغییر وزن داشته‌اید؟* </label>
                <select name="taghir_vazn_pezeshki" class="form-control">
                        <option value="بله">بله  </option>
                        <option value="کاهش">کاهش </option>
                        <option value="افزایش"> افزایش </option>
                        <option value="خیر"> خیر   </option>
                       

                </select>
            </div>
        </div>

       

        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
                <label>در صورت داشتن هرگونه مشکل پوستی موارد زیر را تیک بزنید:</label>
                <select name="moshkele_posti_pezeshki[]" class="form-control multi" multiple="multiple">
                      
                        <option value="هر گونه تغییر رنگ">هر گونه تغییر رنگ  </option>
                        <option value="توده پوستی"> توده پوستی </option>
                        <option value="خارش"> خارش   </option>
                        <option value="خشکی پوست"> خشکی پوست    </option>
                        <option value="کهیر"> کهیر   </option>
                        <option value="تغییرات مو و ناخن"> تغییرات مو و ناخن   </option>
                        <option value="هیچکدام">هیچکدام </option>
                       

                </select>
            </div>
        </div>

        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
                <label>آیا ضعف عمومی و احساس خستگی مداوم دارید؟*</label>
                <select name="ehsas_khastegi_pezeshki" class="form-control">
                        <option value="بله">بله   </option>
                        <option value="خیر">خیر  </option>
                       

                </select>
            </div>
        </div>


        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
                <label>در صورت احساس مشکلات مربوط به سر موارد زیر را تیک بزنید:</label>
                <select name="moshkelate_sar_pezeshki[]" class="form-control multi" multiple="multiple">
                      
                        <option value="سردرد">سردرد  </option>
                        <option value="ضربه به سر">ضربه به سر   </option>
                        <option value="هیچکدام">هیچکدام </option>
                       

                </select>
            </div>
        </div>


        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
                <label>آیا طی یک سال گذشته به چشم‌پزشک مراجعه داشته اید؟*</label>
                <select name="morajee_be_cheshmpezeshki_pezeshki" class="form-control">
                        <option value="بله">بله    </option>
                        <option value="خیر">خیر  </option>
                      
                       

                </select>
            </div>
        </div>


        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
                <label>استفاده موارد خاص برای چشم </label>
                <select name="mavarede_khas_cheshm_pezeshki[]" class="form-control multi" multiple="multiple">
                        <option value="هیچکدام">هیچکدام    </option>
                        <option value="عینک یا لنز">عینک یا لنز  </option>
                        <option value="درد">درد  </option>
                        <option value="سرخی">سرخی  </option>
                        <option value="ترشح زیاد اشک">ترشح زیاد اشک  </option>
                        <option value="مگس پران"> مگس پران  </option>
                        <option value="ناخنک">ناخنک  </option>
                        <option value="آب مروارید">آب مروارید  </option>
                        <option value="آب سیاه">  آب سیاه     </option>
                        <option value="هیچکدام">هیچکدام </option>
                      
                </select>
            </div>
        </div>

        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
                <label>آیا در ۶ ماه گذشته به دندانپزشک مراجعه داشته اید؟*</label>
                <select name="morajee_be_dandanpezeshki_pezeshki" class="form-control">
                        <option value="بله">بله    </option>
                        <option value="خیر">خیر  </option>
                      

                </select>
            </div>
        </div>

        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
                <label>در صورت داشتن مشکلات زیر گزینه موردنظر را تیک بزنید:</label>
                <select name="moshkelate_digar_pezeshki[]" class="form-control multi" multiple="multiple">
                        <option value="خونریزی لثه">خونریزی لثه     </option>
                        <option value="خشونت صدا ">خشونت صدا   </option>
                        <option value="زخم زبان">زخم زبان    </option>
                        <option value="خشکی دهان">خشکی دهان     </option>
                        <option value="زخم های مکرر حلق و دهان">زخم های مکرر حلق و دهان    </option>
                        <option value="هیچکدام">هیچکدام </option>
                       

                </select>
            </div>
        </div>


        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
                <label>در صورت داشتن مشکلات زیر (مربوط به گردن) گزینه مورد نظر را تیک بزنید:</label>
                <select name="moshkelate_gardan_pezeshki[]" class="form-control multi" multiple="multiple">
                        <option value="توده">توده     </option>
                        <option value="گواتر">گواتر    </option>
                        <option value="درد">درد     </option>
                        <option value="سفتی">سفتی      </option>
                        <option value="هیچکدام">هیچکدام </option>
                     
                       

                </select>
            </div>
        </div>


        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
                <label>در صورت داشتن مشکلات تنفسی گزینه های زیر را انتخاب نمایید:</label>
                <select name="moshkelate_tanafosi_pezeshki[]" class="form-control multi" multiple="multiple">
                       
                        <option value="سرفه">سرفه    </option>
                        <option value="خلط خونی">خلط خونی      </option>
                        <option value="خس خس سینه ">خس خس سینه       </option>
                        <option value="">سوت کشیدن سینه     </option>
                        <option value="سوت کشیدن سینه ">سرفه خونی       </option>
                        <option value="تنگی نفس ">تنگی نفس         </option>
                        <option value="هیچکدام">هیچکدام </option>


                </select>
            </div>
        </div>

        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
                <label>آیا در ۶ ماه گذشته عکس سینه ( C.X.R) گرفته‌اید؟*</label>
                <select name="moshkelate_cxr_pezeshki" class="form-control">
                        <option value="بله">بله      </option>
                        <option value="خیر">خیر    </option>
                      
                </select>
            </div>
        </div>

        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
                <label>اگر مشکلات زیر (مربوط به دستگاه قلبی و عروقی) را دارید لطفا تیک بزنید:</label>
                <select name="moshkelate_ghalbi_oroghi_pezeshki[]" class="form-control multi" multiple="multiple">
                      
                        <option value="مشکلات قلبی">مشکلات قلبی    </option>
                        <option value="افزایش فشار خون">افزایش فشار خون      </option>
                        <option value="تب روماتیسمی ">تب روماتیسمی      </option>
                        <option value="درد یا احساس ناراحتی در سینه">درد یا احساس ناراحتی در سینه      </option>
                        <option value="تپش قلب  ">تپش قلب      </option>
                        <option value="تنگی نفس">تنگی نفس      </option>
                        <option value="تنگی نفس در خواب یا در وضعیت های متفاوت"> تنگی نفس در خواب یا در وضعیت های متفاوت     </option>
                        <option value="ورم اندام تحتانی">ورم اندام تحتانی       </option>
                        <option value="هیچکدام">هیچکدام </option>

                </select>
            </div>
        </div>


        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
                <label>آیا در ۶ ماه گذشته نوار قلب گرفته‌اید؟*</label>
                <select name="moshkelate_navar_ghlabi_pezeshki" class="form-control">
                        <option value="بله">بله      </option>
                        <option value="خیر">خیر    </option>
                      
                </select>
            </div>
        </div>


        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
                <label>اگر مشکلات زیر (مربوط به دستگاه گوارشی) را دارید گزینه های زیر را تیک بزنید:</label>
                <select name="moshkelate_dastgahe_govaresh_pezeshki[]" class="form-control multi" multiple="multiple">
                     
                        <option value="مشکلات بلع">مشکلات بلع    </option>
                        <option value="سوزش سر دل"> سوزش سر دل    </option>
                        <option value="تهوع یا استفراغ">تهوع یا استفراغ     </option>
                        <option value="پر اشتهایی "> پر اشتهایی     </option>
                        <option value="کم اشتهایی">کم اشتهایی      </option>
                        <option value="برگشت غذا به دهان  ">برگشت غذا به دهان     </option>
                        <option value="استفراغ خونی">استفراغ خونی      </option>
                        <option value="عدم هضم غذا">عدم هضم غذا      </option>
                        <option value="تغییرات اجابت مزاج">تغییرات اجابت مزاج     </option>
                        <option value="خونریزی از مدفوع">خونریزی از مدفوع     </option>
                        <option value="دردهای مبهم شکم  ">دردهای مبهم شکم     </option>
                        <option value="عدم تحمل به غذای خاص">عدم تحمل به غذای خاص    </option>
                        <option value="یرقان یا زردی  ">یرقان یا زردی     </option>
                        <option value="مشکلات کبدی صفراوی">مشکلات کبدی صفراوی      </option>
                        <option value="هپاتیت ها ">هپاتیت ها         </option>
                        <option value="اسهال مزمن">اسهال مزمن     </option>
                        <option value="هیچکدام">هیچکدام </option>
                      
                </select>
            </div>
        </div>


        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
                <label>اگر مشکلات زیر (مربوط به دستگاه اسکلتی عضلانی) را دارید گزینه های زیر را تیک بزنید:</label>
                <select name="moshkelate_dastgahe_azolani_pezeshki[]" class="form-control multi" multiple="multiple">
                       
                        <option value="درد در عضله یا مفصل ">درد در عضله یا مفصل   </option>
                        <option value="خشکی یا تورم مفصل "> خشکی یا تورم مفصل     </option>
                        <option value="کمر درد">کمر درد     </option>
                        <option value="سابقه انواع دیسک ستون فقرات"> سابقه انواع دیسک ستون فقرات      </option>
                        <option value="هیچکدام">هیچکدام </option>
                        
                      
                </select>
            </div>
        </div>


        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
                <label>اگر مشکلات زیر (مربوط به دستگاه اعصاب مرکزی) را دارید گزینه های زیر را تیک بزنید:</label>
                <select name="moshkelate_dastgahe_asab_markazi_pezeshki[]" class="form-control multi" multiple="multiple">
                       
                        <option value="کاهش موقت هوشیاری">کاهش موقت هوشیاری    </option>
                        <option value="تشنج"> تشنج    </option>
                        <option value="انتخاب ضعف و فلج موقت اندام سوم ">انتخاب ضعف و فلج موقت اندام سوم       </option>
                        <option value="بی حسی"> بی حسی      </option>
                        <option value="سوزن سوزن شدن اندام"> سوزن سوزن شدن اندام      </option>
                        <option value="لرزش"> لرزش     </option>
                        <option value="عدم توانایی در ایستادن"> عدم توانایی در ایستادن      </option>
                        <option value="افتادن به طرفین هنگام راه رفتن "> افتادن به طرفین هنگام راه رفتن      </option>
                        <option value="هیچکدام">هیچکدام </option>
                     
                        
                      
                </select>
            </div>
        </div>



        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
                <label>اگر مشکلات زیر (مربوط به دستگاه خونساز) را دارید گزینه های زیر را تیک بزنید:</label>
                <select name="moshkelate_dastgahe_khonsaz_pezeshki[]" class="form-control multi" multiple="multiple">
                       
                        <option value="انواع کم خونی">انواع کم خونی    </option>
                        <option value="سابقه خونریزی های زیر جلدی با حداقل یا بدون ضربه (خود به خودی)"> سابقه خونریزی های زیر جلدی با حداقل یا بدون ضربه (خود به خودی)     </option>
                        <option value="سابقه انتقال خون و یا واکنش به آن">سابقه انتقال خون و یا واکنش به آن       </option>
                        <option value="هیچکدام">هیچکدام </option>
                       
                      
                </select>
            </div>
        </div>

        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
                <label>اگر مشکلات زیر (مربوط به غدد درون ریز) را دارید گزینه های زیر را تیک بزنید:</label>
                <select name="moshkelate_dastgahe_ghodade_daronriz_pezeshki[]" class="form-control multi" multiple="multiple">
                       
                        <option value="مشکلات تیروئید">مشکلات تیروئید     </option>
                        <option value="عدم تحمل به گرما یا سرما"> عدم تحمل به گرما یا سرما     </option>
                        <option value="تعریق زیاد  ">تعریق زیاد        </option>
                        <option value="دیابت">دیابت      </option>
                        <option value="تشنگی یا گرسنگی زیاد">تشنگی یا گرسنگی زیاد       </option>
                        <option value="پر ادراری ">پر ادراری           </option>
                        <option value="هیچکدام">هیچکدام </option>
                      
                </select>
            </div>
        </div>


        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
                <label>اگر مشکلات زیر (مربوط به پستان (مخصوص خانم ها)) را دارید گزینه های زیر را تیک بزنید:</label>
                <select name="moshkelate_pestan_pezeshki[]" class="form-control multi" multiple="multiple">
                      
                        <option value="توده">توده    </option>
                        <option value="درد یا احساس ناراحتی "> درد یا احساس ناراحتی      </option>
                        <option value="ترشح">ترشح         </option>
                        <option value="هیچکدام">هیچکدام </option>
                     
                      
                </select>
            </div>
        </div>






   

        <div class="col-12">
            <div class="form-group">
            
                <label>در صورت داشتن بیماری دیگر لطفا ذکر نمایید:</label>
            <input class="form-control" type="text" name="bimari_digar_bishtar_pezeshki" style="{{ $errors->has('bimari_digar_bishtar_pezeshki') ? ' border:1px solid red' : '' }}" placeholder=" توضیحات" autocomplete="off">
            </div>
        </div>

        <div class="col-12">
            <div class="form-group">
            
                <label>در صورت مصرف هر نوع دارو لطفا ذکر نمایید:</label>
            <input class="form-control" type="text" name="masraf_daro_pezeshki" style="{{ $errors->has('masraf_daro_pezeshki') ? ' border:1px solid red' : '' }}" placeholder=" توضیحات" autocomplete="off">
            </div>
        </div>



   




    </div>







</div>

<br>