@extends('template.app')

@section('content')


    @include('template.sections.modal-menu')
    
    

    
   <section class="contact-area ptb-100">
      <div class="container">

      

        @include('template.alert.error')
        @include('template.alert.success')

    
  
        @include('template.forms.section-vip-forms.moshakhasate-bime-gozar')

        @include('template.forms.section-vip-forms.moshakhasate-bime-shode')

        @include('template.forms.section-vip-forms.moshakhasate-bime-shode-dar-zamane-hayat')

        @include('template.forms.section-vip-forms.moshakhasate-bime-shode-dar-zamane-fot')

        @include('template.forms.section-vip-forms.moshakhasate-joziate')


        @include('template.forms.section-vip-forms.soalate-omomi-bime-shode')

        @include('template.forms.section-vip-forms.vekalat-bime')

        @include('template.forms.section-vip-forms.sharayet')

       
       
        <div class="col-12">
            <button class="default-btn btn-six" type="submit">
            ارسال 
            </button>
        </div>
        </form>





        </div>
       </div>
    </div>
  </section>    



@endsection


@section('heads')

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

<style>

.select2-selection.select2-selection--single{
  display: none;
}
.select2-container{
  width: 100% !important;
}
  </style>
@endsection

@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>


<script>

$(document).ready(function() {
    $('.multi').select2();
});
</script>
@endsection