﻿@extends('template.app')

@section('content')

<section class="hero-slider-area-six" >
        <div class="hero-slider-six owl-carousel owl-theme">
                
                <div class="slider-item slider-item-bg-1">
                <div class="d-table">
                <div class="d-table-cell">
                <div class="container">
                <div class="slider-text one">
        
                <h1>
                        طرح VIP
                </h1>
                <h5>
                        حتی در خواب هم درآمد داشته باشید
                     </h5>
                <div class="slider-btn">
                <a class="default-btn btn-six" href="{{ route('form.vip') }}">
                        ثبت درخواست
                </a>
        
                </div>
                </div>
                </div>
                </div>
                </div>
                </div>


                <div class="slider-item slider-item-bg-2">
                <div class="d-table">
                <div class="d-table-cell">
                <div class="container">
                <div class="slider-text two">
        
                <h1>بیمه بدنه 
                      
                </h1>

                <h4>
                        (طرح اتوپلاس)
                </h4>
                <div class="slider-btn">
                <a class="default-btn btn-six" href="{{ route('contact-us') }}">
                تماس با ما
                </a>
        
                </div>
                </div>
                </div>
                </div>
                </div>
                </div>


                <div class="slider-item slider-item-bg-1">
                        <div class="d-table">
                        <div class="d-table-cell">
                        <div class="container">
                        <div class="slider-text one">
                
                        <h1>
                                طرح VIP
                        </h1>
                        <h5>
                                درآمد بالای 10 میلیون ماهانه
                        </h5>
                        <div class="slider-btn">
                        <a class="default-btn btn-six" href="{{ route('form.vip')}}">
                                ثبت درخواست
                        </a>
                
                        </div>
                        </div>
                        </div>
                        </div>
                        </div>
                        </div>




                        <div class="slider-item slider-item-bg-2">
                                <div class="d-table">
                                <div class="d-table-cell">
                                <div class="container">
                                <div class="slider-text two">
                        
                                <h1>
                                         درمان انفرادی و شرکتی
                                
                                        </h1>
                                        <h4>
                                                (حتی زیر ۵۰ نفر)
                                        </h4>
                                <div class="slider-btn">
                                <a class="default-btn btn-six" href="{{ route('contact-us') }}">
                                تماس با ما
                                </a>
                        
                                </div>
                                </div>
                                </div>
                                </div>
                                </div>
                                </div>


                                <div class="slider-item slider-item-bg-1">
                                        <div class="d-table">
                                        <div class="d-table-cell">
                                        <div class="container">
                                        <div class="slider-text one">
                                
                                        <h1>
                                                طرح VIP
                                        </h1>
                                        <h5>
                                           فقط با معرفی دو نفر
                                        </h5>
                                        <div class="slider-btn">
                                        <a class="default-btn btn-six" href="{{ route('form.vip')}}">
                                        ثبت درخواست
                                        </a>
                                
                                        </div>
                                        </div>
                                        </div>
                                        </div>
                                        </div>
                                        </div>



                                        <div class="slider-item slider-item-bg-2">
                                                <div class="d-table">
                                                <div class="d-table-cell">
                                                <div class="container">
                                                <div class="slider-text two">
                                        
                                                <h1>
                                                      طرح آتش سوزی و سرقت و طرح منازل مسکونی 
                                                </h1>
                                                <div class="slider-btn">
                                                <a class="default-btn btn-six" href="{{ route('contact-us') }}">
                                                تماس با ما
                                                </a>
                                        
                                                </div>
                                                </div>
                                                </div>
                                                </div>
                                                </div>
                                                </div>

        </div>
        </section>


        <section class="get-quat-area-six pb-100">
        <div class="container">
        <div class="tab get-quat-list-tab">
        <div class="row align-items-center">
        <div class="col-lg-4">
        <ul class="tabs">
        <li class="single-get-list">
        <i class="bx bx-home"></i>
        <span>بیمه مسکن</span>
        </li>
        <li class="single-get-list">
        <i class="bx bxs-plane"></i>
        <span>بیمه مسافرتی</span>
        </li>
        <li class="single-get-list">
        <i class="bx bx-user-circle"></i>
        <span>بیمه زندگی</span>
        </li>
        <li class="single-get-list">
        <i class="bx bxs-car-garage"></i>
        <span>بیمه خودرو</span>
        </li>
        </ul>
        </div>
        <div class="col-lg-4">
        <div class="tab_content">
        <div class="tabs_item">
        <div class="get-quat-img">
        <img src="{{ asset('template/img/insurance/insurance-1.jpg')}}" alt="Image">
        </div>
        </div>
        <div class="tabs_item">
        <div class="get-quat-img">
        <img src="{{ asset('template/img/insurance/insurance-2.jpg')}}" alt="Image">
        </div>
        </div>
        <div class="tabs_item">
        <div class="get-quat-img">
        <img src="{{ asset('template/img/insurance/insurance-3.jpg')}}" alt="Image">
        </div>
        </div>
        <div class="tabs_item">
        <div class="get-quat-img">
        <img src="{{ asset('template/img/insurance/insurance-4.jpg')}}" alt="Image">
        </div>
        </div>
        </div>
        </div>
        <div class="col-lg-4">
        <div class="tab_content">
        <div class="tabs_item">
        <div class="get-quat-content">
        <h3>بیمه مسکن</h3>
<p>
        حادثه هیچ گاه خبر نمی‌کند. ممکن است هر لحظه حادثه‌ای رخ دهد و طی آن به انسان‌ها و ساختمان‌ها آسیب‌های جبران‌ناپذیری وارد شود. با توجه به افزایش قیمت‌ها و رواج گرانی، ساخت دوباره یک خانه ساده برای افرادی با درآمد متوسط بسیار سخت و حتی غیر ممکن است، چه برسد به ساخت منزل برای افرادی که درآمد پایینی دارند و جزو اقشار ضعیف جامعه هستند. به همین علت، بهترین راه برای این که خیال خودتان را از آینده راحت کنید، این است از بیمه منازل مسکونی استفاده کنید.
</p>
        <a href="{{route('contact-us')}}" class="default-btn btn-six">
        تماس با ما
        </a>
        </div>
        </div>
        <div class="tabs_item">
        <div class="get-quat-content">
        <h3>بیمه مسافرتی</h3>
<p>
        سفر رفتن یکی از لذت بخش‌ترین تجربه‌های زندگی است. وقتی برای آن برنامه‌ریزی می‌کنید انگار دارید داستانی را می‌نویسید که قهرمانش خودتان هستید. نوعی ماجراجویی که در بستر آن می‌توانید به شناخت بهتری از خود برسید. سفر کردن نیز مانند هر کاری نیاز به ملزوماتی دارد. برای اینکه حوادث احتمالی در این مسیر کام‌تان را تلخ نکند لازم است از قبل مهیا شوید. بیمه مسافرتی یکی از مهم‌ترین ملزوماتی است که با همراه داشتن آن می‌توانید سفری ایمن و خاطره‌انگیز داشته باشید
</p>
        <a href="{{route('contact-us')}}" class="default-btn btn-six">
                تماس با ما
        </a>
        </div>
        </div>
        <div class="tabs_item">
        <div class="get-quat-content">
        <h3>بیمه زندگی</h3>
<p>
        بیمه زندگی مزایای بسیاری دارد که از مهم‌ترین آن‌ها می‌توان به معافیت مالیاتی این نوع، امکان دریافت سود مشارکت، دریافت سود تضمینی، امکان گرفتن وام با بیمه زندگی، امکان تعیین استفاده کنندگان یا ذی نفع بیمه‌نامه توسط بیمه‌گزار، امکان بازخرید بیمه عمر زودتر از موعد، امکان خرید بدون محدودیت سنی اشاره کرد.یکی از موضوعات بااهمیت در رابطه با این نوع بیمه تعیین میزان پرداخت حق بیمه و روش پرداخت حق بیمه توسط خود شخص بیمه‌گزار است که می‌تواند موضوع مهمی برای افرادی که قصد استفاده از این نوع بیمه‌ را دارند محسوب شود.
</p>
        <a href="{{route('contact-us')}}" class="default-btn btn-six">
                تماس با ما
        </a>
        </div>
        </div>
        <div class="tabs_item">
        <div class="get-quat-content">
        <h3>بیمه خودرو</h3>
<p>
        در بیمه بدنه، اتومبیل بیمه شده در مقابل خطرات سرقت كلی ، آتش سوزی ، انفجار و تصادف  تحت پوشش قرار گرفته و خسارت های جزئی و كلی وارد شده به اتومبیل بیمه شده اعم از دستمزد و ارزش لوازم و قطعات استفاده شده به  قیمت روز حادثه پرداخت میشود
</p>
        <a href="{{ route('contact-us')}}" class="default-btn btn-six">
                تماس با ما
        </a>
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>
        </section>



@include('template.sections.modal-menu')





<section class="about-area about-area-five ptb-100">
<div class="container">
<div class="row align-items-center">
<div class="col-lg-4">
<div class="about-content">
<span>مزایای VIP</span>
<h2>
       مزایای مالی و غیر مالی هم برای زندگانی و هم برای آخرت

</h2>
<h5>(همه در یک پکیج و با یک حق بیمه)</h5>

</div>
</div>
<div class="col-lg-8">
<div class="row">
<div class="col-lg-6 col-sm-6">
<div class="excellence-item">
<i class="bx bx-task"></i>
<h3>پوشش عمر</h3>
<p>
</p>
</div>
</div>
<div class="col-lg-6 col-sm-6">
<div class="excellence-item">
<i class="bx bx-collapse"></i>
<h3>سرمایه گذاری</h3>
<p>
</p>
</div>
</div>


<div class="col-lg-6 col-sm-6">
<div class="excellence-item">
<i class="bx bx-user-circle"></i>
<h3> کارت اعتباری</h3>
<p>
</p>
</div>
</div>


<div class="col-lg-6 col-sm-6">
<div class="excellence-item">
<i class="bx bx-check-square"></i>
<h3>درآمد زایی</h3>
<p>

</p>
</div>
</div>


</div>
</div>
</div>
</div>
</section>


<section class="choose-us-area-three choose-us-area-five pt-100 pb-70">
<div class="container">
<div class="section-title section-title-five">

<h2>ویژگی های طرح VIP</h2>
<p>

        با این طرح چی بدست میاریم؟
</p>
<p>
سود روز شمار که شرکت ماهانه یه حساب بیمه ای ما میریزه ، حدود ۳۰۶ میلیون تومان پوشش بیمه ای و در نهایت سقف درآمد ی ماهانه ۷۰ میلیون تومان  با دعوت فقط دو نفر و پیگیری آنها برای معرفی دو نفر بعدی توسط آنها تا ۵ سال اول قرار داد و کلی بیمه نامه های دیگه مثل درمان ، آتش سوزی ، ثالث و ... و از طرفی پاداش ها.
</p>
</div>
<div class="row">
<div class="col-lg-4 col-sm-6">
<div class="single-choose">
<span class="flaticon-hand"></span>
<h3>پوشش سرمایه گذاری </h3>
<p> </p>
</div>
</div>
<div class="col-lg-4 col-sm-6">
<div class="single-choose">
<span class="flaticon-life-insurance-1"></span>
<h3>مجموعه ای از پوشش های عمر برای زندگی</h3>
<p>

</p>
</div>
</div>
<div class="col-lg-4 col-sm-6 offset-sm-3 offset-lg-0">
<div class="single-choose">
<span class="flaticon-insurance-1"></span>
<h3>درآمد زایی از بیمه با طرح VIP</h3>
<p></p>
</div>
</div>
</div>
</div>
</section>



<section class="services-area-three services-area-five pt-100 pb-70">
        <div class="container">
        <div class="section-title section-title-five">
        <span> محصولات</span>
        <h2>انواع محصولات بیمه ای </h2>
        <p>
        
        به جز طرح VIP با گرفتن کد معرف میتوانید از دیگر محصولات ما هم استفاده نمایید و با اعلام کد معرف و گرفتن بیمه نامه های دیگر برای دوستان  و آشنایان خود درآمد کسب کنید
        </p>
        </div>
        <div class="row">
        <div class="col-lg-4 col-sm-6">
        <div class="single-choose">
        <span class="flaticon-car-insurance"></span>
        <h3>بیمه بدنه طرح اتوپلاس</h3>
        
        <p>
        
                در این طرح خدماتی ویژه به شرح زیر به صورت رایگان به همراه بیمه بدنه خودرو به مشتریان دارای خودروهای با ارزش ۸۰۰ میلیون ریال و بیشتر ارائه می‌شود و شامل خدمات ویژه شرکت بیمه نوین به صورت رایگان و خدمات اختصاصی امداد خودرو به صورت رایگان است.
        
        </p>
        <a href="#">
        <i class="flaticon-left"></i>
        </a>
        </div>
        </div>
        <div class="col-lg-4 col-sm-6">
        <div class="single-choose">
        <span class="flaticon-health-care"></span>
        <h3>بیمه ثالث</h3>
        <p>
        
                با ارسال مدارک درخواستی در واتساپ یا بارگذاری در بخش فرم ثالث  حق بیمه به شما اعلام میگردد. 
        </p>
        <a href="#">
        <i class="flaticon-left"></i>
        </a>
        </div>
        </div>
        <div class="col-lg-4 col-sm-6">
        <div class="single-choose">
        <span class="flaticon-salesman"></span>
        <h3>بیمه درمان انفرادی</h3>
        <p>
        
                متاسفانه وقتی مشکای پیش میاد یاد بیمه میوفتیم .علی الخصوص  بیمه درمان. بیمه درمان تکمیلی  یکی از انواع بیمه های مرتبط با سلامت ماست .متاسفانه وقتی....
        </p>
        <a href="#">
        <i class="flaticon-left"></i>
        </a>
        </div>
        </div>
        <div class="col-lg-4 col-sm-6">
        <div class="single-choose">
        <span class="flaticon-call"></span>
        <h3>بیمه موبایل</h3>
        
        <p>
        
        تا حالا گوشی از دستت افتاده .میدونی چه بلایی سرش میاد؟تازه اون موقع قدر گوشی و بیمه رو میفهمیم! بیمه کدرن گوشی موبایل دقیقا مثل بیمه کردن خودرو یا سایر لوازم دیگه است .با کمترین هزینه از گوشیت محافظت میکنیم .
        
        </p>
        
        <a href="#">
        <i class="flaticon-left"></i>
        </a>
        </div>
        </div>
        <div class="col-lg-4 col-sm-6">
        <div class="single-choose">
        <span class="flaticon-life-insurance"></span>
        <h3>بیمه دندانپزشکی
               
        </h3>
        <h6>
                (انفرادی و خانوادگی)
        </h6>
        <p>
        
        خدمات دندانپزشکی یکی از پر هزینه ترین خدمات  درمانی است.
        
        ما با کمترین هزینه و حق بیمه  دندان ها ی شما رو در مقابل  هزینه ها سر سام آور بیمه میکنیم .
        
        
        
                ۵۰٪ تخفیف واقعی روی تعرفه های نظام پزشکی 
        
        
        
               . یکسال گارانتی خدمات
        </p>
        
        <a href="#">
        <i class="flaticon-left"></i>
        </a>
        </div>
        </div>
                <div class="col-lg-4 col-sm-6">
                <div class="single-choose">
                <span class="flaticon-travel-insurance"></span>
                <h3>
                        
                       
                        
                        بیمه آتش سوزی 
                       
                </h3>
                <h6>
                        ( مفازه ها و فروشگاه ها )
                </h6>
              
                <p>
        
        
                          با توجه به آمار ها ، حوادث آتش سوزی روزانه حداقل ۵۰ مورد هستش .میدونی با چه خونه دلی  کاسبی راه انداختی؟ با کمترین هزینه  ممکن مغاده و اجناست  در مقابل آتش سوزی بیمه کن .بهترین خدمات و کمترین هزینه ها . 
               
                </p>
                        <a href="#">
                <i class="flaticon-left"></i>
                </a>
                </div>
                </div>
        
        
                <div class="col-lg-4 col-sm-6">
                        <div class="single-choose">
                        <span class="flaticon-travel-insurance"></span>
                        <h3>
                                بیمه حوادث 
                                
                                <h6>
                                        (انفرادی و خانوادگی)
                                </h6>
                        </h3>
        
        <p>
        
        حادثه خبر نمیکنه و برای هر کسی ممکنه اتفاق بیوفته ، فرقی نمیکنه کجا هستیم و چه کاره ایم !هزینه بیمارستانی ، از کار افتادگی ر فوت رو پوشش میده. قثط با روزی حدودا ۱۰۰۰ تومن  هم خودت و هم خانوادت  در مقابل  خطرات  احتمالی پیش بینی نشده  بیمه کن.
        
        </p>
                        <a href="#">
                        <i class="flaticon-left"></i>
                        </a>
                        </div>
                        </div>
        
                        <div class="col-lg-4 col-sm-6">
                                <div class="single-choose">
                                <span class="flaticon-travel-insurance"></span>
                                <h3>
                                        بیمه آتش سوزی طرح منازل مسکونی
                                </h3>
                                <h6>
                                        ( زلزله و سرقت )
                                </h6>
        <p>
        
         زندگی  رو با هزار سختی و بد بختی  و آجر به آجر می سازیم ام در یک لحظه بر اثر غفلت ممکنه آتش ممکنه همه رو خاکستر کنه ! یا آقا دزده  هوش کنه که به آپارتمان ما شب خونی بزنه ! قبل از این اتقافات ما به شما هشدار  میدیم که یه  فکر   مال خودتون که با خونو  دل جمع کردید باشید.  
        </p>
        
                                <a href="#">
                                <i class="flaticon-left"></i>
                                </a>
                                </div>
                                </div>
        
        
                                <div class="col-lg-4 col-sm-6">
                                        <div class="single-choose">
                                        <span class="flaticon-travel-insurance"></span>
                                        <h3>
                                                بیمه حیوانات 
                                             
                                        </h3>
                                        <h6>
                                                (سگ و گربه)
                                        </h6>
        
                                        <p>
        
                     
                                                وقتی یه حیوونی رو میاریم خونه در مقابل سلامتی اون مسؤل هستیم .سلامتی هزینه داره  ! هر چند شنیدیم اونایی که حیوون خونگی دارن مثل بچه هاشون ازش مراقبت میکنن جون اونا هم ممکنه مریض بشن و نیاز به دامپزشک  داشته باشن . با بیمه حیوانات ، هزینه های درمانی را کاهش دهیم.
                                        </p>
                                        <a href="#">
                                        <i class="flaticon-left"></i>
                                        </a>
                                        </div>
                                        </div>
                
        
        </div>
        </div>
        </section>
        

<section class="find-an-agent-area pb-100">
<div class="container">
<div class="find-an-agent-bg">
<div class="row align-items-center">
<div class="col-lg-4">
<div class="find-an-agent-content">
<h2> فرم های بیمه</h2>
<p> برای درخواست هر کدام از محصولات بر روی فرم آن کلیک کنید.</p>

</div>
</div>
<div class="col-lg-8">
<div class="row">
<div class="col-lg-4 col-sm-6 p-0">
<div class="single-find-an-agent">
<i class=" bx bxs-business"></i>
<a href="#">
آتش سوزی منازل مسکونی
</a>
</div>
</div>
<div class="col-lg-4 col-sm-6 p-0">
<div class="single-find-an-agent">
<i class="bx bx-car"></i>
<a href="#">
بدنه اتوپلاس
</a>
</div>
</div>
<div class="col-lg-4 col-sm-6 p-0">
<div class="single-find-an-agent">
<i class="bx bx-home"></i>
<a href="#">
ثالث
</a>
</div>
</div>
 <div class="col-lg-4 col-sm-6 p-0">
<div class="single-find-an-agent">
<i class="bx bx-user-circle"></i>
<a href="#">
درمان انفرادی
</a>
</div>
</div>
<div class="col-lg-4 col-sm-6 p-0">
<div class="single-find-an-agent">
<i class="bx bxs-clinic"></i>
<a href="#">
دندان پزشکی
</a>
</div>
</div>
<div class="col-lg-4 col-sm-6 p-0">
<div class="single-find-an-agent">
<i class="bx bx-book-reader"></i>
<a href="{{route('form.vip')}}">
Vip
</a>
</div>
</div>
<div class="col-lg-4 col-sm-6 p-0">
        <div class="single-find-an-agent">
        <i class="bx bx-book-reader"></i>
        <a href="{{route('form.vip')}}">
        حوادث 
        </a>
        </div>
        </div>
        <div class="col-lg-4 col-sm-6 p-0">
                <div class="single-find-an-agent">
                <i class="bx bx-home"></i>
                <a href="{{route('form.vip')}}">
                آتش سوزی مغازه
                </a>
                </div>
                </div>
                <div class="col-lg-4 col-sm-6 p-0">
                        <div class="single-find-an-agent">
                        <i class="bx bx-book-reader"></i>
                        <a href="{{route('form.vip')}}">
                        بیمه حیوانات
                        </a>
                        </div>
                        </div>
</div>
</div>
</div>
</div>
</div>
</section>



<section class="counter-area counter-area-three pt-100 pb-70">
<div class="container">
<div class="row">
<div class="col-lg-3 col-md-6 col-sm-6">
<div class="single-counter">
<i class="bx bx-check-circle"></i>
<h2>
<span class="odometer" data-count="512">00</span>
<span class="target"></span>
</h2>
<p> بیمه شدگان</p>
</div>
</div>
<div class="col-lg-3 col-md-6 col-sm-6">
<div class="single-counter">
<i class="flaticon-trophy"></i>
<h2>
<span class="odometer" data-count="20">00</span>
<span class="target"></span>
</h2>
<p>جوایز اهدایی</p>
</div>
</div>
<div class="col-lg-3 col-md-6 col-sm-6">
<div class="single-counter">
<i class="flaticon-smile"></i>
<h2>
<span class="odometer" data-count="657">00</span>
<span class="traget"></span>
</h2>
<p>کاربران</p>
</div>
</div>
<div class="col-lg-3 col-md-6 col-sm-6">
<div class="single-counter">
<i class="flaticon-support"></i>
<h2>
<span class="odometer" data-count="17">00</span>
<span class="target"></span>
</h2>
<p>خیریه</p>
</div>
</div>
</div>
</div>
</section>







<section class="get-quat-area get-quat-area-three get-quat-area-five ptb-100">
<div class="container">
<div class="row align-items-center">
<div class="col-lg-6 col-md-6">
<div class="get-quat-content">
<div class="section-title">
<h2>
        ارسال سریع درخواست
</h2>
<p>

        در این قسمت خیلی سریع میتوانید فرم مورد نطر را برای ما ارسال کنید و ما در کمترین زمان به آن رسیدگی میکنیم

</p>
</div>
<ul>
<li>
<i class="flaticon-maps-and-flags"></i>
<p>
        
</p>

</li>

<li>
<i class="flaticon-email"></i>
<h3>برای اطلاعات به ما ایمیل کنید</h3>
<p><a>support@karbim.ir</a></p>
</li>
</ul>
</div>
</div>
<div class="col-lg-6 col-md-6">
<div class="quat-form">
<div class="tab">
<ul class="tabs">
<li>
<a href="#">
<div class="dot"></div>  مشاوره
</a>
</li>



</ul>
<div class="tab_content">
        
<form action="{{route('send.consulting')}}" method="POST" class="tabs_item">
       @csrf
        <br>
<p> 
        در سریع ترین زمان رسیدگی می شود  </p>
        <br>
        @include('template.alert.error')
        @include('template.alert.success')
<div class="form">
<div class="form-group">
<input type="text" name="name" class="form-control" id="Name" value="{{old('name')}}" style="{{ $errors->has('name') ? ' border:2px solid red' : '' }}" placeholder="نام و نام خانوادگی" autocomplete="off">
</div>
<div class="form-group">
<input type="email" name="email" class="form-control" id="Email" value="{{old('email')}}" style="{{ $errors->has('email') ? ' border:2px solid red' : '' }}" placeholder="ایمیل "  autocomplete="off">
</div>
<div class="form-group">
<input type="text" name="mobile" class="form-control" id="Number" value="{{old('mobile')}}" style="{{ $errors->has('mobile') ? ' border:2px solid red' : '' }}" placeholder="موبایل " autocomplete="off">
</div>

<button type="submit" class="default-btn btn-five">
ثبت 
</button>

</form>
</div>




</div>
</div>
</div>
</div>
</div>
</div>
</section>



@endsection


@section('heads')



@endsection

@section('scripts')



@endsection
