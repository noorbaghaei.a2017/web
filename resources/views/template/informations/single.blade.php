@extends('template.app')

@section('content')


    <!--page title start-->

    <section class="page-title o-hidden text-center parallaxie" data-overlay="7" data-bg-img="images/bg/02.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-12 ml-auto mr-auto">

                </div>
            </div>
            <nav aria-label="breadcrumb" class="page-breadcrumb">

            </nav>
        </div>
    </section>

    <!--page title end-->


    <!--body content start-->

    <div class="page-content">

        <!--blog start-->

        <section class="pb-17 sm-pb-8">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 col-md-12 order-lg-12">
                        <div class="left-side">
                            <div class="post-image">
                                @if(!$item->Hasmedia('images'))
                                    <img class="img-fluid" src="{{asset('img/no-img.gif')}}" alt="{{convert_lang($item,LaravelLocalization::getCurrentLocale(),'title')}}" title="{{convert_lang($item,LaravelLocalization::getCurrentLocale(),'title')}}" >
                                @else
                                    <img class="img-fluid" src="{{$item->getFirstMediaUrl('images')}}" alt="{{convert_lang($item,LaravelLocalization::getCurrentLocale(),'title')}}" title="{{convert_lang($item,LaravelLocalization::getCurrentLocale(),'title')}}" >
                                @endif
                            </div>
                            <div class="blog-details">
                                <div class="post-desc">
                                    <div class="post-date mb-3">{{convert_date($item,LaravelLocalization::getCurrentLocale(),'created_at')}}</div>
                                    <div class="post-title">
                                        <h4>{{convert_lang($item,LaravelLocalization::getCurrentLocale(),'title')}}</h4>
                                    </div>
                                    <p class="mb-0 line-h-3">
                                        {!! convert_lang($item,LaravelLocalization::getCurrentLocale(),'text') !!}
                                    </p>
                                    <div class="post-bottom mt-3 px-0">
                                        <div class="post-meta">
                                            <ul class="list-inline">
                                                <li>{{__('cms.view')}}</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-12 sidebar order-lg-1">
                        <div class="widget">
                            <h5 class="widget-title">{{__('cms.recent_posts')}}</h5>
                            <div class="recent-post">
                                <ul class="list-unstyled">
                                    @foreach($laste_informations as $information)
                                        <li class="mb-3">
                                            <div class="recent-post-thumb">
                                                @if(!$information->Hasmedia('images'))
                                                    <img class="img-fluid" src="{{asset('img/no-img.gif')}}" alt="{{convert_lang($information,LaravelLocalization::getCurrentLocale(),'title')}}" title="{{convert_lang($information,LaravelLocalization::getCurrentLocale(),'title')}}" >
                                                @else
                                                    <img class="img-fluid" src="{{$information->getFirstMediaUrl('images')}}" alt="{{convert_lang($item,LaravelLocalization::getCurrentLocale(),'title')}}" title="{{convert_lang($information,LaravelLocalization::getCurrentLocale(),'title')}}" >
                                                @endif
                                            </div>
                                            <div class="recent-post-desc">
                                                <a href="{{route('informations.single',['information'=>convert_lang($information,LaravelLocalization::getCurrentLocale(),'slug')])}}">{{convert_lang($information,LaravelLocalization::getCurrentLocale(),'title')}}</a>
                                                <span><i class="fas fa-calendar-alt text-theme ml-1"></i>{{convert_date($information,LaravelLocalization::getCurrentLocale(),'created_at')}}</span>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="widget">
                            <h5 class="widget-title">{{__('cms.follow_us')}}</h5>
                            <div class="social-icons social-colored">
                                <ul class="list-inline mb-0">
                                    <li class="social-facebook"><a href="https://telegram.me/{{$setting->info->telegram}}"><i class="fab fa-telegram" aria-hidden="true"></i></a>
                                    </li>
                                    <li class="social-twitter"><a href="https://www.instagram.com/{{$setting->info->instagram}}"><i class="fab fa-instagram" aria-hidden="true"></i></a>
                                    </li>
                                    <li class="social-gplus"><a href="https://wa.me/{{$setting->info->whatsapp}}"><i class="fab fa-whatsapp" aria-hidden="true"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--blog end-->

    </div>

    <!--body content end-->



@endsection
