@extends('template.app')

@section('content')

   <!-- content-->
   <div class="content">
                    <!--  section  -->
                    <section class="parallax-section single-par" data-scrollax-parent="true">
                        <div class="bg par-elem "  data-bg="{{asset('template/images/job_banner.jpg')}}" data-scrollax="properties: { translateY: '30%' }"></div>
                        <div class="overlay op7"></div>
                        <div class="container">
                            <div class="section-title center-align big-title">
                                
                            
                            </div>
                        </div>
                        <div class="header-sec-link">
                            <a href="#sec1" class="custom-scroll-link"><i class="fal fa-angle-double-down"></i></a> 
                        </div>
                    </section>
                    <!--  section  end-->
                    <section class="gray-bg small-padding no-top-padding-sec" id="sec1">
                        <div class="container">
                           
                            <div class="mob-nav-content-btn  color2-bg show-list-wrap-search ntm fl-wrap"><i class="fal fa-filter"></i>  Filters</div>
                            <div class="fl-wrap" style="margin-top: 20px">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class=" fl-wrap lws_mobile   tabs-act block_box">
                                            <div class="filter-sidebar-header fl-wrap" id="filters-column">
                                                <ul class="tabs-menu fl-wrap no-list-style">
                                                    <li class="current"><a href="#filters-search"> <i class="fal fa-sliders-h"></i> {{ __('cms.filter') }} </a></li>
                                                   
                                                </ul>
                                            </div>
                                            <div class="scrl-content filter-sidebar    fs-viscon">
                                                <!--tabs -->                       
                                                <div class="tabs-container fl-wrap">
                                                    <!--tab -->
                                                    <div class="tab">
                                                        <form action="{{route('search.page.werbung',['filter'=>1])}}" method="GET" id="filters-search" class="tab-content  first-tab ">
                                                          
                                                            <!-- listsearch-input-item-->
                                                            <div class="listsearch-input-item">
                                                                <select name="type_werbung" data-placeholder="Categories" class="chosen-select no-search-select" >
                                                                <option value="all" {{ app('request')->input('type_werbung')=="all" ? "selected" : ""  }}>{{__('cms.all')}}</option>
                                                                <option value="employee" {{ app('request')->input('type_werbung')=="employee" ? "selected" : ""  }}>{{__('cms.employee')}}</option>
                                                            <option value="employer" {{ app('request')->input('type_werbung')=="employer" ? "selected" : ""  }}>{{__('cms.employer')}}</option>
                                                           
                                                           
                                                                  
                                                                </select>
                                                            </div>
                                                            <!-- listsearch-input-item end-->
                                                              <!-- listsearch-input-item-->
                                                              <div class="listsearch-input-item">
                                                                <select name="type" data-placeholder="Type" class="chosen-select no-search-select" >
                                                                    <option value="all" {{ app('request')->input('type')=="all" ? "selected" : ""  }}>{{__('cms.all')}}</option>
                                                                    <option value="yearly" {{ app('request')->input('type')=="yearly" ? "selected" : ""  }}>{{__('cms.yearly')}}</option>
                                                                <option value="monthly" {{ app('request')->input('type')=="monthly" ? "selected" : ""  }}>{{__('cms.monthly')}}</option>
                                                            <option value="daily" {{ app('request')->input('type')=="daily" ? "selected" : ""  }}>{{__('cms.daily')}}</option>
                                                            <option value="hourly" {{ app('request')->input('type')=="hourly" ? "selected" : ""  }}>{{__('cms.hourly')}}</option>
                                                            <option value="project" {{ app('request')->input('type')=="project" ? 'selected' : ""  }}>{{__('cms.project')}}</option>
                                                            
                                                           
                                                           
                                                                  
                                                                </select>
                                                            </div>
                                                            <!-- listsearch-input-item end-->
                                                                             <!-- listsearch-input-item-->
                                                            <div class="listsearch-input-item">
                                                                <span class="iconn-dec"><i class="fas fa-euro-sign"></i></span>
                                                                <input  type="text" name="price_werbung" placeholder="{{__('cms.salary')}}" value="{{ app('request')->input('price_werbung') ? app('request')->input('price_werbung') : ""  }}" autocomplete="off"/>
                                                            </div>
                                                            <!-- listsearch-input-item end-->                                 
                                                            <!-- listsearch-input-item-->
                                                            <div class="listsearch-input-item location autocomplete-container">
                                                                <span class="iconn-dec"><i class="far fa-map-marker"></i></span>
                                                                <input type="text" name="wo_werbung" placeholder=" {{__('cms.where')}}"  value="{{ app('request')->input('wo_werbung') ? app('request')->input('wo_werbung') : ""  }}" class="autocomplete-input" id="autocompleteid3" value="" autocomplete="off"/>
                                                                <a href="#"><i class="fal fa-location"></i></a>
                                                            </div>
                                                            <!-- listsearch-input-item end-->    


                                                                                                      
                                                            <!-- listsearch-input-item-->
                                                            <div class="listsearch-input-item">
                                                                <button class="header-search-button color-bg" ><span>{{ __('cms.search') }}</span></button>
                                                            </div>
                                                            <!-- listsearch-input-item end-->                                                     
                                                            
                                                        </form>
                                                    </div>
                                                    <!--tab end-->
                                                   
                                                </div>
                                                <!--tabs end-->                         
                                            </div>
                                        </div>
                                        <a class="back-tofilters color2-bg custom-scroll-link fl-wrap" href="#filters-column">Back to filters <i class="fas fa-caret-up"></i></a>
                                    </div>
                                    <div class="col-md-8">
                                                            
                                        <!-- listing-item-container -->
                                        <div class="listing-item-container init-grid-items fl-wrap nocolumn-lic">
                                          @foreach($items as $item)
                                            <!-- listing-item  -->
                                            <div class="listing-item">
                                                <article class="geodir-category-listing fl-wrap">
                                                    <div class="geodir-category-img">
                                                     
                                                        <a href="{{route('werbungs.single',['werbung'=>$item->slug])}}" class="geodir-category-img-wrap fl-wrap">
                                                        @if($item->Hasmedia('images'))
                                                                   <img  src="{{$item->getFirstMediaUrl('images')}}" alt="title" title="title" >

           
                                                                    @else
                                                                    <img  src="{{asset('template/images/no-image.jpg')}}" alt="title" title="title" >

                                                             

                                                               @endif
                                                        </a>
                                                        <div class="listing-avatar"><a href="#">
                                                        @if($item->user_info->Hasmedia('images'))
                                                        <img src="{{$item->user_info->getFirstMediaUrl('images')}}" alt="">
                                                        @else
                                                        <img src="{{asset('template/images/Default-welcomer.png')}}" alt="">
                                                        @endif
                                                        </a>
                                                            <span class="avatar-tooltip">{{$item->user_info->full_name}}</span>
                                                        </div>
                                                     
                                                       
                                                    </div>
                                                    <div class="geodir-category-content fl-wrap title-sin_item">
                                                        <div class="geodir-category-content-title fl-wrap">
                                                            <div class="geodir-category-content-title-item">
                                                                <h3 class="title-sin_map"><a href="{{route('werbungs.single',['werbung'=>$item->slug])}}">{{convert_lang($item,LaravelLocalization::getCurrentLocale(),'title')}}</a></h3>
                                                                @if($item->access_phone=="1")
                                                                <div class="geodir-category-location fl-wrap"><a  href="tel::{{$item->phone}}" ><i class="fal fa-phone"></i> {{$item->phone}}</a></div>
                                                               @endif
                                                                @if($item->access_email=="1")
                                                                <div class="geodir-category-location fl-wrap"><a  href="maito::{{$item->email}}" ><i class="fas fal fa-envelope"></i> {{$item->email}}</a></div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="geodir-category-text fl-wrap">
                                                            <p class="small-text">{{Str::limit(convert_lang($item,LaravelLocalization::getCurrentLocale(),'excerpt'), 25)}}</p>
                                                           
                                                        </div>
                                                      
                                                    </div>
                                                </article>
                                            </div>
                                            <!-- listing-item end -->                                       
                                   @endforeach
                                        </div>
                                        <!-- listing-item-container end -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <div class="limit-box fl-wrap"></div>
                </div>
                <!--content end-->

@endsection

@section('scripts')

<script>
 console.log("ok");
$('input[name=price_werbung]').keyup(function(){
   
   console.log("asd");
   $(this).val($(this).val().replace(/[^\d]/,''));
});

</script>
@if(LaravelLocalization::getCurrentLocale()=="fa" || LaravelLocalization::getCurrentLocale()=="ar")

<script src="{{asset('template/js/shop.js')}}" defer></script>
<link type="text/css" rel="stylesheet" href="{{asset('template/css/shop.css?')}}{{uniqid()}}">

@else

<script src="{{asset('template/js/ltr-shop.js')}}" defer></script>
<link type="text/css" rel="stylesheet" href="{{asset('template/css/ltr-shop.css?')}}{{uniqid()}}">


@endif
@endsection



