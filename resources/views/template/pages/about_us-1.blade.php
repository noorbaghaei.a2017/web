@extends('template.app')

@section('content')


 <!-- content-->
 <div class="content">
                    <div class="page-scroll-nav">
                        <nav class="scroll-init color2-bg">
                            <ul class="no-list-style">
                                <li><a class="act-scrlink tolt" href="#sec1" data-microtip-position="left" data-tooltip=" {{__('cms.about-us')}}"><i class="fal fa-building"></i></a></li>
                                <li><a href="#sec2" class="tolt" data-microtip-position="left" data-tooltip="{{__('cms.text')}}"><i class="fal fa-info"></i></a></li>
                                <li><a href="#sec3" class="tolt" data-microtip-position="left" data-tooltip=" {{__('cms.staff')}}"><i class="far fa-users-crown"></i></a></li>
                                <li><a href="#sec4" class="tolt" data-microtip-position="left" data-tooltip=" {{__('cms.features')}}"><i class="fal fa-user-astronaut"></i></a></li>
                            </ul>
                        </nav>
                    </div>
                    <!--  section  -->
                    <section class="parallax-section single-par" data-scrollax-parent="true">
                        <div class="bg par-elem "  data-bg="{{asset('template/images/iniaz_banner.jpg')}}" data-scrollax="properties: { translateY: '30%' }"></div>
                        <div class="overlay op7"></div>
                        <div class="container">
                            <div class="section-title center-align big-title">
                                <h2><span>  {{__('cms.about-us')}}</span></h2>
                            </div>
                        </div>
                        <div class="header-sec-link">
                            <a href="#sec1" class="custom-scroll-link"><i class="fal fa-angle-double-down"></i></a> 
                        </div>
                    </section>
                    <!--  section  end-->
                    <section   id="sec1" data-scrollax-parent="true">
                        <div class="container">
                         
                            <!--about-wrap -->
                            <div class="about-wrap">
                                <div class="row">
                                <div class="col-md-6">
                                        <div class="list-single-main-media fl-wrap" style="box-shadow: 0 9px 26px rgba(58, 87, 135, 0.2);">
                                            <img src="{{asset('template/images/banner-index-1.jpg')}}" class="respimg" alt="">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="ab_text">
                                            <div class="ab_text-title fl-wrap">
                                                <h3>{{__('cms.about-us')}}</span></h3>
                                              
                                                <span class="section-separator fl-sec-sep"></span>
                                            </div>
                                            <p>

                                            </p>
                                            <a href="#sec3" class="btn color2-bg float-btn custom-scroll-link"> {{__('cms.staff')}} <i class="fal fa-users"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- about-wrap end  --> 
                            <span class="fw-separator"></span>
                           
                        </div>
                    </section>
                    <!--section end-->  
                     
                    <!--section  -->  
                    <section class="parallax-section video-section" data-scrollax-parent="true" id="sec2">
                        <div class="bg par-elem "  data-bg="{{asset('template/images/iniaz_banner.jpg')}}" data-scrollax="properties: { translateY: '30%' }"></div>
                        <div class="overlay op7"></div>
                        <!--container-->
                        <div class="container">
                            <div class="video_section-title fl-wrap">
                                <h4>{{__('cms.text')}}</h4>
                            </div>
                        </div>
                    </section>
                    <!--section end-->  
                    <!--section -->  
                    <section id="sec3">
                        <div class="container">
                            <div class="section-title">
                                <h2> {{__('cms.staff')}} </h2>
                               
                             
                            </div>
                            <div class="about-wrap team-box2 fl-wrap">
                            @foreach ($members as $member)
                                 <!-- team-item -->
                                 <div class="team-box">
                                    <div class="team-photo">
                                    @if(!$member->Hasmedia('images'))
                                                                       
                                                                        <img src="{{asset('img/no-img.gif')}}" alt="" class="respimg">
                                                                    @else
                                                                   
                                                                    <img src="{{$member->getFirstMediaUrl('images')}}" alt="" class="respimg">
                                                                    @endif
                                        
                                    </div>
                                    <div class="team-info fl-wrap">
                                        <h3><a href="#"> {{$member->first_name}} {{$member->last_name}} </a></h3>
                                        <!-- <h4>{{$member->role_name}} </h4> -->
                                        <p>
                                       
                                        </p>
                                        <div class="team-social">
                                            <ul class="no-list-style">
                                                <li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                                <li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                               
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- team-item  end-->
                            @endforeach
                               
                               
                            </div>
                        </div>
                        <div class="waveWrapper waveAnimation">
                          <div class="waveWrapperInner bgMiddle">
                            <div class="wave-bg-anim waveMiddle" style="background-image: url({{asset('template/images/wave-top.png')}})"></div>
                          </div>
                          <div class="waveWrapperInner bgBottom">
                            <div class="wave-bg-anim waveBottom" style="background-image: url({{asset('template/images/wave-top.png')}})"></div>
                          </div>
                        </div> 						
                    </section>
                    <!--section end-->  
                    <!--section  -->  
                    <section class="gray-bg">
                        <div class="container">
                          
                        </div>
                    </section>
                    <!--section end-->  
                    <!--section  -->  
                    <section class="parallax-section" data-scrollax-parent="true" id="sec4">
                        <div class="bg par-elem "  data-bg="{{asset('template/images/iniaz_banner.jpg')}}" data-scrollax="properties: { translateY: '30%' }"></div>
                        <div class="overlay op7"></div>
                        <!--container-->
                        <div class="container">
                            <div class="section-title center-align big-title">
                                <h2><span>  {{__('cms.features')}}</span></h2>
                                <span class="section-separator"></span>
                            </div>
                        </div>
                    </section>
                    <!--section end-->  
                    <!--section  -->  
                    <section class="gray-bg absolute-wrap_section">
                        <div class="container">
                            <div class="absolute-wrap fl-wrap">
                                <!-- features-box-container --> 
                                <div class="features-box-container fl-wrap">
                                    <div class="row">
                                         <!--features-box --> 
                                         <div class="col-md-4">
                                            <div class="features-box gray-bg">
                                                <div class="time-line-icon">
                                                    <i class="fas fa-desktop"></i>
                                                </div>
                                                <h3>{{__('cms.web-design')}} </h3>
                                             <p>

                                             </p>
                                            </div>
                                        </div>
                                        <!-- features-box end  --> 
                                        <!--features-box --> 
                                        <div class="col-md-4">
                                            <div class="features-box gray-bg">
                                                <div class="time-line-icon">
                                                    <i class="fas fa-camera"></i>
                                                </div>
                                                <h3>{{__('cms.photo-design')}} </h3>
                                             <p>

                                             </p>
                                            </div>
                                        </div>
                                        <!-- features-box end  --> 
                                        <!--features-box --> 
                                        <div class="col-md-4">
                                            <div class="features-box gray-bg">
                                                <div class="time-line-icon">
                                                    <i class="fa fa-paint-brush"></i>
                                                </div>
                                                <h3>{{__('cms.grafic-design')}} </h3>
                                             <p>

                                             </p>
                                            </div>
                                        </div>
                                        <!-- features-box end  --> 
                                    </div>
                                </div>
                                <!-- features-box-container end  -->                             
                            </div>
                            <div class="section-separator"></div>
                        </div>
                    </section>
                    <!--section end-->  

                    
                                <!--section  -->
                                <section      data-scrollax-parent="true">
                                    <div class="container">
                                    <div class="section-title">
                                            <h2><span>  {{__('cms.how-to-work')}}</span></h2>
                                            <div class="section-subtitle">{{__('cms.how-to-work')}}</div>
                            
                                            <span class="section-separator"></span>
                                        </div>
                                        <div class="process-wrap fl-wrap">
                                            <ul class="no-list-style">
                                                <li>
                                                    <div class="process-item">
                                                        <span class="process-count">1 </span>
                                                        <div class="time-line-icon"><i class="fa fa-user"></i></div>
                                                        <h4>  {{__('cms.register')}}    </h4>
                                                        <p>  {{__('cms.register_text')}}    </p>
                                                    </div>
                                                    <span class="pr-dec"></span>
                                                </li>
                                               
                                                <li>
                                                    <div class="process-item">
                                                        <span class="process-count">2</span>
                                                        <div class="time-line-icon"><i class="fal fa-layer-plus"></i></div>
                                                        <h4>  {{__('cms.bussiness_title')}}  </h4>
                                                        <p>  {{__('cms.bussiness_text')}}    </p>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="process-item">
                                                        <span class="process-count">3</span>
                                                        <div class="time-line-icon"><i class="fa fa-smile"></i></div>
                                                        <h4>  {{__('cms.enjoy')}}  </h4>
                                                        <p>  {{__('cms.enjoy_text')}}    </p>
                                                    </div>
                                                    <span class="pr-inc"></span>
                                                </li>
                                            </ul>
                                            <div class="process-end"><i class="fal fa-check"></i></div>
                                        </div>
                                    </div>
                                </section>
                                <!--section end-->
                  
                </div>
                <!--content end-->




@endsection
