@extends('template.app')

@section('content')

@include('template.sections.modal-menu')
    
    
 
    
    <section class="about-area ptb-100">
    <div class="container">
    <div class="row align-items-center">
    <div class="col-lg-6">
    <div class="about-content">
    <span>درباره ما</span>
  <p>
    ما کارگزار رسمي بيمه بازرگانی هستیم که مجوز و پروانه کارگزاری رو از بيمه مرکزي ج.ا.ا گرفتیم.
    زیر نظر بیمهمرکزی با 33 تا شرکت بیمه کار میکنیم.
     ما مشاور و وکیل بیمه ای شما نزد همه شرکتهای بیمه هستیم.
    فعاليت ما شامل مشاوره حرفه ای در تمام رشته های بیمه بازرگانی (آتش سوزي و زلزله، مسئوليت، اموال و بيمه هاي عمر و سرمايه گذاري و ...) و پیگیری خسارت احتمالی مي باشد.
    از افتخارات ما این هست که با طرح های درآمدزایی اختصاصی خودمان به رشد مالی همه ایرانی ها کمک میکنیم.
    دیگر افتخار تيم کارگزاري 1204 اين هستش که شما را تا آخرين مراحل تنها نخواهیم گذاشت و از صفر تا 100 کار همراه و در کنار شما خواهیم بود. یعنی اینکه حتي پس از صدور بيمه نامه توسط شرکتهاي بيمه اي اگر خداي نکرده خسارتي براي شما رخ دهد، تيم ما تا آخرين مرحله خسارت در کنار شما عزيزان پيگير خواهد بود.
  </p>
    <div class="about-list">
    <ul>
    <li>
    <i class="flaticon-checked"></i>
    پس انداز کنید
    </li>
    <li>
    <i class="flaticon-checked"></i>
    برنامه سریع
    </li>
    <li>
    <i class="flaticon-checked"></i>
    بیمه انعطاف پذیر
    </li>
    </ul>
    <ul class="ml-30">
    <li>
    <i class="flaticon-checked"></i>
    بدون کارگزار ، بدون فروش زیاد
    </li>
    <li>
    <i class="flaticon-checked"></i>
    برنامه ریزی سرمایه گذاری
    </li>
    <li>
    <i class="flaticon-checked"></i>
    مشاور حرفه ای
    </li>
    </ul>
    </div>
    </div>
    </div>
    <div class="col-lg-6">
    <div class="about-img wow fadeInRight" data-wow-delay=".2s">
    <img src="{{asset('template/img/about-img.png')}}" alt="Image">
    </div>
    </div>
    </div>
    </div>
    </section>
    
    
    <section class="our-vision-area ptb-100">
    <div class="container">
    <div class="row align-items-center">
    <div class="col-lg-6">
    <div class="about-img wow fadeInLeft" data-wow-delay=".2s">
    <img src="{{asset('template/img/vision-img.png')}}" alt="Image">
    </div>
    </div>
    <div class="col-lg-6">
    <div class="vision-content">
    <span>چشم انداز ما</span>
    <h2>تغییر نگرش و فرهنگ سازی حرفه ای در صنعت بیمه</h2>
  <p>
    بزرگترين، قويترين و قابل اطمينان ترين مركز تخصصي مشاوره حضوري و آنلاين خدمات بيمه اي علی الخصوص بیمه زندگی در سراسر ايران در محيطي پويا، دوستانه، دانش پژوه، دانش محور، راستگو، درستكار و قابل اعتماد و پيگير جهت تحقق سلامتي، امنيت و تمكن مالي، از بين بردن بيم و ترس تك تك بيمه گذاران حقيقي و حقوقي در تعامل كامل با شركتهاي بيمه اي زير نظر قوانين بيمه مركزي جمهوري اسلامي ايران در ايران 1401
  </p>
    <div class="vision-list">
    <ul>
    <li>
    <i class="bx bx-chevrons-left"></i>
    پس انداز کنید
    </li>
    <li>
    <i class="bx bx-chevrons-left"></i>
    برنامه سریع
    </li>
    <li>
    <i class="bx bx-chevrons-left"></i>
    بیمه انعطاف پذیر
    </li>
    </ul>
    <ul class="ml-30">
    <li>
    <i class="bx bx-chevrons-left"></i>
    بدون کارگزار ، بدون فروش زیاد
    </li>
    <li>
    <i class="bx bx-chevrons-left"></i>
    برنامه ریزی سرمایه گذاری
    </li>
    <li>
    <i class="bx bx-chevrons-left"></i>
    مشاور حرفه ای
    </li>
    </ul>
    </div>
    </div>
    </div>
    </div>
    </div>
    </section>
    
    
    <section class="about-area ptb-100">
    <div class="container">
    <div class="row align-items-center">
    <div class="col-lg-6">
    <div class="about-content">
    <span>درباره ما</span>
    <h2>بیانه ماموريت</h2>
    <p>
        كارگزاري رسمی بیمه 1204 با تاکید بر اصل مشتری مداري، نهایت سعی و تلاش خود را جهت ارائه بهترين خدمات در عرصه کاهش ریسک افراد حقيقي و حقوقي در صنعت بيمه، با استفاده از دانش و تکنولوژی وابسته به این صنعت در ایران بکار میگيرد. این كارگزاري  با به کارگیری مجموعه ای از راهکارهای نوآورانه و خلاق و همچنین استفاده بهینه از منابع و افزایش سود در راستای تکمیل زنجیره عرضه با هدف كليه افراد حقيقي وحقوقي و برآورده نمودن منافع همه ذینفعان در جهت ايجاد بزرگترين شبكه مويرگي خدمات بيمه اي علي الخصوص بيمه هاي زندگي (بيمه هاي عمر و سرمايه گذاري) به دليل بازدهي 100% اين رشته از طريق سود بردن از تكنولوژي و عرضه بهترين خدمات پس از فروش براي سپاس از وفاداري مشتريان محترم گام بر می دارد.
       </p>
    <div class="about-list">
    <div class="row">
    <div class="col-lg-6">
    <div class="about-single-list list-2">
    <i class="flaticon-social-care-1"></i>
    <span>مشتری را زیر پوشش می دهیم</span>
    </div>
    </div>
    <div class="col-lg-6">
    <div class="about-single-list">
    <i class="flaticon-target"></i>
    <span>100+ مشارکت جامعه در راه حل</span>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    <div class="col-lg-6">
    <div class="about-img-3 wow fadeInRight" data-wow-delay=".2s">
    <img src="{{asset('template/img/about-img-3.jpg')}}" alt="Image">
    <div class="about-img-2">
    <img src="{{ asset('template/img/about-img-2.jpg')}}" alt="Image">
    </div>
    </div>
    </div>
    </div>
    </div>
    </section>
    
    
   

    @endsection
    