@extends('template.app')

@section('content')


    @include('template.sections.modal-menu')
    
    
    
    
    
    <section class="contact-area ptb-100">
    <div class="container">
    <div class="row">
    <div class="col-lg-8">
    <div class="contact-wrap">
    <div class="contact-form">
    <div class="section-title">
    <h4>برای هرگونه سوال پیامی برای ما ارسال کنید</h4>
    @include('template.alert.error')
    @include('template.alert.success')
    </div>
    <form action="{{route('send.idea')}}" method="get" >
    <div class="row">
    <div class="col-lg-6 col-sm-6">
    <div class="form-group">
    <input type="text" name="name" id="name" class="form-control"  data-error="لطفا نام خود را وارد کنید" placeholder="نام شما">
    <div class="help-block with-errors"></div>
    </div>
    </div>
    <div class="col-lg-6 col-sm-6">
    <div class="form-group">
    <input type="email" name="email" id="email" class="form-control"  data-error="لطفا ایمیل خود را وارد کنید" placeholder="ایمیل شما">
    <div class="help-block with-errors"></div>
    </div>
    </div>
    
    <div class="col-lg-12 col-sm-12">
    <div class="form-group">
    <input type="text" name="subject" id="subject" class="form-control"  data-error="لطفا موضوع خود را وارد کنید" placeholder="موضوع شما">
    <div class="help-block with-errors"></div>
    </div>
    </div>
    <div class="col-lg-12 col-md-12">
    <div class="form-group">
    <textarea name="message" class="form-control" id="message" cols="30" rows="5"  data-error="پیام خود را بنویسید" placeholder="پیام شما"></textarea>
    <div class="help-block with-errors"></div>
    </div>
    </div>
    <div class="col-lg-12 col-md-12">
    <button type="submit" class="default-btn btn-six page-btn">
    ارسال پیام
    </button>
    <div id="msgSubmit" class="h3 text-center hidden"></div>
    <div class="clearfix"></div>
    </div>
    </div>
    </form>
    </div>
    </div>
    </div>
    <div class="col-lg-4">
    <div class="quick-contact">
    <h5>تماس سریع </h5>
    <ul>
    <li>
    <i class="flaticon-maps-and-flags"></i>
    موقعیت:
    <span>  </span>
    </li>
    <li>
    <i class="flaticon-call"></i>
    تماس با ما:
    <a href="tel:02112345678">02112345678 </a>
    </li>
    <li>
    <i class="flaticon-email"></i>
    ایمیل ما:
    <a href="mailto:support@karbim.ir">support@karbim.ir</a>
   
    </a>
    </li>
    </ul>
    </div>
    </div>
    </div>
    </div>
    </section>
    
    
    {{-- <div class="map-area">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d29624158.067582883!2d115.22492796537!3d-24.992291572825824!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2b2bfd076787c5df%3A0x538267a1955b1352!2sAustralia!5e0!3m2!1sen!2sbd!4v1587547480347!5m2!1sen!2sbd"></iframe>
    </div> --}}



@endsection