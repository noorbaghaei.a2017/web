@extends('template.app')

@section('content')



<div class="sidebar-modal">
    <div class="modal right fade" id="myModal2">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">
    <i class="bx bx-x"></i>
    </span>
    </button>
    <h2 class="modal-title">
    <a href="index.html">
    <img src="assets/img/logo.png" alt="Logo">
    </a>
    </h2>
    </div>
    <div class="modal-body">
    <div class="sidebar-modal-widget">
    <h3 class="title">درباره ما</h3>
    <p>لورم ایپسوم ساختار چاپ و متن را در بر می گیرد. لورم ایپسوم استاندارد صنعت بوده است. لورم ایپسوم ساختار چاپ و متن را در بر می گیرد. لورم ایپسوم استاندارد صنعت بوده است. لورم ایپسوم ساختار چاپ و متن را در بر می گیرد.</p>
    </div>
    <div class="sidebar-modal-widget">
    <h3 class="title">پیوندهای اضافی</h3>
    <ul>
    <li>
    <a href="log-in.html">وارد شوید</a>
    </li>
    <li>
    <a href="sign-in.html">ثبت نام</a>
    </li>
    <li>
    <a href="faq.html">سوالات متداول</a>
    </li>
    <li>
    <a href="#">خروج</a>
    </li>
    </ul>
    </div>
    <div class="sidebar-modal-widget">
    <h3 class="title">اطلاعات مشتری</h3>
    <ul class="contact-info">
    <li>
    <i class="bx bx-location-plus"></i>
    آدرس
    <span>ایران ، استان تهران ، میدان آزادی ، خیابان 9 شرقی</span>
    </li>
    <li>
    <i class="bx bx-envelope"></i>
    ایمیل
    <a href="mailto:hello@surety.com">hello@surety.com</a>
    </li>
    <li>
    <i class="bx bxs-phone-call"></i>
    تلفن
    <a href="tel:+822456974">02112345678</a>
    </li>
    </ul>
    </div>
    <div class="sidebar-modal-widget">
    <h3 class="title">با ما در ارتباط باشید</h3>
    <ul class="social-list">
    <li>
    <a href="#">
    <i class='bx bxl-twitter'></i>
    </a>
    </li>
    <li>
    <a href="#">
    <i class='bx bxl-facebook'></i>
    </a>
    </li>
    <li>
    <a href="#">
    <i class='bx bxl-instagram'></i>
    </a>
    </li>
    <li>
    <a href="#">
    <i class='bx bxl-linkedin'></i>
    </a>
    </li>
    <li>
    <a href="#">
    <i class='bx bxl-youtube'></i>
    </a>
    </li>
    </ul>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    
    
    <div class="page-title-area bg-4">
    <div class="container">
    <div class="page-title-content">
    <h2>ثبت نام</h2>
    <ul>
    <li>
    <a href="index.html">
    خانه
    </a>
    </li>
    <li>صفحات</li>
    <li>کاربر</li>
    <li>ثبت نام</li>
    </ul>
    </div>
    </div>
    </div>
    
    
    <section class="user-area-all-style sign-up-area ptb-100">
    <div class="container">
    <div class="row">
    <div class="col-12">
    <div class="contact-form-action">
    <div class="form-heading text-center">
    <h3 class="form-title">ایجاد حساب کاربری!</h3>
    <p class="form-desc">یا با شبکه اجتماعی وارد شوید.</p>
    </div>
    <form method="post">
    <div class="row">
    <div class="col-lg-4 col-md-4 col-sm-12">
    <button class="default-btn" type="submit">
    گوگل
    </button>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12">
    <button class="default-btn" type="submit">
    فیسبوک
    </button>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12">
    <button class="default-btn" type="submit">
    توئیتر
    </button>
    </div>
    <div class="col-md-12 col-sm-12">
    <div class="form-group">
    <input class="form-control" type="text" name="name" placeholder="نام">
    </div>
    </div>
    <div class="col-md-12 col-sm-12 ">
    <div class="form-group">
    <input class="form-control" type="text" name="name" placeholder="نام خانوادگی">
    </div>
    </div>
    <div class="col-md-12 col-sm-12">
    <div class="form-group">
    <input class="form-control" type="text" name="name" placeholder="نام کاربری شما">
    </div>
    </div>
    <div class="col-md-12 col-sm-12">
    <div class="form-group">
    <input class="form-control" type="email" name="email" placeholder="آدرس ایمیل">
    </div>
    </div>
    <div class="col-md-12 col-sm-12">
    <div class="form-group">
    <input class="form-control" type="text" name="password" placeholder="گذرواژه">
    </div>
    </div>
    <div class="col-md-12 col-sm-12 ">
    <div class="form-group">
    <input class="form-control" type="text" name="password" placeholder="تکرار گذرواژه">
    </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 form-condition">
    <div class="agree-label">
    <input type="checkbox" id="chb1">
    <label for="chb1">
    من شرایط سورتی را می پذیرم
    <a href="privacy-policy.html">حریم خصوصی</a>
    </label>
    </div>
    <div class="agree-label">
    <input type="checkbox" id="chb2">
     <label for="chb2">
    من شرایط سورتی را می پذیرم
    <a href="terms-conditions.html">قوانین و ضوابط</a>
    </label>
    </div>
    </div>
    <div class="col-12">
    <button class="default-btn btn-two" type="submit">
    ثبت نام
    </button>
    </div>
    <div class="col-12">
    <p class="account-desc">
    حساب کاربری داری؟
    <a href="log-in.html"> ورود</a>
    </p>
    </div>
    </div>
    </form>
    </div>
    </div>
    </div>
    </div>
    </section>


@endsection