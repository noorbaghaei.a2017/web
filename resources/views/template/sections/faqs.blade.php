<!-- Start Feedback Area -->
<section class="feedback-area ptb-100 pt-0">
    <div class="container">
        <div class="section-title">
            <span>بازخورد</span>
            <h2>بازخورد مشتریان</h2>

        </div>

        <div class="feedback-slides">
            <div class="client-thumbnails">
                <div>
                    <div class="item">
                        <div class="img-fill"><img src="{{asset('template/img/client-image/2.jpg')}}" alt="client"></div>
                    </div>

                    <div class="item">
                        <div class="img-fill"><img src="{{asset('template/img/client-image/4.jpg')}}" alt="client"></div>
                    </div>

                    <div class="item">
                        <div class="img-fill"><img src="{{asset('template/img/client-image/1.jpg')}}" alt="client"></div>
                    </div>

                    <div class="item">
                        <div class="img-fill"><img src="{{asset('template/img/client-image/5.jpg')}}" alt="client"></div>
                    </div>

                    <div class="item">
                        <div class="img-fill"><img src="{{asset('template/img/client-image/1.jpg')}}" alt="client"></div>
                    </div>


                </div>
            </div>

            <div class="client-feedback">
                <div>
                    <div class="item">
                        <div class="single-feedback">
                            <h3>امید نعمتی</h3>
                            <span>مشتری</span>
                            <p>
                                واقعا فوق العادست
                            </p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="single-feedback">
                            <h3>علی نیازی</h3>
                            <span>مشتری</span>
                            <p>
                                مشتری واقعا براشون مهمه
                            </p>

                        </div>
                    </div>

                    <div class="item">
                        <div class="single-feedback">
                            <h3>لیلا عزیز</h3>
                            <span>مشتری</span>
                            <p>
                                خیلی از اعتمادی که کردم راضی هستم
                            </p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="single-feedback">
                            <h3>سارا همتی</h3>
                            <span>مشتری</span>
                            <p>
                                من فکر نمیکردم انقدر رشد موهام خوب بشه
                            </p>


                        </div>
                    </div>

                    <div class="item">
                        <div class="single-feedback">
                            <h3>رضا حمیدی</h3>
                            <span>مشتری</span>
                            <p>
                                فقط خیلی دیر نوبت رسید وگرنه همه چی خوب بود
                            </p>
                        </div>
                    </div>



                </div>

                <button class="prev-arrow slick-arrow"><i class='flaticon-arrow-pointing-to-right'></i>

                </button>

                <button class="next-arrow slick-arrow"><i class='flaticon-left-arrow'></i>

                </button>
            </div>
        </div>
    </div>

    <div class="shape3"><img src="{{asset('template/img/shape/3.png')}}" class="wow fadeInRight" alt="image"></div>
</section>
<!-- End Feedback Area -->
