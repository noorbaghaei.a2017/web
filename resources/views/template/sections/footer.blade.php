
<footer class="footer-top-area footer-top-area-five pt-100 pb-70">
    <div class="container">
    <div class="row">
    <div class="col-lg-3 col-md-6">
    <div class="single-widget">
    <a href="index.html">
    <img src="{{ asset('template/img/brand-2-karbin.png')}}" alt="Logo" style="width:100px">
    </a>
    <p>
            با پیوستن به طرح VIP دیگه برای مسائل مالی نگرانی نداشته باشید و نگرانی مالی را از زندگی خود خارج کنید
    </p>
    <div class="social-area">
    <ul>
    
    
    <li>
    <a href="https://instagram.com/@karbim.ir"><i class="bx bxl-instagram"></i></a>
    </li>
    </ul>
    </div>
    </div>
    </div>
    <div class="col-lg-3 col-md-6">
    <div class="single-widget contact">
    <h3>تماس با ما</h3>
    <ul>
    <li class="pl-0">
    <a href="tel:Phone:02155055037">
    <i class="flaticon-call"></i>
    
    02155055037
    </a>
    </li>
    <li class="pl-0">
    <a href="mailto:support@karbim.ir">
    <i class="flaticon-email"></i>
    
    support@karbim.ir
    </a>
    </li>
    <li>
    <i class="flaticon-maps-and-flags"></i>
    </li>
    </ul>
    </div>
    </div>
    <div class="col-lg-3 col-md-6">
    <div class="single-widget">
    <h3>منو ها </h3>
    <ul>
    
    
    
    <li>
    <a href="{{route('about-us')}}">
    درباره ما
    </a>
    </li>
    <li>
    <a href="{{route('contact-us')}}">
    تماس با ما</a>
    </li>
    </ul>
    </div>
    </div>
    
    </div>
    </div>
    </footer>
    
    
    <footer class="footer-bottom-area footer-bottom-area-five">
    <div class="container">
    <div class="copy-right">
    <p>
    طراحی شده توسط   <a href="https://amin-nourbaghaei.info" target="_blank" rel="nofollow">amin nourbaghaei</a></p>
    </div>
    </div>
    </footer>
    
    
    <div class="go-top go-top-five">
    <i class='bx bx-chevrons-up'></i>
    <i class='bx bx-chevrons-up'></i>
    </div>
    
    
    <script src="{{ asset('template/js/jquery-3.5.1.slim.min.js')}}"></script>
    
    <script src="{{ asset('template/js/popper.min.js')}}"></script>
    
    <script src="{{ asset('template/js/bootstrap.min.js')}}"></script>
    
    <script src="{{ asset('template/js/jquery.meanmenu.js')}}"></script>
    
    <script src="{{ asset('template/js/wow.min.js')}}"></script>
    
    <script src="{{ asset('template/js/owl.carousel.js')}}"></script>
    
    <script src="{{ asset('template/js/jquery.magnific-popup.min.js')}}"></script>
    
    <script src="{{ asset('template/js/jquery.nice-select.min.js')}}"></script>
    
    <script src="{{ asset('template/js/parallax.min.js')}}"></script>
    
    <script src="{{ asset('template/js/jquery.mixitup.min.js')}}"></script>
    
    <script src="{{ asset('template/js/jquery.appear.js')}}"></script>
    
    <script src="{{ asset('template/js/odometer.min.js')}}"></script>
    
    <script src="{{ asset('template/js/jquery.ajaxchimp.min.js')}}"></script>
    
    <script src="{{ asset('template/js/form-validator.min.js')}}"></script>
    
    <script src="{{ asset('template/js/contact-form-script.js')}}"></script>
    
    <script src="{{ asset('template/js/custom.js')}}"></script>

    @yield('scripts')
    </body>
    </html>
    
