<!DOCTYPE HTML>
<html lang="{{LaravelLocalization::getCurrentLocale()}}">
    <head>
        <!--=============== basic  ===============-->
        <meta charset="UTF-8">
        {!! SEO::generate() !!}

            @yield('seo')
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">


        <meta name="google-site-verification" content="XM1lZFrEWSpl9ybBUmhC8byffubviu84b0pOa9C5zMc" />


        <meta name="csrf-token" content="{{ csrf_token() }}">
       

<link rel="stylesheet" href="{{asset('template/css/bootstrap.min.css')}}">

<link rel="stylesheet" href="{{ asset('template/css/owl.theme.default.min.css')}}">

<link rel="stylesheet" href="{{asset('template/css/owl.carousel.min.css')}}">

<link rel="stylesheet" href="{{ asset('template/css/magnific-popup.css')}}">

<link rel="stylesheet" href="{{ asset('template/css/animate.css')}}">

<link rel="stylesheet" href="{{ asset('template/css/boxicons.min.css')}}">

<link rel="stylesheet" href="{{ asset('template/css/flaticon.css')}}">

<link rel="stylesheet" href="{{ asset('template/css/meanmenu.css')}}">

<link rel="stylesheet" href="{{ asset('template/css/nice-select.css')}}">

<link rel="stylesheet" href="{{ asset('template/css/odometer.css')}}">

<link rel="stylesheet" href="{{ asset('template/css/style.css')}}">

<link rel="stylesheet" href="{{ asset('template/css/responsive.css')}}">

<link rel="stylesheet" href="{{ asset('template/css/rtl.css')}}">

<link rel="icon" type="image/png" href="{{ asset('template/img/brand-2-karbin.png')}}">



@yield('heads')
</head>
<body class="body-five">

<div class="preloader preloader-five">
<div class="lds-ripple">
<div></div>
<div></div>
</div>
</div>


<header class="header-area header-area-five">
<div class="top-header-area">
<div class="container">
<div class="row align-items-center">
<div class="col-lg-4">
 <div class="header-content location">
<span>
<i class="bx bx-location-plus"></i>
تهران خیابان خزانه بخارايي شمالي پلاک 517 
</span>
</div> 
</div>
    <div class="col-lg-2">
        <div class="header-content envelope">
        <span>
        <i class="bx bx-envelope"></i>

        </span>
        <a href="mailto:support@karbim.ir"> support@karbim.ir
        </a>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="header-content phone">
       
       
       @if(auth('client')->check())

       <a target="_blank" class="default-btn btn-six" href="{{ route('client.dashboard') }}">
        پنل کاربری
        </a>
       
        @else

        <a class="default-btn btn-six" href="{{ route('client.login') }}">
           ورود
            </a>

       @endif

          
        </div>
    </div>
    <div class="col-lg-2">
        <div class="header-content phone">
        <span>
        <i class="bx bx-phone-call"></i>

        </span>
        <a href="tel:02155055037">
            02155055037
        </a>
        </div>
    </div>
</div>
</div>
</div>

<div class="nav-area nev-style-five">
<div class="navbar-area">

<div class="mobile-nav">
<a href="{{ route('front.website')}}" class="logo">
<img src="{{ asset('template/img/brand-2-karbin.png')}}" style="width:80px" alt="Logo">
</a>
</div>

<div class="main-nav">
<nav class="navbar navbar-expand-md navbar-light">
<div class="container">
<a class="navbar-brand" href="{{ route('front.website')}}">
<img src="{{ asset('template/img/brand-2-karbin.png')}}" style="width:80px" alt="Logo">
</a>
<div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">
<ul class="navbar-nav m-auto">


<li class="nav-item">
<a href="{{ route('contact-us')}}" class="nav-link">تماس با ما</a>
</li>
<li class="nav-item">
        <a href="{{route('about-us')}}" class="nav-link">درباره ما  </a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link dropdown-toggle">
                بیمه زندگی
            <i class="bx bx-chevron-down"></i>
            </a>
            <ul class="dropdown-menu">
            <li class="nav-item">
            <a href="{{ route('bime.vip') }}" class="nav-link">طرح Vip  </a>
            </li>
          
         
            </ul>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link dropdown-toggle">
                    لیست فرم ها
                <i class="bx bx-chevron-down"></i>
                </a>
                <ul class="dropdown-menu">
                <li class="nav-item">
                <a href="{{ route('form.vip') }}" class="nav-link"> Vip  </a>
                </li>
              
             
                </ul>
                </li>
      
</ul>

<div class="others-option">


<div class="sidebar-menu">
<a href="#" data-toggle="modal" data-target="#myModal2">
<i class="bx bx-menu"></i>
</a>
</div>
</div>

</div>
</div>
</nav>
</div>
</div>
</div>

</header>

