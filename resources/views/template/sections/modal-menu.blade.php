<div class="sidebar-modal sidebar-modal-five">
    <div class="modal right fade" id="myModal2">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">
    <i class="bx bx-x"></i>
    </span>
    </button>
    <h2 class="modal-title">
    <a href="{{ route('front.website')}}">
    <img src="{{ asset('template/img/brand-2-karbin.png')}}" alt="Logo" style="width:80px">
    </a>
    </h2>
    </div>
    <div class="modal-body">
    <div class="sidebar-modal-widget">
    <h3 class="title">درباره ما</h3>
    <p>
            با پیوستن به طرح VIP دیگه برای مسائل مالی نگرانی نداشته باشید و نگرانی مالی را از زندگی خود خارج کنید
    </p>
    </div>
    
    <div class="sidebar-modal-widget">
    <h3 class="title">تماس با ما </h3>
    <ul class="contact-info">
    <li>
    <i class="bx bx-location-plus"></i>
    
    <span></span>
    </li>
    <li>
    <i class="bx bx-envelope"></i>
    
    <a href="mailto:support@karbim.ir">support@karbim.ir</a>
    </li>
    <li>
    <i class="bx bxs-phone-call"></i>
    
    <a href="tel:02155055037">02155055037</a>
    </li>
    </ul>
    </div>
    <div class="sidebar-modal-widget">
    <h3 class="title">با ما در ارتباط باشید</h3>
    <ul class="social-list">
    
    <li>
    <a target="_blank" href="https://instagram.com/karbim.ir">
    <i class='bx bxl-instagram'></i>
    </a>
    </li>
    
    </ul>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    